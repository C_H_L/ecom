<?php

use Illuminate\Http\Request;
use App\Helpers\ApiResponse;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace("Auth")->group(function()
{
	/*Route::get("test", function(Request $request)
	{
		return view("test");
	});*/
	
	Route::post("register", "RegisterController@register")->name(".register");
	Route::post("resend/email_verification", "RegisterController@resend_email_verification")->name(".resend.email_verification");
	
	Route::post("login", "LoginController@login")->name(".login");
});

Route::middleware("auth:api", "api.encryption")->group(function()
{
	Route::post("home/list_products", "HomeController@home_list_products")->name(".home.list_products");
	
	Route::namespace("Auth")->group(function()
	{
		Route::post("logout", "LoginController@logout")->name(".logout");
	});
	
});

Route::fallback(function()
{
	return ApiResponse::returnError(trans("m.access.route_not_found"));
});