<?php
	use Illuminate\Http\Request;
	use App\Helpers\ApiResponse;

	/*
	|--------------------------------------------------------------------------
	| API Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register API routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| is assigned the "api" middleware group. Enjoy building your API!
	|
	*/
	
	Route::get('/', function()
	{
		return redirect()
			->route("admin.index");
	})->name('./');
	
	Route::middleware(["auth:admin"])->group(function()
	{
		//Index
		Route::get('/', "GlobalController@index")->name('./');
		Route::get('home', 'GlobalController@index')->name('.home');
		Route::get('index', "GlobalController@index")->name('.index');
		
		//Dashboard
		Route::get('dashboard', function()
		{
			return view("dashboard");
		})->name('.dashboard');
		
		//Banner
		Route::get('banner', "GlobalController@banner")->name('.banner');
		Route::post('banner/upload', "GlobalController@banner_upload")->name('.banner.upload');
		Route::post('banner/{id}/single', "GlobalController@banner_single")->name('.banner.single');
		Route::post('banner/{id}/single/submit', "GlobalController@banner_single_submit")->name('.banner.single.submit');
		Route::get('banner/{id}/delete', "GlobalController@banner_delete")->name('.banner.delete');
		
		
		
		//Web config
		Route::get('webconfig', "WebConfigController@webconfig")->name('.webconfig');
		Route::post('webconfig/submit', "WebConfigController@webconfig_submit")->name('.webconfig.submit');
		
		//Admin account
		Route::get('adminacc', "GlobalController@adminacc")->name('.adminacc');
		Route::post('adminacc/submit', "GlobalController@adminacc_submit")->name('.adminacc.submit');
		
		//Logout
		Route::post('logout', 'Auth\LoginController@logout')->name('.logout');
	});

	Route::middleware(["guest:admin"])->group(function()
	{
		// Authentication Routes...
		Route::get('login', 'Auth\LoginController@showLoginForm')->name('.login');
		Route::post('login', 'Auth\LoginController@login');

		// Registration Routes...
		Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('.register');
		Route::post('register', 'Auth\RegisterController@register');

		// Password Reset Routes...
		Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('.password.request');
		Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('.password.email');
		Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('.password.reset');
		Route::post('password/reset', 'Auth\ResetPasswordController@reset');
	});
?>