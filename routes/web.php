<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("test", function()
{
	return view("test");
});

//Email verification
Route::get("verify/email/success", "UserController@verify_email_success")->name("user.verify.email.success");
Route::get("verify/email/fail", "UserController@verify_email_fail")->name("user.verify.email.fail");
Route::get("verify/email/{key}", "UserController@verify_email")->name("user.verify.email");

//Reset password
Route::get("reset_password/success", "UserController@reset_password_success")->name("user.reset_password.success");
Route::get("reset_password/fail", "UserController@reset_password_fail")->name("user.reset_password.fail");
Route::get("reset_password/{token}", "UserController@reset_password")->name("user.reset_password");
Route::post("reset_password/{token}/submit", "UserController@reset_password_submit")->name("user.reset_password.submit");

//Web-App routes
Route::prefix("app")->name("app")->namespace("App")->group(function()
{
	//Doctor profile
	Route::get("doctor/profile/{uuid}", "DoctorController@profile")->name(".doctor.profile");
	
	//Campaign profile
	Route::get("campaign/profile/{uuid}", "CampaignController@profile")->name(".campaign.profile");
	
	//Contact Us form
	Route::get("contact", "GlobalController@contact")->name(".contact");
	Route::post("contact/submit", "GlobalController@contact_submit")->name(".contact.submit");
});

//TAC
Route::get("tac", "GlobalController@tac")->name("tac");