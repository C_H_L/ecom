<?php
	return
	[
		"placeholder" =>
		[
			"common_img" => "images/placeholder/common_img.png",
			"profile_img" => "images/placeholder/profile_img.png",
			"newsfeed_img" => "images/placeholder/common_img.png",
			"navbanner_img" => "images/placeholder/common_img.png",
			"navitem_img" => "images/placeholder/common_img.png",
			"tutorial_img" => "images/placeholder/common_img.png",
		],
		"user_profile_img" => "uploads/profile_images/user/",
		"doctor_profile_img" => "uploads/profile_images/doctor/",
		"doctor_qr_img" => "uploads/qr_code/doctor/",
		"campaign_profile_img" => "uploads/profile_images/campaign/",
		"campaign_qr_img" => "uploads/qr_code/campaign/",
		"newsfeed_img" => "uploads/newsfeed/images/",
		"newsfeed_media" => "uploads/newsfeed/medias/",
		"banner_img" => "uploads/banner_images/",
		"navbanner_img" => "uploads/navbanner_images/",
		"navitem_img" => "uploads/navitem_images/",
		"tutorial_img" => "uploads/tutorial_images/",
	];