<?php
	return
	[
		"admin" =>
		[
			"domain" => env("ADMIN_DOMAIN", "localhost"),
			"namespace" => env("ADMIN_NAMESPACE", "\Admin"),
		],
		"web" =>
		[
			"domain" => env("WEB_DOMAIN", "localhost"),
			"namespace" => env("WEB_NAMESPACE", ""),
		],
		"api" =>
		[
			"domain" => env("API_DOMAIN", "localhost"),
			"namespace" => env("API_NAMESPACE", "\Api"),
		],
	];