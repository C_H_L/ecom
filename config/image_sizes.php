<?php
	return
	[
		"profile_img" =>
		[
			"web" => "600",
			"mobile" => "300",
		],
		"profile_qr_img" =>
		[
			"web" => "512",
			"mobile" => "256",
		],
		"newsfeed_img" =>
		[
			"web" => "1080",
			"mobile" => "800",
		],
		"banner_img" =>
		[
			"web" => "1080",
			"mobile" => "500",
		],
		"navbanner_img" =>
		[
			"web" => "1080",
			"mobile" => "500",
		],
		"navitem_img" =>
		[
			"web" => "800",
			"mobile" => "800",
		],
		"tutorial_img" =>
		[
			"web" => "1080",
			"mobile" => "800",
		],
	];