<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Schema, DB, Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
		/*DB::listen(function ($query) {
			Log::alert($query->sql);
			Log::alert($query->bindings);
		});*/
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
