<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use App\Helpers\ApiResponse;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if(!$request->expectsJson())
		{
			/*return redirect()
				->route("admin.login");
			
			dd($request->route()->is("admin.*"));
			
			if($guard == "web")
			{
				return redirect()
					->route('index');
			}
			else
			{
				return redirect()
					->route($guard . '.index');
			}*/
        }
    }
}
