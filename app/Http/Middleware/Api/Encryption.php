<?php

namespace App\Http\Middleware\Api;

use Closure, Route, Auth;
use App\Helpers\ApiResponse;

use Illuminate\Encryption\Encrypter;

class Encryption
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$user = Auth::user();
		$key = base64_decode($user->encryption_key);
		$encrypter = new Encrypter($key, config("app.cipher"));
		
		//dd($encrypter->encryptString('{"skip":0,"take":1}'));
		
		//Decryption
		if(!Route::is("api.user.update.profile_img") && env("REQUEST_ENCRYPTION") == 1 && !empty($request->cipher))
		{
			$cipher_json = json_decode(base64_decode($request->cipher));
			
			if(empty($cipher_json))
			{
				return ApiResponse::sendError("access.encryption.invalid_cipher", "Invalid cipher");
			}
			
			$data = json_decode($encrypter->decryptString(base64_encode(json_encode(
			[
				"iv" => $cipher_json->iv,
				"value" => $cipher_json->value,
				"mac" => hash_hmac('sha256', $cipher_json->iv.$cipher_json->value, $key),
			]))));
			
			if(empty($data))
			{
				return ApiResponse::sendError("access.encryption.decryption_fail", trans("m.access.encryption.decryption_fail"));
			}
			
			foreach($data as $key=>$val)
			{
				$request->request->add([$key => $val]);
			}
		}
		
		$response = $next($request);
		
		//Encryption
		if(env("RESPONSE_ENCRYPTION") == 1 || (env("RESPONSE_ENCRYPTION") == 2 && ($request->header("Encrypt-Response") == null || $request->header("Encrypt-Response") != 0)))
		{
			$responseData = $response->getData();
			
			if(property_exists($responseData, "data"))
			{
				$data_json = json_encode($responseData->data);
				
				if(empty($data_json))
				{
					return ApiResponse::sendError("access.encryption.encryption_fail", trans("m.access.encryption.encryption_fail"));
				}
				
				$responseData->data = $encrypter->encryptString($data_json);
				$response->setData($responseData);
			}
		}
		
		return $response;
    }
}

//Encyrpt
//$cipher = $encrypter->encryptString("test1234");
//dd(config("app.cipher"), base64_encode($key), $cipher, base64_decode($cipher));

//Decrypt
/*
	IV Base64: aStfkCB0eh4FE4kBaX7fsQ==
	Cipher: KwlMAGJhX8hCP2HC9AyPHA==
*/
/*$mac = hash_hmac('sha256', "aStfkCB0eh4FE4kBaX7fsQ=="."KwlMAGJhX8hCP2HC9AyPHA==", $key);

$encrypter->decryptString(base64_encode(json_encode(
[
	"iv" => "aStfkCB0eh4FE4kBaX7fsQ==",
	"value" => "KwlMAGJhX8hCP2HC9AyPHA==",
	"mac" => $mac,
])));*/