<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
		foreach(config("auth.guards") as $g => $val)
		{
			if($g != $guard) continue;
			
			if(Auth::guard($g)->check())
			{
				if($g == "web")
				{
					return redirect()
						->route("index");
				}
				else
				{
					return redirect()
						->route($g . ".index");
				}
			}
		}

        return $next($request);
    }
}
