<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth, Validator, Session, File, MessageBag, Config, Exception, Route, Mail, DB, Carbon\Carbon, Log, Crypt;
use App\User;
use App\PasswordResetToken;

class UserController extends Controller
{
    public function verify_email(Request $request)
	{
		try
		{
			$user_id = Crypt::decrypt($request->key);
		}
		catch(\Exception $e)
		{
			Session::flash("bootboxError", "Invalid key");
			return redirect()
				->route("user.verify.email.fail");
		}
		
		$user = User::id($user_id)->first();
		
		if(empty($user))
		{
			Session::flash("bootboxError", "User not found");
			return redirect()
				->route("user.verify.email.fail");
		}
		
		if(empty($user->email_verified_at))
		{
			if(!$user->updateRecord(
			[
				"email_verified_at" => Carbon::now(),
			]))
			{
				Session::flash("bootboxError", trans("m.record.update_fail"));
				return redirect()
					->route("user.verify.email.fail");
			}
		}
		
		return redirect()
			->route("user.verify.email.success");
	}
	
    public function verify_email_success(Request $request)
	{
		return view("verify_email_success");
	}
	
    public function verify_email_fail(Request $request)
	{
		return view("verify_email_fail");
	}
	
    public function reset_password(Request $request)
	{
		$token = PasswordResetToken::where("token", $request->token)->first();
		
		if(empty($token))
		{
			Session::flash("bootboxError", "Token not found");
			return redirect()
				->route("user.reset_password.fail");
		}
		
		if(!$token->isActive())
		{
			Session::flash("bootboxError", "Token has expired");
			return redirect()
				->route("user.reset_password.fail");
		}
		
		$user = $token->User;
		
		if(empty($user))
		{
			Session::flash("bootboxError", "User not found");
			return redirect()
				->route("user.reset_password.fail");
		}
		
		return view("reset_password")->with(
		[
			"reset_token" => $request->token,
		]);
	}
	
    public function reset_password_submit(Request $request)
	{
		$token = PasswordResetToken::where("token", $request->token)->first();
		
		if(empty($token))
		{
			Session::flash("bootboxError", "Token not found");
			return redirect()
				->route("user.reset_password.fail");
		}
		
		if(!$token->isActive())
		{
			Session::flash("bootboxError", "Token expired");
			return redirect()
				->route("user.reset_password.fail");
		}
		
		$user = $token->User;
		
		if(empty($user))
		{
			Session::flash("bootboxError", "User not found");
			return redirect()
				->route("user.reset_password.fail");
		}
		
		$validator = Validator::make($request->all(),
		[
			"password" => "required|string|min:8|confirmed",
		]);
		$validator->setAttributeNames(
		[
			"password" => "Password",
		]);
		
		if($validator->fails())
		{
			return redirect()
				->back()
				->withErrors($validator, "formerror")
				->withInput();
		}
		
		if(!$user->validatePassword($request->input("password")))
		{
			Session::flash("bootboxError", trans("m.record.update_fail"));
			return redirect()
				->back()
				->withErrors($validator, "formerror")
				->withInput();
		}
		
		if(!$user->updateRecord(
		[
			"password" => bcrypt($request->input("password")),
		]))
		{
			Session::flash("bootboxError", trans("m.record.update_fail"));
			return redirect()
				->route("user.verify.email.fail");
		}
		
		$token->updateRecord(
		[
			"used" => 1,
		]);
		
		return redirect()
			->route("user.reset_password.success");
	}
	
    public function reset_password_success(Request $request)
	{
		return view("reset_password_success");
	}
	
    public function reset_password_fail(Request $request)
	{
		return view("reset_password_fail");
	}
}
