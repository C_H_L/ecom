<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth, Validator, Session, File, MessageBag, Config, Exception, Route, Mail, DB, Carbon\Carbon, Log, Crypt;

class GlobalController extends Controller
{
    public function tac()
	{
		return view("tac");
	}
}
