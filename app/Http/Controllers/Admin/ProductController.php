<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Response, File, Auth, Validator, Hash, DB, Session, Config, Carbon\Carbon;
use App\Product;

class ProductController extends Controller
{
	//Product
	public function product()
	{
		$products = Product::get();
		
		return view(env('ADMIN_FOLDER') . 'product')
			->with("products", $products);
	}
	
	public function product_single($id = null)
	{
		if($id == null)
		{
			return view(env('ADMIN_FOLDER') . 'product_single')->with(
			[
				"sort" => Product::max("sort") + 1,
			]);
		}
		else
		{
			$product = Product::id($id)->first();
			
			if(empty($product))
			{
				Session::flash("bootboxError", "Product not found");
				
				return redirect()
					->route("admin.product");
			}
			
			return view(env('ADMIN_FOLDER') . 'product_single')->with(
			[
				"product" => $product,
				"skus" => $product->Sku,
			]);
		}
	}
	
	public function product_create(Request $request)
	{
		$validator = Validator::make($request->all(),
		[
			"name" => "required|string|max:255",
			"sort" => "required|numeric",
		]);
		$validator->setAttributeNames(
		[
			"name" => "Name",
			"sort" => "Sorting Index",
		]);
		
		if($validator->fails())
		{
			return redirect()
				->back()
				->withErrors($validator, "formerror")
				->withInput();
		}
		
		$product = new Product;
		$product->createRecord(
		[
			"name" => $request->input('name'),
			"sort" => $request->input('sort'),
			"active" => $request->has("active") ? 1 : 0,
		]);
		
		Session::flash("bootboxSuccess", "Product added");
		
	    return redirect()
			->route("admin.product");
	}
	
	public function product_update(Request $request, $id)
	{
		$product = Product::id($id)->first();
		
		if(empty($product))
		{
			Session::flash("bootboxError", "Product not found");
			
			return redirect()
				->route("admin.product");
		}
		
		$validator = Validator::make($request->all(),
		[
			"name" => "required|string|max:255",
			"sort" => "required|numeric",
		]);
		$validator->setAttributeNames(
		[
			"name" => "Name",
			"sort" => "Sorting Index",
		]);
		
		if($validator->fails())
		{
			return redirect()
				->back()
				->withErrors($validator, "formerror")
				->withInput();
		}
		
		$product->updateRecord(
		[
			"name" => $request->input('name'),
			"sort" => $request->input('sort'),
		]);
		
		Session::flash("bootboxSuccess", "Product updated");
		
		return redirect()
			->back();
	}
	
	public function product_delete($id)
	{
		$product = Product::id($id)->first();
		
		if(empty($product))
		{
			Session::flash("bootboxError", "Product not found");
			
			return redirect()
				->route("admin.product");
		}
		
		$product->deleteRecord();
		
		Session::flash("bootboxSuccess", "Product deleted");
		
	    return redirect()
			->route("admin.product");
	}
	
	public function product_toggleactive($id)
	{
		$product = Product::id($id)->first();
		
		if(empty($product))
		{
			Session::flash("bootboxError", "Product not found");
			
			return redirect()
				->route("admin.product");
		}
		
		$product->active = !$product->active;
		$product->save();
		
		if($product->active)
		{
			Session::flash("bootboxSuccess", "Product set to active");
		}
		else
		{
			Session::flash("bootboxSuccess", "Product set to inactive");
		}
		
		return redirect()
			->back();
	}
	
	public function product_getskus($id)
	{
		$product = Product::id($id)->first();
		
		if(empty($product))
		{
			return ["erorr", "Product not found"];
		}
		
		$skus = $product->Sku()->pluck("name", "id");
		
		return ["success", $skus->toJson()];
	}
}