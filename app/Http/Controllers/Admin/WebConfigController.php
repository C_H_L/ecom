<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Response, File, Auth, Validator, Hash, DB, Session, Config;
use App\webConfig;

class WebConfigController extends Controller
{
	public function webconfig()
	{
		$webConfigs = webConfig::active()->get();
		
		return view(env('ADMIN_FOLDER') . 'webconfig')
			->with("webConfigs", $webConfigs);
	}
	
	public function webconfig_submit()
	{
		$webConfigs = webConfig::active()->get();
		
		$propertyRules = array();
		
		foreach($webConfigs as $webConfig)
		{
			$propertyRules[$webConfig->name] = "required";
			$propertyRules[$webConfig->name . "_unit"] = "nullable";
		}
		
		$validator = Validator::make(Input::all(), $propertyRules);
		
		if($validator->fails())
		{
			return redirect()
				->back()
				->withErrors($validator, "formerror")
				->withInput();
		}
		
		foreach($webConfigs as $webConfig)
		{
			$webConfig->value = Input::get($webConfig->name);
			$webConfig->unit = Input::get($webConfig->name . "_unit");
			$webConfig->save();
		}
		
		Session::flash("bootboxSuccess", "Website configuration updated");
		
		return redirect()
			->back();
	}
	
}