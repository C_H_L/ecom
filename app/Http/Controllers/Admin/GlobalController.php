<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Response, File, Auth, Validator, Hash, DB, Session, Config, Route;
use App\Admin;
use App\Banner;

class GlobalController extends Controller
{
	public function __construct()
	{
		//dd(Route::current()->getAction());
		
	}
	
	public function index()
	{
		return redirect()->route('admin.banner');
	}
	
	public function banner()
	{
		$banners = Banner::get();
		
		return view(env('ADMIN_FOLDER') . 'banner')
			->with("banners", $banners);
	}
	 
    public function banner_upload()
    {
		$validator = Validator::make(Input::all(),
		[
			'file.*' => 'required|file|mimes:jpg,jpeg,png,gif,bmp,svg,mp4|max:' . (8 * (2**20)),
		]);
		$validator->setAttributeNames(
		[
			'file.*' => 'File',
		]);
		
		if($validator->fails())
		{
			return response($validator->messages()->all(), 500);
		}
		
		$type = 0;
		$videoValidator = Validator::make(Input::all(),
		[
			'file.*' => 'mimes:mp4',
		]);
		if(!$videoValidator->fails())
		{
			$type = 1;
		}
		
		foreach(Input::file("file") as $file)
		{
			$filename = "banner_" . date("Ymdhis") . "_" . rand(000000, 999999) . "." . strtolower($file->getClientOriginalExtension());
			
			if(!File::exists(config("assetpath.banner_img"))) File::makeDirectory(config("assetpath.banner_img"), 0755, true);
			$file->move(config("assetpath.banner_img"), $filename);
			
			$banner = new Banner;
			$banner->createRecord(
			[
				"sort" => Banner::max("sort") + 1,
				"type" => $type,
				"url" => $filename,
				"active" => 1,
			]);
		}
		
	    return response(200);
    }
	
	public function banner_single($id)
	{
		$banner = Banner::id($id)->first();
		
		if(empty($banner))
		{
			return response("Banner not found", 500);
		}
		
		return
		[
			"sort" => $banner->sort,
			"active" => $banner->active,
		];
	}
	
	public function banner_single_submit($id)
	{
		$banner = Banner::id($id)->first();
		
		if(empty($banner))
		{
			return response("Banner not found", 500);
		}
		
		$validator = Validator::make(Input::all(),
		[
			"sort" => "required | numeric",
		]);
		
		$validator->setAttributeNames(
		[
			"sort" => "Sort",
		]);
		
		if($validator->fails())
		{
			return response($validator->messages()->all());
		}
		
		$banner->updateRecord(
		[
			"sort" => Input::get("sort"),
			"active" => Input::has("active") ? 1 : 0,
		]);
		
	    return response('success', 200);
	}
	
	public function banner_delete($id)
	{
		$banner = Banner::id($id)->first();
		
		if(empty($banner))
		{
			return response("Banner not found", 500);
		}
		
		$banner->deleteRecord();
		
	    return response('success', 200);
	}
	
	
	
	public function adminacc()
	{
		return view(env('ADMIN_FOLDER') . 'adminacc')
			->with("admin", Auth::guard('admin')->user());
	}
	
	public function adminacc_submit()
	{
		$validator = Validator::make(Input::all(),
		[
			"cpassword" => "required",
			"password" => "required|min:6|confirmed",
		]);
		$validator->setAttributeNames(
		[
			"cpassword" => "Current Password",
			"password" => "New Password",
		]);
		
		if($validator->fails())
		{
			return redirect()
				->back()
				->withErrors($validator, "formerror")
				->withInput();
		}
		
		$admin = Auth::guard('admin')->user();
		
		if(Input::has("password") && !Hash::check(Input::get("cpassword"), $admin->password))
		{
			Session::flash("bootboxError", "Wrong current password");
			
			return redirect()
				->back()
				->withInput();
		}
		
		$admin->password = bcrypt(Input::get("password"));
		$admin->save();
		
		Session::flash("bootboxSuccess", "Successfully updated admin password");
		
		return redirect()
			->back();
	}
}