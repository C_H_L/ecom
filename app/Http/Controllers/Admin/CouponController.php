<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Response, File, Auth, Validator, Hash, DB, Session, Config, Carbon\Carbon;
use App\Coupon;

class CouponController extends Controller
{
	public function coupon()
	{
		$coupons = Coupon::get();
		
		return view(env('ADMIN_FOLDER') . 'coupon')
			->with("coupons", $coupons);
	}
	
	public function coupon_single($id = null)
	{
		if($id == null)
		{
			return view(env('ADMIN_FOLDER') . 'coupon_single')->with(
			[
				"formula_names" => Coupon::$formula_names,
				"usage_types" => Coupon::$usage_types,
			]);
		}
		else
		{
			$coupon = Coupon::id($id)->first();
			
			if(empty($coupon))
			{
				Session::flash("bootboxError", "Coupon not found");
				
				return redirect()
					->route("admin.coupon");
			}
			
			return view(env('ADMIN_FOLDER') . 'coupon_single')->with(
			[
				"formula_names" => Coupon::$formula_names,
				"usage_types" => Coupon::$usage_types,
				"coupon" => $coupon,
			]);
		}
	}
	
	public function coupon_create(Request $request)
	{
		$validator = Validator::make($request->all(),
		[
			"name" => "required|string|max:255",
			"code" => "required|string",
			"formula" => "required|numeric",
			"amount" => "required|numeric",
			"start_time" => "nullable|date",
			"end_time" => "nullable|date|after_or_equal:start_time",
			"usage_type" => "required|numeric",
			"usage_target" => "requiredIf:usage_type,1|nullable|numeric",
		]);
		$validator->setAttributeNames(
		[
			"name" => "Name",
			"code" => "Code",
			"formula" => "Formula",
			"amount" => "Amount",
			"start_time" => "Validity Start",
			"end_time" => "Validity End",
			"usage_type" => "Usage Type",
			"usage_target" => "Usage Target",
		]);
		
		if($validator->fails())
		{
			return redirect()
				->back()
				->withErrors($validator, "formerror")
				->withInput();
		}
		
		$amount = $request->input('amount');
		if($request->input('formula') == 0)
		{
			$amount = $request->input('amount') / 100;
		}
		
		$coupon = new Coupon;
		$coupon->createRecord(
		[
			"name" => $request->input('name'),
			"code" => $request->input('code'),
			"formula" => $request->input('formula'),
			"amount" => $amount,
			"start_time" => !empty($request->input('start_time')) ? Carbon::parse($request->input('start_time')) : null,
			"end_time" => !empty($request->input('end_time')) ? Carbon::parse($request->input('end_time')) : null,
			"usage_type" => $request->input('usage_type'),
			"usage_target" => $request->input('usage_target'),
			"active" => $request->has("active") ? 1 : 0,
		]);
		
		Session::flash("bootboxSuccess", "Coupon added");
		
	    return redirect()
			->route("admin.coupon");
	}
	
	public function coupon_update(Request $request, $id)
	{
		$coupon = Coupon::id($id)->first();
		
		if(empty($coupon))
		{
			Session::flash("bootboxError", "Coupon not found");
			
			return redirect()
				->route("admin.coupon");
		}
		
		$validator = Validator::make($request->all(),
		[
			"name" => "required|string|max:255",
			"code" => "required|string",
			"formula" => "required|numeric",
			"amount" => "required|numeric",
			"start_time" => "nullable|date",
			"end_time" => "nullable|date|after_or_equal:start_time",
			"usage_type" => "required|numeric",
			"usage_target" => "requiredIf:usage_type,1|nullable|numeric",
		]);
		$validator->setAttributeNames(
		[
			"name" => "Name",
			"code" => "Code",
			"formula" => "Formula",
			"amount" => "Amount",
			"start_time" => "Validity Start",
			"end_time" => "Validity End",
			"usage_type" => "Usage Type",
			"usage_target" => "Usage Target",
		]);
		
		if($validator->fails())
		{
			return redirect()
				->back()
				->withErrors($validator, "formerror")
				->withInput();
		}
		
		$amount = $request->input('amount');
		if($request->input('formula') == 0)
		{
			$amount = $request->input('amount') / 100;
		}
		
		$coupon->updateRecord(
		[
			"name" => $request->input('name'),
			"code" => $request->input('code'),
			"formula" => $request->input('formula'),
			"amount" => $amount,
			"start_time" => !empty($request->input('start_time')) ? Carbon::parse($request->input('start_time')) : null,
			"end_time" => !empty($request->input('end_time')) ? Carbon::parse($request->input('end_time')) : null,
			"usage_type" => $request->input('usage_type'),
			"usage_target" => $request->input('usage_target'),
		]);
		
		Session::flash("bootboxSuccess", "Coupon updated");
		
		return redirect()
			->back();
	}
	
	public function coupon_delete($id)
	{
		$coupon = Coupon::id($id)->first();
		
		if(empty($coupon))
		{
			Session::flash("bootboxError", "Coupon not found");
			
			return redirect()
				->route("admin.coupon");
		}
		
		$coupon->deleteRecord();
		
		Session::flash("bootboxSuccess", "Coupon deleted");
		
	    return redirect()
			->route("admin.coupon");
	}
	
	public function coupon_toggleactive($id)
	{
		$coupon = Coupon::id($id)->first();
		
		if(empty($coupon))
		{
			Session::flash("bootboxError", "Coupon not found");
			
			return redirect()
				->route("admin.coupon");
		}
		
		$coupon->active = !$coupon->active;
		$coupon->save();
		
		if($coupon->active)
		{
			Session::flash("bootboxSuccess", "Coupon set to active");
		}
		else
		{
			Session::flash("bootboxSuccess", "Coupon set to inactive");
		}
		
		return redirect()
			->back();
	}
}