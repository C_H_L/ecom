<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Response, File, Auth, Validator, Hash, DB, Session, Config, Carbon\Carbon;
use App\Bundle;
use App\BundleItem;
use App\BundleImage;
use App\Product;
use App\Sku;

class BundleController extends Controller
{
	//Bundles
	public function bundle()
	{
		$bundles = Bundle::get();
		
		return view(env('ADMIN_FOLDER') . 'bundle')
			->with("bundles", $bundles);
	}
	
	public function bundle_single($id = null)
	{
		if($id == null)
		{
			return view(env('ADMIN_FOLDER') . 'bundle_single')->with(
			[
				"sort" => Bundle::max("sort") + 1,
			]);
		}
		else
		{
			$bundle = Bundle::id($id)->first();
			
			if(empty($bundle))
			{
				Session::flash("bootboxError", "Bundle not found");
				
				return redirect()
					->route("admin.bundle");
			}
			
			return view(env('ADMIN_FOLDER') . 'bundle_single')->with(
			[
				"sort" => Bundle::max("sort") + 1,
				"bundle" => $bundle,
				"bundle_items" => $bundle->BundleItem,
				"bundle_images" => $bundle->BundleImage,
			]);
		}
	}
	
	public function bundle_create(Request $request)
	{
		$validator = Validator::make($request->all(),
		[
			"name" => "required|string|max:255",
			"title" => "nullable|string",
			"sort" => "required|numeric",
			"description" => "required|string",
			"price" => "required|numeric",
		]);
		$validator->setAttributeNames(
		[
			"name" => "Name",
			"title" => "Title",
			"sort" => "Sorting Index",
			"description" => "Description",
			"price" => "Price",
		]);
		
		if($validator->fails())
		{
			return redirect()
				->back()
				->withErrors($validator, "formerror")
				->withInput();
		}
		
		$bundle = new Bundle;
		$bundle->createRecord(
		[
			"name" => $request->input('name'),
			"title" => $request->input('title'),
			"sort" => $request->input('sort'),
			"description" => $request->input('description'),
			"price" => $request->input('price'),
			"active" => $request->has("active") ? 1 : 0,
		]);
		
		Session::flash("bootboxSuccess", "Bundle added");
		
	    return redirect()
			->route("admin.bundle");
	}
	
	public function bundle_update(Request $request, $id)
	{
		$bundle = Bundle::id($id)->first();
		
		if(empty($bundle))
		{
			Session::flash("bootboxError", "Bundle not found");
			
			return redirect()
				->route("admin.bundle");
		}
		
		$validator = Validator::make($request->all(),
		[
			"name" => "required|string|max:255",
			"title" => "nullable|string",
			"sort" => "required|numeric",
			"description" => "required|string",
			"price" => "required|numeric",
		]);
		$validator->setAttributeNames(
		[
			"name" => "Name",
			"title" => "Title",
			"sort" => "Sorting Index",
			"description" => "Description",
			"price" => "Price",
		]);
		
		if($validator->fails())
		{
			return redirect()
				->back()
				->withErrors($validator, "formerror")
				->withInput();
		}
		
		$bundle->updateRecord(
		[
			"name" => $request->input('name'),
			"title" => $request->input('title'),
			"sort" => $request->input('sort'),
			"description" => $request->input('description'),
			"price" => $request->input('price'),
		]);
		
		Session::flash("bootboxSuccess", "Bundle updated");
		
		return redirect()
			->back();
	}
	
	public function bundle_delete($id)
	{
		$bundle = Bundle::id($id)->first();
		$bundle->deleteRecord();
		
		Session::flash("bootboxSuccess", "Bundle deleted");
		
	    return redirect()
			->route("admin.bundle");
	}
	
	public function bundle_toggleactive($id)
	{
		$bundle = Bundle::id($id)->first();
		
		if(empty($bundle))
		{
			Session::flash("bootboxError", "Bundle not found");
			
			return redirect()
				->route("admin.bundle");
		}
		
		$bundle->active = !$bundle->active;
		$bundle->save();
		
		if($bundle->active)
		{
			Session::flash("bootboxSuccess", "Bundle set to active");
		}
		else
		{
			Session::flash("bootboxSuccess", "Bundle set to inactive");
		}
		
		return redirect()
			->back();
	}
	
	//Bundle items
	public function bundle_item_single($bundle_id, $id = null)
	{
		$bundle = Bundle::id($bundle_id)->first();
		
		if(empty($bundle))
		{
			Session::flash("bootboxError", "Bundle not found");
			
			return redirect()
				->route("admin.bundle");
		}
		
		if($id == null)
		{
			return view(env('ADMIN_FOLDER') . 'bundle_item_single')->with(
			[
				"products" => Product::get(),
				"skus" => Product::first()->Sku,
				"bundle_id" => $bundle_id,
			]);
		}
		else
		{
			$bundle_item = BundleItem::id($id)->first();
			
			if(empty($bundle_item))
			{
				Session::flash("bootboxError", "Bundle item not found");
				
				return redirect()
					->route("admin.bundle.single", ["id" => $bundle_id, "#bundle_item"]);
			}
			
			return view(env('ADMIN_FOLDER') . 'bundle_item_single')->with(
			[
				"products" => Product::get(),
				"skus" => $bundle_item->Product->Sku,
				"bundle_id" => $bundle_id,
				"bundle_item" => $bundle_item,
			]);
		}
	}
	
	public function bundle_item_create(Request $request, $bundle_id)
	{
		$bundle = Bundle::id($bundle_id)->first();
		
		if(empty($bundle))
		{
			Session::flash("bootboxError", "Bundle not found");
			
			return redirect()
				->route("admin.bundle");
		}
		
		$validator = Validator::make($request->all(),
		[
			"product_id" => "required|numeric",
			"sku_id" => "required|numeric",
			"quantity" => "required|numeric",
		]);
		$validator->setAttributeNames(
		[
			"product_id" => "Product",
			"sku_id" => "Sku",
			"quantity" => "Quantity",
		]);
		
		if($validator->fails())
		{
			return redirect()
				->back()
				->withErrors($validator, "formerror")
				->withInput();
		}
		
		$bundle_item = new BundleItem;
		$bundle_item->createRecord(
		[
			"bundle_id" => $bundle_id,
			"product_id" => $request->input("product_id"),
			"sku_id" => $request->input("sku_id"),
			"quantity" => $request->input("quantity"),
			"active" => $request->has("active") ? 1 : 0,
		]);
		
		Session::flash("bootboxSuccess", "Bundle item added");
		
	    return redirect()
			->route("admin.bundle.single", ['id' => $bundle_id, "#bundle_item"]);
	}
	
	public function bundle_item_update(Request $request, $bundle_id, $id)
	{
		$bundle = Bundle::id($bundle_id)->first();
		
		if(empty($bundle))
		{
			Session::flash("bootboxError", "Bundle not found");
			
			return redirect()
				->route("admin.bundle");
		}
		
		$bundle_item = BundleItem::id($id)->first();
		
		if(empty($bundle_item))
		{
			Session::flash("bootboxError", "Bundle item not found");
			
			return redirect()
				->route("admin.bundle.single", ["id" => $bundle_id, "#bundle_item"]);
		}
		
		$validator = Validator::make($request->all(),
		[
			"product_id" => "required|numeric",
			"sku_id" => "required|numeric",
			"quantity" => "required|numeric",
		]);
		$validator->setAttributeNames(
		[
			"product_id" => "Product",
			"sku_id" => "Sku",
			"quantity" => "Quantity",
		]);
		
		if($validator->fails())
		{
			return redirect()
				->back()
				->withErrors($validator, "formerror")
				->withInput();
		}
		
		$bundle_item->updateRecord(
		[
			"product_id" => $request->input("product_id"),
			"sku_id" => $request->input("sku_id"),
			"quantity" => $request->input("quantity"),
		]);
		
		Session::flash("bootboxSuccess", "Bundle item updated");
		
		return redirect()
			->back();
	}
	
	public function bundle_item_delete($bundle_id, $id)
	{
		$bundle = Bundle::id($bundle_id)->first();
		
		if(empty($bundle))
		{
			Session::flash("bootboxError", "Bundle not found");
			
			return redirect()
				->route("admin.bundle");
		}
		
		$bundle_item = BundleItem::id($id)->first();
		
		if(empty($bundle_item))
		{
			Session::flash("bootboxError", "Bundle item not found");
			
			return redirect()
				->route("admin.bundle.single", ["id" => $bundle_id, "#bundle_item"]);
		}
		
		$bundle_item->deleteRecord();
		
		Session::flash("bootboxSuccess", "Bundle item deleted");
		
	    return redirect()
			->route("admin.bundle.single", ['id' => $bundle_id, "#bundle_item"]);
	}
	
	public function bundle_item_toggleactive($bundle_id, $id)
	{
		$bundle = Bundle::id($bundle_id)->first();
		
		if(empty($bundle))
		{
			Session::flash("bootboxError", "Bundle not found");
			
			return redirect()
				->route("admin.bundle");
		}
		
		$bundle_item = BundleItem::id($id)->first();
		
		if(empty($bundle_item))
		{
			Session::flash("bootboxError", "Bundle item not found");
			
			return redirect()
				->route("admin.bundle.single", ["id" => $bundle_id, "#bundle_item"]);
		}
		
		$bundle_item->active = !$bundle_item->active;
		$bundle_item->save();
		
		if($bundle_item->active)
		{
			Session::flash("bootboxSuccess", "Bundle item set to active");
		}
		else
		{
			Session::flash("bootboxSuccess", "Bundle item set to inactive");
		}
		
		return redirect()
			->back();
	}
	
	//Bundle images
    public function bundle_image_upload(Request $request, $bundle_id)
    {
		$bundle = Bundle::id($bundle_id)->first();
		
		if(empty($bundle))
		{
			return response("Bundle not found", 500);
		}
		
		$validator = Validator::make($request->all(),
		[
			'file.*' => 'required|image|max:' . (8 * (2**20)),
		]);
		$validator->setAttributeNames(
		[
			'file.*' => 'File',
		]);
		
		if($validator->fails())
		{
			return response($validator->messages()->all(), 500);
		}
		
		foreach($request->file("file") as $file)
		{
			$filePath = config("assetpath.bundle_img") . $bundle_id . "/images/";
			$fileName = "image_" . date("Ymdhis") . "_" . rand(000000, 999999) . "." . strtolower($file->getClientOriginalExtension());
			
			if(!File::exists($filePath)) File::makeDirectory($filePath, 0755, true);
			$file->move($filePath, $fileName);
			
			$bundle_image = new BundleImage;
			$bundle_image->createRecord(
			[
				"bundle_id" => $bundle_id,
				"sort" => BundleImage::where("bundle_id", $bundle_id)->max("sort") + 1,
				"url" => $fileName,
				"active" => 1,
			]);
		}
		
	    return response(200);
    }
	
	public function bundle_image_single($bundle_id, $id)
	{
		$bundle = Bundle::id($bundle_id)->first();
		
		if(empty($bundle))
		{
			return response("Bundle not found", 500);
		}
		
		$bundle_image = BundleImage::id($id)->first();
		
		if(empty($bundle_image))
		{
			return response("Bundle image not found", 500);
		}
		
		return
		[
			"sort" => $bundle_image->sort,
			"active" => $bundle_image->active,
		];
	}
	
	public function bundle_image_single_submit(Request $request, $bundle_id, $id)
	{
		$bundle = Bundle::id($bundle_id)->first();
		
		if(empty($bundle))
		{
			return response("Bundle not found", 500);
		}
		
		$bundle_image = BundleImage::id($id)->first();
		
		if(empty($bundle_image))
		{
			return response("Bundle image not found", 500);
		}
		
		$validator = Validator::make($request->all(),
		[
			"sort" => "required | numeric",
		]);
		
		$validator->setAttributeNames(
		[
			"sort" => "Sort",
		]);
		
		if($validator->fails())
		{
			return response($validator->messages()->all());
		}
		
		$bundle_image->updateRecord(
		[
			"sort" => $request->input("sort"),
			"active" => $request->input("active") ? 1 : 0,
		]);
		
	    return response('success', 200);
	}
	
	public function bundle_image_delete($bundle_id, $id)
	{
		$bundle = Bundle::id($bundle_id)->first();
		
		if(empty($bundle))
		{
			return response("Bundle not found", 500);
		}
		
		$bundle_image = BundleImage::id($id)->first();
		
		if(empty($bundle_image))
		{
			return response("Bundle image not found", 500);
		}
		
		$bundle_image->deleteRecord();
		
	    return response('success', 200);
	}
}