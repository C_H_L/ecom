<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Response, File, Auth, Validator, Hash, DB, Session, Config, Carbon\Carbon;
use App\Product;
use App\Sku;
use App\SkuImage;

class SkuController extends Controller
{
	//SKU
	public function sku_single($product_id, $id = null)
	{
		$product = Product::id($product_id)->first();
		
		if(empty($product))
		{
			Session::flash("bootboxError", "Product not found");
			
			return redirect()
				->route("admin.product");
		}
		
		if($id == null)
		{
			return view(env('ADMIN_FOLDER') . 'sku_single')->with(
			[
				"product_id" => $product_id,
				"sort" => Sku::where("product_id", $product_id)->max("sort") + 1,
			]);
		}
		else
		{
			$sku = Sku::id($id)->first();
			
			if(empty($sku))
			{
				Session::flash("bootboxError", "SKU not found");
				
				return redirect()
					->route("admin.product.single", ['id' => $product_id, "#sku"]);
			}
			
			return view(env('ADMIN_FOLDER') . 'sku_single')->with(
			[
				"product_id" => $product_id,
				"sku" => $sku,
				"sku_images" => $sku->SkuImage,
			]);
		}
	}
	
	public function sku_create(Request $request, $product_id)
	{
		$product = Product::id($product_id)->first();
		
		if(empty($product))
		{
			Session::flash("bootboxError", "Product not found");
			
			return redirect()
				->route("admin.product");
		}
		
		$validator = Validator::make($request->all(),
		[
			"name" => "required|string|max:255",
			"sort" => "required|numeric",
			"img_url" => "nullable|image|max:" . (8 * (2**20)),
			"dimension_url" => "nullable|image|max:" . (8 * (2**20)),
			"ribbon_url" => "nullable|image|max:" . (8 * (2**20)),
			"title" => "nullable|string",
			"description" => "nullable|string",
			"size" => "nullable|string|max:255",
			"freebie" => "nullable|string",
			"price" => "nullable|numeric",
		]);
		$validator->setAttributeNames(
		[
			"name" => "Name",
			"sort" => "Sorting Index",
			"img_url" => "Main Image",
			"dimension_url" => "Dimension Image",
			"ribbon_url" => "Ribbon Image",
			"title" => "Title",
			"description" => "Description",
			"size" => "Size",
			"freebie" => "Freebie",
			"price" => "Price",
		]);
		
		if($validator->fails())
		{
			return redirect()
				->back()
				->withErrors($validator, "formerror")
				->withInput();
		}
		
		$sku = new Sku;
		$sku->createRecord(
		[
			"product_id" => $product_id,
			"name" => $request->input("name"),
			"sort" => $request->input("sort"),
			"img_url" => null,
			"dimension_url" => null,
			"ribbon_url" => null,
			"title" => $request->input("title"),
			"description" => $request->input("description"),
			"size" => $request->input("size"),
			"freebie" => $request->input("freebie"),
			"price" => $request->input("price"),
			"direct_purchase" => $request->has("direct_purchase") ? 1 : 0,
			"active" => $request->has("active") ? 1 : 0,
		]);
		
		//Main Image
		if($request->hasFile("img_url"))
		{
			$file = $request->file("img_url");
			$img_url = "sku_" . $sku->id . "." . strtolower($file->getClientOriginalExtension());
			
			if(!File::exists(config("assetpath.sku_img") . $sku->id)) File::makeDirectory(config("assetpath.sku_img") . $sku->id, 0755, true);
			$file->move(config("assetpath.sku_img") . $sku->id, $img_url);
			
			$sku->updateRecord(
			[
				"img_url" => $img_url,
			]);
		}
		
		//Dimension Image
		if($request->hasFile("dimension_url"))
		{
			$file = $request->file("dimension_url");
			$dimension_url = "dimension_" . $sku->id . "." . strtolower($file->getClientOriginalExtension());
			
			if(!File::exists(config("assetpath.sku_img") . $sku->id)) File::makeDirectory(config("assetpath.sku_img") . $sku->id, 0755, true);
			$file->move(config("assetpath.sku_img") . $sku->id, $dimension_url);
			
			$sku->updateRecord(
			[
				"dimension_url" => $dimension_url,
			]);
		}
		
		//Ribbon Image
		if($request->hasFile("ribbon_url"))
		{
			$file = $request->file("ribbon_url");
			$ribbon_url = "ribbon_" . $sku->id . "." . strtolower($file->getClientOriginalExtension());
			
			if(!File::exists(config("assetpath.sku_img") . $sku->id)) File::makeDirectory(config("assetpath.sku_img") . $sku->id, 0755, true);
			$file->move(config("assetpath.sku_img") . $sku->id, $ribbon_url);
			
			$sku->updateRecord(
			[
				"ribbon_url" => $ribbon_url,
			]);
		}
		
		Session::flash("bootboxSuccess", "SKU added");
		
	    return redirect()
			->route("admin.product.single", ['id' => $product_id, "#sku"]);
	}
	
	public function sku_update(Request $request, $product_id, $id)
	{
		$product = Product::id($product_id)->first();
		
		if(empty($product))
		{
			Session::flash("bootboxError", "Product not found");
			
			return redirect()
				->route("admin.product");
		}
		
		$sku = Sku::id($id)->first();
		
		if(empty($sku))
		{
			Session::flash("bootboxError", "SKU not found");
			
			return redirect()
				->route("admin.product.single", ['id' => $product_id, "#sku"]);
		}
		
		$validator = Validator::make($request->all(),
		[
			"name" => "required|string|max:255",
			"sort" => "required|numeric",
			"img_url" => "nullable|image|max:" . (8 * (2**20)),
			"dimension_url" => "nullable|image|max:" . (8 * (2**20)),
			"ribbon_url" => "nullable|image|max:" . (8 * (2**20)),
			"title" => "nullable|string",
			"description" => "nullable|string",
			"size" => "nullable|string|max:255",
			"freebie" => "nullable|string",
			"price" => "nullable|numeric",
		]);
		$validator->setAttributeNames(
		[
			"name" => "Name",
			"sort" => "Sorting Index",
			"img_url" => "Main Image",
			"dimension_url" => "Dimension Image",
			"ribbon_url" => "Ribbon Image",
			"title" => "Title",
			"description" => "Description",
			"size" => "Size",
			"freebie" => "Freebie",
			"price" => "Price",
		]);
		
		if($validator->fails())
		{
			return redirect()
				->back()
				->withErrors($validator, "formerror")
				->withInput();
		}
		
		//Main Image
		if($request->hasFile("img_url"))
		{
			$file = $request->file("img_url");
			$img_url = "sku_" . $sku->id . "." . strtolower($file->getClientOriginalExtension());
			
			if(!File::exists(config("assetpath.sku_img") . $sku->id)) File::makeDirectory(config("assetpath.sku_img") . $sku->id, 0755, true);
			$file->move(config("assetpath.sku_img") . $sku->id, $img_url);
			
			$sku->updateRecord(
			[
				"img_url" => $img_url,
			]);
		}
		
		//Dimension Image
		if($request->hasFile("dimension_url"))
		{
			$file = $request->file("dimension_url");
			$dimension_url = "dimension_" . $sku->id . "." . strtolower($file->getClientOriginalExtension());
			
			if(!File::exists(config("assetpath.sku_img") . $sku->id)) File::makeDirectory(config("assetpath.sku_img") . $sku->id, 0755, true);
			$file->move(config("assetpath.sku_img") . $sku->id, $dimension_url);
			
			$sku->updateRecord(
			[
				"dimension_url" => $dimension_url,
			]);
		}
		
		//Ribbon Image
		if($request->hasFile("ribbon_url"))
		{
			$file = $request->file("ribbon_url");
			$ribbon_url = "ribbon_" . $sku->id . "." . strtolower($file->getClientOriginalExtension());
			
			if(!File::exists(config("assetpath.sku_img") . $sku->id)) File::makeDirectory(config("assetpath.sku_img") . $sku->id, 0755, true);
			$file->move(config("assetpath.sku_img") . $sku->id, $ribbon_url);
			
			$sku->updateRecord(
			[
				"ribbon_url" => $ribbon_url,
			]);
		}
		
		$sku->updateRecord(
		[
			"product_id" => $product_id,
			"name" => $request->input("name"),
			"sort" => $request->input("sort"),
			"title" => $request->input("title"),
			"description" => $request->input("description"),
			"size" => $request->input("size"),
			"freebie" => $request->input("freebie"),
			"price" => $request->input("price"),
		]);
		
		Session::flash("bootboxSuccess", "SKU updated");
		
		return redirect()
			->back();
	}
	
	public function sku_delete($product_id, $id)
	{
		$sku = Sku::id($id)->first();
		
		if(empty($sku))
		{
			Session::flash("bootboxError", "SKU not found");
			
			return redirect()
				->route("admin.product.single", ['id' => $product_id, "#sku"]);
		}
		
		$sku->deleteRecord();
		
		Session::flash("bootboxSuccess", "SKU deleted");
		
	    return redirect()
			->route("admin.product.single", ['id' => $product_id, "#sku"]);
	}
	
	public function sku_toggleactive($product_id, $id)
	{
		$product = Product::id($product_id)->first();
		
		if(empty($product))
		{
			Session::flash("bootboxError", "Product not found");
			
			return redirect()
				->route("admin.product");
		}
		
		$sku = Sku::id($id)->first();
		
		if(empty($sku))
		{
			Session::flash("bootboxError", "SKU not found");
			
			return redirect()
				->route("admin.product.single", ['id' => $product_id, "#sku"]);
		}
		
		$sku->active = !$sku->active;
		$sku->save();
		
		if($sku->active)
		{
			Session::flash("bootboxSuccess", "SKU set to active");
		}
		else
		{
			Session::flash("bootboxSuccess", "SKU set to inactive");
		}
		
		return redirect()
			->back();
	}
	
	public function sku_toggledirectpurchase($product_id, $id)
	{
		$product = Product::id($product_id)->first();
		
		if(empty($product))
		{
			Session::flash("bootboxError", "Product not found");
			
			return redirect()
				->route("admin.product");
		}
		
		$sku = Sku::id($id)->first();
		
		if(empty($sku))
		{
			Session::flash("bootboxError", "SKU not found");
			
			return redirect()
				->route("admin.product.single", ['id' => $product_id, "#sku"]);
		}
		
		$sku->direct_purchase = !$sku->direct_purchase;
		$sku->save();
		
		if($sku->direct_purchase)
		{
			Session::flash("bootboxSuccess", "SKU set to direct purchaseable");
		}
		else
		{
			Session::flash("bootboxSuccess", "SKU set to not direct purchaseable");
		}
		
		return redirect()
			->back();
	}
	
	//SKU images
    public function sku_image_upload(Request $request, $product_id, $sku_id)
    {
		$sku = Sku::id($sku_id)->first();
		
		if(empty($sku))
		{
			return response("SKU not found", 500);
		}
		
		$validator = Validator::make($request->all(),
		[
			'file.*' => 'required|image|max:' . (8 * (2**20)),
		]);
		$validator->setAttributeNames(
		[
			'file.*' => 'File',
		]);
		
		if($validator->fails())
		{
			return response($validator->messages()->all(), 500);
		}
		
		foreach($request->file("file") as $file)
		{
			$filePath = config("assetpath.sku_img") . $sku->id . "/images/";
			$fileName = "image_" . date("Ymdhis") . "_" . rand(000000, 999999) . "." . strtolower($file->getClientOriginalExtension());
			
			if(!File::exists($filePath)) File::makeDirectory($filePath, 0755, true);
			$file->move($filePath, $fileName);
			
			$sku_image = new SkuImage;
			$sku_image->createRecord(
			[
				"sku_id" => $sku->id,
				"sort" => SkuImage::where("sku_id", $sku->id)->max("sort") + 1,
				"url" => $fileName,
				"active" => 1,
			]);
		}
		
	    return response(200);
    }
	
	public function sku_image_single($product_id, $sku_id, $id)
	{
		$sku = Sku::id($sku_id)->first();
		
		if(empty($sku))
		{
			return response("SKU not found", 500);
		}
		
		$sku_image = SkuImage::id($id)->first();
		
		if(empty($sku_image))
		{
			return response("SKU image not found", 500);
		}
		
		return
		[
			"sort" => $sku_image->sort,
			"active" => $sku_image->active,
		];
	}
	
	public function sku_image_single_submit(Request $request, $product_id, $sku_id, $id)
	{
		$sku = Sku::id($sku_id)->first();
		
		if(empty($sku))
		{
			return response("SK not found", 500);
		}
		
		$sku_image = SkuImage::id($id)->first();
		
		if(empty($sku_image))
		{
			return response("SKU image not found", 500);
		}
		
		$validator = Validator::make($request->all(),
		[
			"sort" => "required | numeric",
		]);
		
		$validator->setAttributeNames(
		[
			"sort" => "Sort",
		]);
		
		if($validator->fails())
		{
			return response($validator->messages()->all());
		}
		
		$sku_image->updateRecord(
		[
			"sort" => $request->input("sort"),
			"active" => $request->input("active") ? 1 : 0,
		]);
		
	    return response('success', 200);
	}
	
	public function sku_image_delete($product_id, $sku_id, $id)
	{
		$sku = Sku::id($sku_id)->first();
		
		if(empty($sku))
		{
			return response("SKU not found", 500);
		}
		
		$sku_image = SkuImage::id($id)->first();
		
		if(empty($sku_image))
		{
			return response("SKU image not found", 500);
		}
		
		$sku_image->deleteRecord();
		
	    return response('success', 200);
	}
}