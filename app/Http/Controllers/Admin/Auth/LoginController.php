<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;

use Auth, Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
	//Can be a string or function
    protected $redirectTo = "/";
	protected $redirectAfterLogout = "/";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }
	
	public function username()
	{
		return "username";
	}
	
	protected function guard()
	{
		return Auth::guard("admin");
	}
	
	public function showLoginForm()
	{
		return view(env("ADMIN_FOLDER") . "auth.login");
	}
	
	public function logout(Request $request)
	{
		$this->guard()->logout();
		$request->session()->invalidate();

		return redirect()
			->route("admin.index");
	}
}
