<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Response, File, Auth, Validator, Hash, DB, Session, Config, Carbon\Carbon;
use App\Order;
use App\OrderAddr;
use App\OrderCoupon;
use App\OrderItem;
use App\OrderItemSku;

class OrderController extends Controller
{
	//Order
	public function order()
	{
		$orders_pending = Order::active()->where("status", 0)->get();
		$orders_processing = Order::active()->where("status", 1)->get();
		$orders_shipping = Order::active()->where("status", 2)->get();
		$orders_completed = Order::active()->where("status", 3)->get();
		$orders_inactive = Order::where("active", 0)->get();
		
		return view(env('ADMIN_FOLDER') . 'order')->with(
		[
			"order_groups" => (object) array_map(function($arr)
			{
				return (object) $arr;
			},
			[
				"orders_pending" =>
				[
					"name" => "Pending",
					"orders" => $orders_pending,
					"count" => count($orders_pending),
				],
				"orders_processing" =>
				[
					"name" => "Processing",
					"orders" => $orders_processing,
					"count" => count($orders_processing),
				],
				"orders_shipping" =>
				[
					"name" => "Shipping",
					"orders" => $orders_shipping,
					"count" => count($orders_shipping),
				],
				"orders_completed" =>
				[
					"name" => "Completed",
					"orders" => $orders_completed,
					"count" => count($orders_completed),
				],
				"orders_inactive" =>
				[
					"name" => "Inactive",
					"orders" => $orders_inactive,
					"count" => count($orders_inactive),
				],
			]),
		]);
	}
	
	public function order_single($id)
	{
		$order = Order::id($id)->first();
		
		if(empty($order))
		{
			Session::flash("bootboxError", "Order not found");
			
			return redirect()
				->route("admin.order");
		}
		
		return view(env('ADMIN_FOLDER') . 'order_single')->with(
		[
			"order" => $order,
			"skus" => $order->Sku,
		]);
	}
	
	public function order_delete($id)
	{
		$order = Order::id($id)->first();
		
		if(empty($order))
		{
			Session::flash("bootboxError", "Order not found");
			
			return redirect()
				->route("admin.order");
		}
		
		$order->deleteRecord();
		
		Session::flash("bootboxSuccess", "Order deleted");
		
	    return redirect()
			->route("admin.order");
	}
	
	public function order_toggleactive($id)
	{
		$order = Order::id($id)->first();
		
		if(empty($order))
		{
			Session::flash("bootboxError", "Order not found");
			
			return redirect()
				->route("admin.order");
		}
		
		$order->active = !$order->active;
		$order->save();
		
		if($order->active)
		{
			Session::flash("bootboxSuccess", "Order set to active");
		}
		else
		{
			Session::flash("bootboxSuccess", "Order set to inactive");
		}
		
		return redirect()
			->back();
	}
	
	public function order_updatestatus($id, $action)
	{
		$order = Order::id($id)->first();
		
		if(empty($order))
		{
			Session::flash("bootboxError", "Order not found");
			
			return redirect()
				->route("admin.order");
		}
		
		if($action == 0)
		{
			$order->status = max(0, $order->status - 1);
		}
		else if($action == 1)
		{
			$order->status = min(count(Order::$statuses) - 1, $order->status + 1);
		}
		$order->save();
		
		Session::flash("bootboxSuccess", "Order status updated");
		
		return redirect()
			->back();
	}
	
	//Order item
	public function order_item_single($order_id, $id)
	{
		$order = Order::id($order_id)->first();
		
		if(empty($order))
		{
			Session::flash("bootboxError", "Order not found");
			
			return redirect()
				->route("admin.order");
		}
		
		$order_item = OrderItem::id($id)->first();
		
		if(empty($order))
		{
			Session::flash("bootboxError", "Order item not found");
			
			return redirect()
				->route("admin.order.single", ['id' => $order_id]);
		}
		
		return view(env('ADMIN_FOLDER') . 'order_item_single')->with(
		[
			"order_id" => $order_id,
			"order_item" => $order_item,
		]);
	}
}