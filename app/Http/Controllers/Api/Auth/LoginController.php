<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use Image;
use Auth, Validator, Session, File, MessageBag, Config, Exception, Route, Mail, DB, Carbon\Carbon, Log, Crypt;
use App\User;
use App\PasswordResetToken;

class LoginController extends Controller
{
    public function login(Request $request)
	{
		$validator = Validator::make($request->all(),
		[
			"username" => "required|string|max:255",
			"password" => "required|string|min:8",
			"fcm_token" => "required|string|max:256",
		]);
		$validator->setAttributeNames(
		[
			"username" => "Username",
			"password" => "Password",
			"fcm_token" => "FCM Token",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::returnError($validator->errors()->all());
		}
		
		if(Auth::attempt(["username" => $request->input("username"), "password" => $request->input("password")]))
		{
			$user = Auth::user();
			
			if(!$user->active) return ApiResponse::returnError(trans("m.login.inactive"));
			
			//Check if email is verified
			if(empty($user->email_verified_at)) return ApiResponse::returnError(trans("m.login.verify_email"));
			
			if(!$user->updateRecord(
			[
				"fcm_token" => $request->input("fcm_token"),
			])) return ApiResponse::returnUpdateRecordError();
			
			//Clear user access token
			$user->revokeAccessTokens();
			
			$response["access_token"] = $user->createToken(config("route.api.domain"))->accessToken;
			$response["key"] = $user->encryption_key;
			
			return ApiResponse::returnSuccess($response);
		}
		
		return ApiResponse::returnError(trans("m.login.not_match"));
	}
	
	public function logout(Request $request)
	{
		$user = Auth::user();
			
		if(!$user->updateRecord(
		[
			"fcm_token" => null,
		])) return ApiResponse::returnUpdateRecordError();
		
		if(empty($user))
		{
			return ApiResponse::returnError(trans("m.record.not_found"));
		}
		
		$user->revokeAccessTokens();
		
		return ApiResponse::returnSuccess(null, trans("m.logout.success"));
	}
}