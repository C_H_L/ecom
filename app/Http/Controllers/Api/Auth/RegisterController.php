<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use Auth, Validator, Session, File, MessageBag, Config, Exception, Route, Mail, DB, Carbon\Carbon, Log, Crypt;
use App\User;

class RegisterController extends Controller
{
    public function register(Request $request)
	{
		$validator = Validator::make($request->all(),
		[
			"username" => "required|string|max:255",
			"name" => "required|string|max:255",
			"email" => "required|email|max:255",
			"password" => "required|string|min:8|confirmed",
			"contact_num" => "required|string|max:128",
			"dob" => "required|date",
		]);
		$validator->setAttributeNames(
		[
			"username" => "Your username",
			"name" => "Your name",
			"email" => "Email",
			"password" => "Password",
			"contact_num" => "Contact Number",
			"dob" => "Date of Birth",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::returnError($validator->errors()->all());
		}
		
		$user = User::where("username", $request->input("username"))
				->orWhere("email", $request->input("email"))
				->first();
		
		if(!empty($user))
		{
			return ApiResponse::returnError(trans("m.register.taken"));
		}
		
		$user = new User;
		
		if(!$user->validatePassword($request->input("password")))
		{
			return ApiResponse::returnError(trans("m.register.password_validation_failed"));
		}
		
		if(!$user->createRecord(
		[
			"username" => $request->input("username"),
			"name" => $request->input("name"),
			"email" => $request->input("email"),
			"password" => bcrypt($request->input("password")),
			"contact_num" => $request->input("contact_num"),
			"dob" => $request->input("dob"),
			"active" => 1,
		])) return ApiResponse::returnCreateRecordError();
		
		//Email verification
		Mail::send("Api.emails.email_verification", ["user" => $user, "key" => Crypt::encrypt($user->id)], function($msg) use($user)
		{
			$msg->subject("Email verification for " . env("APP_NAME"))
				->to($user->email, $user->name);
		});
		
		return ApiResponse::returnSuccess(trans("m.register.success"));
	}
	
	public function resend_email_verification(Request $request)
	{
		$validator = Validator::make($request->all(),
		[
			"email" => "required|email|max:255",
		]);
		$validator->setAttributeNames(
		[
			"email" => "Email",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::returnError($validator->errors()->all());
		}
		
		$user = User::where("email", $request->input("email"))->active()
				->first();
		
		if(empty($user))
		{
			return ApiResponse::returnError(trans("m.register.resend_email_verification.not_found"));
		}
		
		if(!empty($user->email_verified_at))
		{
			return ApiResponse::returnError(trans("m.register.resend_email_verification.already_verified"));
		}
		
		//Email verification
		Mail::send("Api.emails.email_verification", ["user" => $user, "key" => Crypt::encrypt($user->id)], function($msg) use($user)
		{
			$msg->subject("Email verification for " . env("APP_NAME"))
				->to($user->email, $user->name);
		});
		
		return ApiResponse::returnSuccessAck(null, trans("m.register.resend_email_verification.success"));
	}
}
