<?php

namespace App\Http\Controllers\Api\v1\User;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use Auth, Validator, Session, File, MessageBag, Config, Exception, Route, Mail, DB, Carbon\Carbon, Log;
use Image;
use App\User;
use App\UserNotification;

class UserNotificationController extends Controller
{
	public function list(Request $request)
	{
		$user = Auth::user();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user_notification.list.not_found", trans("m.record.not_found"));
		}
		
		$validator = Validator::make($request->all(),
		[
			"skip" => "requiredWith:take|numeric|min:0",
			"take" => "requiredWith:skip|numeric|min:0",
			"is_read" => "nullable|numeric|min:0",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::sendError("user_notification.list.validation_fail", $validator->messages()->first());
		}
		
		$user_notifications = $user->UserNotification();
		if($request->has("skip") && $request->has("take")) $user_notifications->skip($request->input("skip"))->take($request->input("take"));
		if($request->has("is_read")) $user_notifications->where("is_read", $request->input("is_read"));
		$user_notifications = $user_notifications->get();
		
		$data = null;
		foreach($user_notifications as $key=>$user_notification)
		{
			$data[$key] = $user_notification->getAPIListData();
		}
		
		return ApiResponse::sendSuccess("user_notification.list.success", $data);
	}
	
	public function count(Request $request)
	{
		$user = Auth::user();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user_notification.count.not_found", trans("m.record.not_found"));
		}
		
		$validator = Validator::make($request->all(),
		[
			"is_read" => "nullable|numeric|min:0",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::sendError("user_notification.count.validation_fail", $validator->messages()->first());
		}
		
		$user_notifications = $user->UserNotification();
		if($request->has("is_read")) $user_notifications->where("is_read", $request->input("is_read"));
		$user_notifications = $user_notifications->count();
		
		return ApiResponse::sendSuccess("user_notification.count.success", $user_notifications);
	}
	
	public function set_read(Request $request)
	{
		$user = Auth::user();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user_notification.set.read.user_not_found", trans("m.record.not_found"));
		}
		
		$validator = Validator::make($request->all(),
		[
			"id" => "required|numeric",
			"read" => "nullable|numeric",
		]);
		$validator->setAttributeNames(
		[
			"id" => "ID",
			"read" => "Read",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::sendError("user_notification.set.read.validation_fail", $validator->messages()->first());
		}
		
		$user_notification = UserNotification::id($request->input("id"))->first();
		
		if(empty($user_notification))
		{
			return ApiResponse::sendError("user_notification.set.read.not_found", trans("m.record.not_found"));
		}
		
		if(!$user_notification->updateRecord(
		[
			"is_read" => $request->has("read") ? $request->input("read") : 1,
		])) return ApiResponse::sendUpdateRecordError("user_notification.set.read.update_fail");
		
		return ApiResponse::sendSuccess("user_notification.set.read.success", trans("m.record.update_success"));
	}
}