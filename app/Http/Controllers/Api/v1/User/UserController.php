<?php

namespace App\Http\Controllers\Api\v1\User;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use Auth, Validator, Session, File, MessageBag, Config, Exception, Route, Mail, DB, Carbon\Carbon, Log, Crypt, Hash;
use Image;
use App\User;
use App\UserAddr;

class UserController extends Controller
{
	public function detail(Request $request)
	{
		$user = Auth::user();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user.detail.not_found", trans("m.record.not_found"));
		}
		
		$addr = $user->UserAddr;
		
		return ApiResponse::sendSuccess("user.detail.success",
			[
				"details" => $user->getAPIData(),
				"address" => !empty($addr) ? $addr->getAPIData() : null,
			]);
	}
	
	public function address_detail(Request $request)
	{
		$user = Auth::user();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user.address.detail.user_not_found", trans("m.record.not_found"));
		}
		
		$addr = $user->UserAddr;
		
		if(empty($addr))
		{
			return ApiResponse::sendError("user.address.detail.not_found", trans("m.record.not_found"));
		}
		
		return ApiResponse::sendSuccess("user.address.detail.success", $addr->getAPIData());
	}
	
	public function review_list(Request $request)
	{
		$validator = Validator::make($request->all(),
		[
			"skip" => "requiredWith:take|numeric|min:0",
			"take" => "requiredWith:skip|numeric|min:0",
			"user_id" => "required|numeric",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::sendError("user.review.list.validation_fail", $validator->messages()->first());
		}
		
		$user = User::id($request->input("user_id"))->first();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user.review.list.not_found", trans("m.record.not_found"));
		}
		
		$reviews = $user->UserReviewActive();
		
		if($request->has("skip") && $request->has("take")) $reviews->skip($request->input("skip"))->take($request->input("take"));
		$reviews = $reviews->get();
		
		$data = null;
		foreach($reviews as $key=>$review)
		{
			$data[$key] = $review->getAPIListData();
		}
		
		return ApiResponse::sendSuccess("user.review.list.success", $data);
	}
	
	public function has_appointment_needs_review(Request $request)
	{
		$user = Auth::user();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user.has.appointment_needs_review.not_found", trans("m.record.not_found"));
		}
		
		$appointment_needs_review = $user->AppointmentActive()
			->completed()
			->doesntHave("DoctorReview")
			->first();
		
		$campaign_appointment_needs_review = $user->CampaignAppointmentActive()
			->completed()
			->doesntHave("CampaignReview")
			->first();
		
		if(!empty($appointment_needs_review)) return ApiResponse::sendSuccess("user.has.appointment_needs_review.success.has_appointment", $appointment_needs_review->getAPIListData());
		
		if(!empty($campaign_appointment_needs_review)) return ApiResponse::sendSuccess("user.has.appointment_needs_review.success.has_campaign_appointment", $campaign_appointment_needs_review->getAPIListData());
		
		return ApiResponse::sendSuccessAck("user.has.appointment_needs_review.success.no_appointment");
	}
	
	public function update_password(Request $request)
	{
		$user = Auth::user();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user_update.password.not_found", trans("m.record.not_found"));
		}
		
		if($user->login_type != "normal")
		{
			return ApiResponse::sendError("user_update.password.not_available", trans("m.user.not_available"));
		}
		
		$validator = Validator::make($request->all(),
		[
			"current_password" => "required|string|min:6",
			"new_password" => "required|string|min:6|confirmed",
		]);
		$validator->setAttributeNames(
		[
			"current_password" => "Current Password",
			"new_password" => "New Password",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::sendError("user_update.password.validation_fail", $validator->messages()->first());
		}
		
		if(!Hash::check($request->input("current_password"), $user->password))
		{
			return ApiResponse::sendError("user_update.password.incorrect_password", trans("m.user.incorrect_password"));
		}
		
		if(!$user->updateRecord(
		[
			"password" => bcrypt($request->input("new_password")),
		])) return ApiResponse::sendUpdateRecordError("user_update.password.update_fail");
		
		return ApiResponse::sendSuccessAck("user_update.password.success", trans("m.record.update_success"));
	}
	
	public function update_email(Request $request)
	{
		$user = Auth::user();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user_update.email.not_found", trans("m.record.not_found"));
		}
		
		if($user->login_type != "normal")
		{
			return ApiResponse::sendError("user_update.email.not_available", trans("m.user.not_available"));
		}
		
		$validator = Validator::make($request->all(),
		[
			"email" => "required|email|max:255",
		]);
		$validator->setAttributeNames(
		[
			"email" => "Email",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::sendError("user_update.email.validation_fail", $validator->messages()->first());
		}
		
		$exist = User::where("email", $request->input("email"))->first();
		
		if(!empty($exist))
		{
			return ApiResponse::sendError("user_update.email.email_exist", trans("m.user.email_exist"));
		}
		
		if(!$user->updateRecord(
		[
			"email" => $request->input("email"),
		])) return ApiResponse::sendUpdateRecordError("user_update.email.update_fail");
		
		return ApiResponse::sendSuccessAck("user_update.email.success", trans("m.record.update_success"));
	}
	
	public function update_profile_img(Request $request)
	{
		$user = Auth::user();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user_update.profile_img.not_found", trans("m.record.not_found"));
		}
		
		$validator = Validator::make($request->all(),
		[
			"profile_img" => "required|image|max:" . env("MAX_FILE_SIZE"),
		]);
		$validator->setAttributeNames(
		[
			"profile_img" => "Profile Image",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::sendError("user_update.profile_img.validation_fail", $validator->messages()->first());
		}
		
		if(!$user->updateRecord(
		[
			"profile_img" => $request->file("profile_img"),
		])) return ApiResponse::sendUpdateRecordError("user_update.profile_img.update_fail");
		
		return ApiResponse::sendSuccessAck("user_update.profile_img.success", trans("m.record.update_success"));
	}
	
	public function update(Request $request)
	{
		$user = Auth::user();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user_update.not_found", trans("m.record.not_found"));
		}
		
		$validator = Validator::make($request->all(),
		[
			"name" => "required|string|max:255",
			"ic_num" => "required|string|max:128",
			"contact_num" => "required|string|max:128",
			"dob" => "required|date",
			"blood_type" => "required|string|max:32",
			"gender" => "required|string|max:32",
			"height" => "required|numeric",
			"weight" => "required|numeric",
			"addr_name" => "required|string|max:255",
			"addr_1" => "required|string",
			"addr_2" => "nullable|string",
			"zipcode" => "required|string|max:127",
			"area" => "required|string|max:255",
			"state" => "required|string|max:255",
			"country" => "required|string|max:255",
		]);
		$validator->setAttributeNames(
		[
			"name" => "Name",
			"ic_num" => "IC Number",
			"contact_num" => "Contact Number",
			"dob" => "Date of Birth",
			"blood_type" => "Blood Type",
			"gender" => "Gender",
			"height" => "Height",
			"weight" => "Weight",
			"addr_name" => "Address Name",
			"addr_1" => "Address Line 1",
			"addr_2" => "Address Line 2",
			"zipcode" => "Zipcode",
			"area" => "Area",
			"state" => "State",
			"country" => "Country",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::sendError("user_update.validation_fail", $validator->messages()->first());
		}
		
		if(!$user->updateRecord(
		[
			"name" => $request->input("name"),
			"ic_num" => $request->input("ic_num"),
			"contact_num" => $request->input("contact_num"),
			"dob" => $request->input("dob"),
			"blood_type" => $request->input("blood_type"),
			"gender" => $request->input("gender"),
			"height" => $request->input("height"),
			"weight" => $request->input("weight"),
		])) return ApiResponse::sendUpdateRecordError("user_update.update_fail");
		
		$record_data = 
		[
			"name" => $request->input("addr_name"),
			"addr_1" => $request->input("addr_1"),
			"addr_2" => $request->input("addr_2"),
			"zipcode" => $request->input("zipcode"),
			"area" => $request->input("area"),
			"state" => $request->input("state"),
			"country" => $request->input("country"),
		];
		
		$addr = $user->UserAddr;
		
		if(empty($addr))
		{
			$record_data["user_id"] = $user->id;
			
			$addr = new UserAddr;
			if(!$addr->createRecord($record_data)) return ApiResponse::sendCreateRecordError("user_update.address.create_fail");
		}
		else
		{
			if(!$addr->updateRecord($record_data)) return ApiResponse::sendUpdateRecordError("user_update.address.update_fail");
		}
		
		return ApiResponse::sendSuccessAck("user_update.success", trans("m.record.update_success"));
	}
	
	public function campaign_review_list(Request $request)
	{
		$validator = Validator::make($request->all(),
		[
			"skip" => "requiredWith:take|numeric|min:0",
			"take" => "requiredWith:skip|numeric|min:0",
			"user_id" => "required|numeric",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::sendError("user.campaign_review.list.validation_fail", $validator->messages()->first());
		}
		
		$user = User::id($request->input("user_id"))->first();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user.campaign_review.list.not_found", trans("m.record.not_found"));
		}
		
		$campaign_reviews = $user->UserCampaignReviewActive();
		
		if($request->has("skip") && $request->has("take")) $campaign_reviews->skip($request->input("skip"))->take($request->input("take"));
		$campaign_reviews = $campaign_reviews->get();
		
		$data = null;
		foreach($campaign_reviews as $key=>$campaign_review)
		{
			$data[$key] = $campaign_review->getAPIListData();
		}
		
		return ApiResponse::sendSuccess("user.campaign_review.list.success", $data);
	}
	
	public function has_campaign_appointment_needs_review(Request $request)
	{
		$user = Auth::user();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user.has.campaign_appointment_needs_review.not_found", trans("m.record.not_found"));
		}
		
		$appointment_needs_review = $user->CampaignAppointmentActive()
			->completed()
			->doesntHave("CampaignReview")
			->first();
		
		if(empty($appointment_needs_review)) return ApiResponse::sendSuccessAck("user.has.campaign_appointment_needs_review.success.no_appointment");
		
		return ApiResponse::sendSuccess("user.has.campaign_appointment_needs_review.success.has_appointment", $appointment_needs_review->getAPIListData());
	}
}