<?php

namespace App\Http\Controllers\Api\v1\User;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use Auth, Validator, Session, File, MessageBag, Config, Exception, Route, Mail, DB, Carbon\Carbon, Log, Crypt, Hash;
use Image;
use App\User;
use App\UserEmergencyContact;
use App\PresetEmergencyContact;

class UserEmergencyContactController extends Controller
{
	public function list(Request $request)
	{
		$user = Auth::user();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user_emergency_contact.list.not_found", trans("m.record.not_found"));
		}
		
		$validator = Validator::make($request->all(),
		[
			"skip" => "requiredWith:take|numeric|min:0",
			"take" => "requiredWith:skip|numeric|min:0",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::sendError("user_emergency_contact.list.validation_fail", $validator->messages()->first());
		}
		
		$user_emergency_contacts = $user->UserEmergencyContact();
		if($request->has("skip") && $request->has("take")) $user_emergency_contacts->skip($request->input("skip"))->take($request->input("take"));
		$user_emergency_contacts = $user_emergency_contacts->get();
		
		$data = null;
		foreach($user_emergency_contacts as $key=>$contact)
		{
			$data["user_emergency_contacts"][$key] = $contact->getAPIData();
		}
		
		foreach(PresetEmergencyContact::get() as $key=>$contact)
		{
			$data["preset_emergency_contacts"][$key] = $contact->getAPIData();
		}
		
		return ApiResponse::sendSuccess("user_emergency_contact.list.success", $data);
	}
	
	public function create(Request $request)
	{
		$user = Auth::user();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user_emergency_contact.create.not_found", trans("m.record.not_found"));
		}
		
		$validator = Validator::make($request->all(),
		[
			"priority" => "required|numeric",
			"name" => "required|string|max:255",
			"contact_num" => "required|string|max:127",
		]);
		$validator->setAttributeNames(
		[
			"priority" => "Priority",
			"name" => "Name",
			"contact_num" => "Contact Number",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::sendError("user_emergency_contact.create.validation_fail", $validator->messages()->first());
		}
		
		$user_emergency_contacts = $user->UserEmergencyContact;
		
		if(!empty($user_emergency_contacts) && count($user_emergency_contacts) >= env("MAX_USER_EMERGENCY_CONTACT", 3))
		{
			return ApiResponse::sendError("user_emergency_contact.create.max_reached", trans("m.user_emergency_contact.max_reached"));
		}
		
		$user_emergency_contact = new UserEmergencyContact;
		if(!$user_emergency_contact->createRecord(
		[
			"user_id" => $user->id,
			"priority" => $request->input("priority"),
			"name" => $request->input("name"),
			"contact_num" => $request->input("contact_num"),
		])) return ApiResponse::sendCreateRecordError("user_emergency_contact.create.create_fail");
		
		return ApiResponse::sendSuccess("user_emergency_contact.create.success", $user_emergency_contact->getAPIData(), trans("m.record.create_success"));
	}
	
	public function update(Request $request)
	{
		$user = Auth::user();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user_emergency_contact.update.user_not_found", trans("m.record.not_found"));
		}
		
		$validator = Validator::make($request->all(),
		[
			"id" => "required|numeric",
			"priority" => "required|numeric",
			"name" => "required|string|max:255",
			"contact_num" => "required|string|max:127",
		]);
		$validator->setAttributeNames(
		[
			"id" => "ID",
			"priority" => "Priority",
			"name" => "Name",
			"contact_num" => "Contact Number",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::sendError("user_emergency_contact.update.validation_fail", $validator->messages()->first());
		}
		
		$user_emergency_contact = UserEmergencyContact::id($request->input("id"))->first();
		
		if(empty($user_emergency_contact))
		{
			return ApiResponse::sendError("user_emergency_contact.update.not_found", trans("m.record.not_found"));
		}
		
		if(!$user_emergency_contact->updateRecord(
		[
			"priority" => $request->input("priority"),
			"name" => $request->input("name"),
			"contact_num" => $request->input("contact_num"),
		])) return ApiResponse::sendUpdateRecordError("user_emergency_contact.update.update_fail");
		
		return ApiResponse::sendSuccessAck("user_emergency_contact.update.success", trans("m.record.update_success"));
	}
	
	public function delete(Request $request)
	{
		$user = Auth::user();
		
		if(empty($user))
		{
			return ApiResponse::sendError("user_emergency_contact.delete.user_not_found", trans("m.record.not_found"));
		}
		
		$validator = Validator::make($request->all(),
		[
			"id" => "required|numeric",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::sendError("user_emergency_contact.delete.validation_fail", $validator->messages()->first());
		}
		
		$user_emergency_contact = UserEmergencyContact::id($request->input("id"))->first();
		
		if(empty($user_emergency_contact))
		{
			return ApiResponse::sendError("user_emergency_contact.delete.not_found", trans("m.record.not_found"));
		}
		
		if(!$user_emergency_contact->deleteRecord()) return ApiResponse::sendDeleteRecordError("user_emergency_contact.delete.delete_fail");
		
		return ApiResponse::sendSuccessAck("user_emergency_contact.delete.success", trans("m.record.delete_success"));
	}
}