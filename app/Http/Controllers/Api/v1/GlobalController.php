<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use Auth, Validator, Session, File, MessageBag, Config, Exception, Route, Mail, DB, Carbon\Carbon, Log;
use App\WebConfig;
use App\Banner;
use App\State;
use App\Area;
use App\BloodType;

class GlobalController extends Controller
{
    public function hospital_list_radius(Request $request)
	{
		return ApiResponse::sendSuccess("global.hospital.list.radius.success",
		[
			"google_api_key" => env("APP_GOOGLE_API_KEY"),
			"radius" => WebConfig::getValue("hospital_list_radius"),
		]);
	}
	
	public function banner_list(Request $request)
	{
		$validator = Validator::make($request->all(),
		[
			"skip" => "requiredWith:take|numeric|min:0",
			"take" => "requiredWith:skip|numeric|min:0",
		]);
		
		if($validator->fails())
		{
			return ApiResponse::sendError("global.banner.list.validation_fail", $validator->messages()->first());
		}
		
		$banners = Banner::active();
		if($request->has("skip") && $request->has("take")) $banners->skip($request->input("skip"))->take($request->input("take"));
		$banners = $banners->get();
		
		$data = null;
		foreach($banners as $key=>$banner)
		{
			$data[$key] = $banner->getAPIData();
		}
		
		return ApiResponse::sendSuccess("global.banner.list.success", $data);
	}
	
    public function state_area_list(Request $request)
	{
		$states = State::active()->get();
		$areas = Area::active()->get();
		
		$data = null;
		
		foreach($states as $key=>$state)
		{
			$temp = $state->getAPIListData();
			
			$temp["areas"] = [];
			foreach($state->AreaActive as $area)
			{
				array_push($temp["areas"], $area->getAPIListData());
			}
			
			$data[$key] = $temp;
		}
		
		return ApiResponse::sendSuccess("global.state_area.list.success", $data);
	}
	
    public function blood_type_list(Request $request)
	{
		$blood_types = BloodType::get();
		
		$data = null;
		foreach($blood_types as $key=>$blood_type)
		{
			$data[$key] = $blood_type->getAPIListData();
		}
		
		return ApiResponse::sendSuccess("global.blood_type.list.success", $data);
	}
}
