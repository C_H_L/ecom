<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use File, Log, Carbon\Carbon;

class PasswordResetToken extends Model
{
	protected $appends =
	[
		"created_at_formatted",
		"used_text",
	];
	
	protected static function boot()
	{
		parent::boot();
		
		static::addGlobalScope("orderBy", function(Builder $builder)
		{
			$builder->orderBy("password_reset_tokens.created_at", "DESC");
		});
	}
	
	//CURD
	public function createRecord($data)
	{
		try
		{
			foreach($data as $key => $value)
			{
				$this->$key = $value;
			}
			$this->save();
			
			return $this->id;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			return false;
		}
	}
	
	public function updateRecord($data)
	{
		try
		{
			foreach($data as $key => $value)
			{
				$this->$key = $value;
			}
			$this->save();
			
			return true;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			
			return false;
		}
	}
	
	public function deleteRecord()
	{
		try
		{
			$this->delete();
			
			return true;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			
			return false;
		}
	}
	
	//Relationships
	public function User()
	{
		return $this->belongsTo("App\User");
	}
	public function UserActive()
	{
		return $this->User()->active();
	}
	
	//Scopes
	public function scopeId($query, $id)
	{
		return $query->where("id", $id);
	}
	
	public function scopeActive($query)
	{
		return $query->where("used", 0)
			->where("created_at", ">", Carbon::now()->subMinutes(env("PRT_EXPIRY")))
			->has("UserActive");
	}
	
	//Attributes
	public function getCreatedAtFormattedAttribute()
	{
		return date(env("GLOBAL_DATE_FORMAT"), strtotime($this->created_at));
	}
	
	public function getUsedTextAttribute()
	{
		switch($this->used)
		{
			case 0:
				return "Not Used";
				break;
			case 1:
				return "Used";
				break;
		}
	}
	
	//Functions
	public function isActive()
	{
		if($this->used == 0
		&& Carbon::parse($this->created_at)->greaterThan(Carbon::now()->subMinutes(env("PRT_EXPIRY")))) return true;
		return false;
	}
}
