<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use File, Log;

class WebConfig extends Model
{
	protected $appends =
	[
		"created_at_formatted",
		"active_text",
		"value_name",
	];
	
	protected static function boot()
	{
		parent::boot();
		
		static::addGlobalScope("orderBy", function(Builder $builder)
		{
			$builder->orderBy("web_configs.created_at", "DESC");
		});
	}
	
	//CURD
	public function createRecord($data)
	{
		try
		{
			foreach($data as $key => $value)
			{
				$this->$key = $value;
			}
			$this->save();
			
			return $this->id;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			return false;
		}
	}
	
	public function updateRecord($data)
	{
		try
		{
			foreach($data as $key => $value)
			{
				$this->$key = $value;
			}
			$this->save();
			
			return true;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			
			return false;
		}
	}
	
	public function deleteRecord()
	{
		try
		{
			$this->delete();
			
			return true;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			
			return false;
		}
	}
	
	//Relationships
	
	//Scopes
	public function scopeActive($query)
	{
		return $query->where("active", 1);
	}
	
	public function scopeId($query, $id)
	{
		return $query->where("id", $id);
	}
	
	//Attributes
	public function getCreatedAtFormattedAttribute()
	{
		return date(env("GLOBAL_DATE_FORMAT"), strtotime($this->created_at));
	}
	
	public function getActiveTextAttribute()
	{
		switch($this->active)
		{
			case 0:
				return "Inactive";
				break;
			case 1:
				return "Active";
				break;
		}
	}
	
	public function getValueNameAttribute($value)
	{
		if(empty($this->value_map)) return null;
		return json_decode($this->value_map)[$this->value];
	}
	
	//Functions
	public static function getValue($name, $asName = true, $withUnit = false)
	{
		$config = WebConfig::where("name", $name)->active()->first();
		
		if(!empty($config)) return (($asName && !empty($config->value_name))? $config->value_name : $config->value) . ($withUnit ? " " . $config->unit : '');
		return null;
	}
	
	public function getValueName($value)
	{
		if(empty($this->value_map)) return null;
		return json_decode($this->value_map)[$this->value];
	}
	
	public function getValueNames()
	{
		if(empty($this->value_map)) return null;
		
		return json_decode($this->value_map);
	}
	
	public static function calculateTax($amount)
	{
		return WebConfig::getValue("tax") * $amount;
	}
	
	public static function calculateShippingFee($amount)
	{
		if(WebConfig::getValue("shipping_fee_formula", false) == 0) return WebConfig::getValue("shipping_fee") * $amount;
		return WebConfig::getValue("shipping_fee");
	}
}
