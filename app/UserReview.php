<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use Log;

class UserReview extends Model
{
	protected $appends =
	[
		"created_at_formatted",
		"active_text",
	];
	
	protected static function boot()
	{
		parent::boot();
		
		static::addGlobalScope("orderBy", function(Builder $builder)
		{
			$builder->orderBy("user_reviews.created_at", "DESC");
		});
	}
	
	//CURD
	public function createRecord($data)
	{
		try
		{
			foreach($data as $key => $value)
			{
				$this->$key = $value;
			}
			$this->save();
			
			return $this->id;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			return false;
		}
	}
	
	public function updateRecord($data)
	{
		try
		{
			foreach($data as $key => $value)
			{
				$this->$key = $value;
			}
			$this->save();
			
			return true;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			
			return false;
		}
	}
	
	public function deleteRecord()
	{
		try
		{
			//Delete this record
			$this->delete();
			
			return true;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			
			return false;
		}
	}
	
	//Relationships
	public function User()
	{
		return $this->belongsTo("App\User");
	}
	
	public function UserActive()
	{
		return $this->User()->active();
	}
	
	public function Doctor()
	{
		return $this->belongsTo("App\Doctor");
	}
	
	public function DoctorActive()
	{
		return $this->Doctor()->active();
	}
	
	public function Appointment()
	{
		return $this->belongsTo("App\Appointment");
	}
	
	public function AppointmentActive()
	{
		return $this->Appointment()->active();
	}
	
	//Scopes
	public function scopeId($query, $id)
	{
		return $query->where("id", $id);
	}
	
	public function scopeActive($query)
	{
		return $query->where("active", 1)
			->has("UserActive");
	}
	
	//Attributes
	public function getCreatedAtFormattedAttribute()
	{
		return date(env("GLOBAL_DATE_FORMAT"), strtotime($this->created_at));
	}
	
	public function getActiveTextAttribute()
	{
		switch($this->active)
		{
			case 0:
				return "Inactive";
				break;
			case 1:
				return "Active";
				break;
		}
	}
	
	//Functions
	public function getAPIListData()
	{
		return
		[
			"id" => (int)$this->id,
			"doctor" => $this->Doctor->getAPIListData(),
			"rating" => (double)$this->rating,
			"review" => $this->review,
			"created_at_formatted" => $this->created_at_formatted,
		];
	}
}
