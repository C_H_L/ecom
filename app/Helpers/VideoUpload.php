<?php
	namespace App\Helpers;
	
	use Illuminate\Http\Request;
	use File;
	
	class VideoUpload
	{
		public static function store($path, $file, $file_name = null)
		{
			if(empty($file_name))
			{
				$file_name = "video_" . date("Ymdhis") . "_" . rand(000000, 999999) . "." . strtolower($file->getClientOriginalExtension());
			}
			else
			{
				$file_name = $file_name . "_" . date("Ymdhis") . "_" . rand(000000, 999999) . "." . strtolower($file->getClientOriginalExtension());
			}
			
			if(!File::exists($path)) File::makeDirectory($path, 0755, true);
			$file->move(public_path() . "/" . $path, $file_name);
			
			if(!File::exists($path . "mobile/")) File::makeDirectory($path . "mobile/", 0755, true);
			$file->move(public_path() . "/" . $path . "mobile", $file_name);
			
			return $file_name;
		}
	}
?>