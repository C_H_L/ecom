<?php
	namespace App\Helpers;
	
	use Illuminate\Http\Request;
	use File, Image;
	
	class ImageUpload
	{
		public static function store($path, $file, $size_name, $file_name = null)
		{
			$img_web = Image::make($file);
			$img_mobile = Image::make($file);
			
			$img_web->resize(config("image_sizes." . $size_name . ".web"), null, function($c)
			{
				$c->aspectRatio();
			});
			$img_mobile->resize(config("image_sizes." . $size_name . ".mobile"), null, function($c)
			{
				$c->aspectRatio();
			});
			
			switch($img_web->mime())
			{
				case "image/jpg":
					$extension = "jpg";
					break;
				case "image/jpeg":
					$extension = "jpeg";
					break;
				case "image/png":
					$extension = "png";
					break;
				case "image/bmp":
					$extension = "bmp";
					break;
				case "image/gif":
					$extension = "gif";
					break;
				case "image/svg":
					$extension = "svg";
					break;
			}
			
			if(empty($file_name))
			{
				$file_name = "image_" . date("Ymdhis") . "_" . rand(000000, 999999) . "." . $extension;
			}
			else
			{
				$file_name = $file_name . "_" . date("Ymdhis") . "_" . rand(000000, 999999) . "." . $extension;
			}
			
			if(!File::exists($path)) File::makeDirectory($path, 0755, true);
			$img_web->save(public_path() . "/" . $path . $file_name);
			
			if(!File::exists($path . "mobile/")) File::makeDirectory($path . "mobile/", 0755, true);
			$img_mobile->save(public_path() . "/" . $path . "mobile/" . $file_name);
			
			return $file_name;
		}
	}
?>