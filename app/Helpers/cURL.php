<?php
	namespace App\Helpers;
	
	use Illuminate\Http\Request;
	use Log;
	
	class cURL
	{
		public static function send($url, $post_fields = null, $headers = null)
		{
			$curl = curl_init();
			
			curl_setopt_array($curl,
			[
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_FOLLOWLOCATION => true,
			]);
			if(!empty($headers)) curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
			if(!empty($post_fields)) curl_setopt($curl, CURLOPT_POST, true);
			if(!empty($post_fields)) curl_setopt($curl, CURLOPT_POSTFIELDS, $post_fields);
			
			$response = curl_exec($curl);
			$error = curl_error($curl);
			
			curl_close($curl);
			
			if($error)
			{
				Log::error("cURL Error: " . $error);
				return false;
			}
			
			return $response;
		}
	}
?>