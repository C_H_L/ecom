<?php
	namespace App\Helpers;
	
	use Illuminate\Support\Str;
	use File, QrCode;
	
	class QRGenerator
	{
		public static function generateDoctorUUID()
		{
			$uuid = (string)Str::uuid();
			
			return
			[
				"uuid" => $uuid,
				"qr_img" => QRGenerator::generateDoctorQR($uuid),
			];
		}
		
		public static function generateDoctorQR($uuid)
		{
			$file_name = "profile_qr_" . date("Ymdhis") . "_" . rand(000, 999) . ".png";
			$url = route("app.doctor.profile", ["uuid" => $uuid]);
			
			if(!File::exists(config("assetpath.doctor_qr_img") . "mobile/")) File::makeDirectory(config("assetpath.doctor_qr_img") . "mobile/", 0755, true);
			if(!File::exists(config("assetpath.doctor_qr_img"))) File::makeDirectory(config("assetpath.doctor_qr_img"), 0755, true);
			
			QrCode::format("png")->size(config("image_sizes.profile_qr_img.web"))->generate($url, config("assetpath.doctor_qr_img") . $file_name);
			QrCode::format("png")->size(config("image_sizes.profile_qr_img.mobile"))->generate($url, config("assetpath.doctor_qr_img") . "mobile/" . $file_name);
			
			return $file_name;
		}
		
		public static function generateCampaignUUID()
		{
			$uuid = (string)Str::uuid();
			
			return
			[
				"uuid" => $uuid,
				"qr_img" => QRGenerator::generateCampaignQR($uuid),
			];
		}
		
		public static function generateCampaignQR($uuid)
		{
			$file_name = "profile_qr_" . date("Ymdhis") . "_" . rand(000, 999) . ".png";
			$url = route("app.campaign.profile", ["uuid" => $uuid]);
			
			if(!File::exists(config("assetpath.campaign_qr_img") . "mobile/")) File::makeDirectory(config("assetpath.campaign_qr_img") . "mobile/", 0755, true);
			if(!File::exists(config("assetpath.campaign_qr_img"))) File::makeDirectory(config("assetpath.campaign_qr_img"), 0755, true);
			
			QrCode::format("png")->size(config("image_sizes.profile_qr_img.web"))->generate($url, config("assetpath.campaign_qr_img") . $file_name);
			QrCode::format("png")->size(config("image_sizes.profile_qr_img.mobile"))->generate($url, config("assetpath.campaign_qr_img") . "mobile/" . $file_name);
			
			return $file_name;
		}
	}
?>