<?php
	namespace App\Helpers;
	
	use Illuminate\Http\Request;
	use App\Helpers\cURL;
	
	use App\User;
	use App\UserNotification;
	use App\DoctorNotification;
	
	class PushNotification
	{
		public static function pushToUser($notification_id)
		{
			$notification = UserNotification::id($notification_id)->first();
			$user = $notification->User;
			
			if(!empty($user) && !empty($user->fcm_token))
			{
				$notification =
				[
					"title" => $notification->title,
					"body" => $notification->description,
					"badge" => 1,
				];
				
				$data =
				[
					"notification_id" => $notification_id
				];
				
				return PushNotification::fcmSend($user->fcm_token, $notification, $data);
			}
		}
		
		static function fcmSend($fcm_token, $notification, $data = null)
		{
			$fcm_options = 
			[
				"registration_ids" => [$fcm_token],
				"content_available" => true,
				"priority" => "high",
				"notification" => $notification,
				"data" => $data,
				"badge" => 1,
			];
			
			return cURL::send("https://fcm.googleapis.com/fcm/send", json_encode($fcm_options),
			[
				"Authorization: key=" . env("FCM_KEY"),
				"Content-Type: application/json",
			]);
		}
		
		public static function fcmToUser($user_id, $title, $desc, $data = null)
		{
			$user = User::id($user_id)->first();
			
			$fcm_options = 
			[
				"registration_ids" => [$user->fcm_token],
				"content_available" => true,
				"priority" => "high",
				"notification" =>
				[
					"title" => $title,
					"body" => $desc,
					"badge" => 1,
				],
				"data" => $data,
				"badge" => 1,
			];
			
			return cURL::send("https://fcm.googleapis.com/fcm/send", json_encode($fcm_options),
			[
				"Authorization: key=" . env("FCM_KEY"),
				"Content-Type: application/json",
			]);
		}
	}
?>