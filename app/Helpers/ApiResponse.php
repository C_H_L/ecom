<?php
	namespace App\Helpers;
	
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	
	class ApiResponse
	{
		//Class Helpers
		/*public static function parseErrorCode($code)
		{
			if(is_array($code)) return config("response_code." . $code[0] . ".code") + $code[1];
			if(is_string($code)) return config("response_code." . $code . ".code");
			return $code;
		}*/
		
		//Class functions
		public static function returnSuccess($data = null, $message = null, $code = null)
		{
			$response =
			[
				"status" => 1,
				"code" => $code,
				"message" => $message,
				"data" => $data,
			];
			
			return response()
				->json($response, 200);
		}
		
		public static function returnError($message, $code = null)
		{
			$response =
			[
				"status" => 0,
				"code" => $code,
				"message" => $message,
				"data" => null,
			];
			
			return response()
				->json($response, 200);
		}
		
		public static function returnCreateRecordError($code = null)
		{
			$response =
			[
				"status" => 0,
				"code" => $code,
				"message" => trans("m.record.create_fail"),
				"data" => null,
			];
			
			return response()
				->json($response, 200);
		}
		
		public static function returnUpdateRecordError($code = null)
		{
			$response =
			[
				"status" => 0,
				"code" => $code,
				"message" => trans("m.record.update_fail"),
				"data" => null,
			];
			
			return response()
				->json($response, 200);
		}
		
		public static function returnDeleteRecordError($code = null)
		{
			$response =
			[
				"status" => 0,
				"code" => $code,
				"message" => trans("m.record.delete_fail"),
				"data" => null,
			];
			
			return response()
				->json($response, 200);
		}
	}
?>