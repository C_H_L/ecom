<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Builder;

use Illuminate\Encryption\Encrypter;

use App\Helpers\ImageUpload;

use Crypt, Log, File, Carbon\Carbon;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	protected $appends =
	[
		"created_at_formatted",
		"member_since",
		"active_text",
		"profile_img_asset",
		"mobile_profile_img_asset",
		"age",
	];
	
	protected static function boot()
	{
		parent::boot();
		
		static::addGlobalScope("orderBy", function(Builder $builder)
		{
			$builder->orderBy("users.created_at", "DESC");
		});
	}
	
	//CURD
	public function createRecord($data)
	{
		try
		{
			if(!array_key_exists("encryption_key", $data)) $data["encryption_key"] = base64_encode(Encrypter::generateKey(config("app.cipher")));
			
			foreach($data as $key => $value)
			{
				if($key == "profile_img")
				{
					$file_name = ImageUpload::store(config("assetpath.user_profile_img"), $value, "profile_img", "profile_image");
					
					/*$img_web = Image::make($value);
					$img_mobile = Image::make($value);
					
					$img_web->resize(config("image_sizes.profile_img.web"), null, function($c)
					{
						$c->aspectRatio();
					});
					$img_mobile->resize(config("image_sizes.profile_img.web"), null, function($c)
					{
						$c->aspectRatio();
					});
					
					if(!File::exists(config("assetpath.profile_img"))) File::makeDirectory(config("assetpath.profile_img"), 0755, true);
					$img_web->save(public_path() . "/" . config("assetpath.profile_img") . $profile_img);
					$img_web->save(public_path() . "/" . config("assetpath.profile_img") . $profile_img);*/
					
					$this->$key = $file_name;
				}
				else
				{
					$this->$key = $value;
				}
			}
			$this->save();
			
			return $this->id;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			return false;
		}
	}
	
	public function updateRecord($data)
	{
		try
		{
			foreach($data as $key => $value)
			{
				if($key == "profile_img")
				{
					$file_name = ImageUpload::store(config("assetpath.user_profile_img"), $value, "profile_img", "profile_image");
					
					/*$img_web = Image::make($value);
					$img_mobile = Image::make($value);
					
					$img_web->resize(config("image_sizes.profile_img.web"), null, function($c)
					{
						$c->aspectRatio();
					});
					$img_mobile->resize(config("image_sizes.profile_img.web"), null, function($c)
					{
						$c->aspectRatio();
					});
					
					if(!File::exists(config("assetpath.profile_img"))) File::makeDirectory(config("assetpath.profile_img"), 0755, true);
					$img_web->save(public_path() . "/" . config("assetpath.profile_img") . $profile_img);
					$img_web->save(public_path() . "/" . config("assetpath.profile_img") . $profile_img);*/
					
					$this->$key = $file_name;
				}
				else
				{
					$this->$key = $value;
				}
			}
			$this->save();
			
			return true;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			
			return false;
		}
	}
	
	public function deleteRecord()
	{
		try
		{
			//Delete relationships
			foreach($this->UserAddr as $record)
			{
				$record->deleteRecord();
			}
			
			foreach($this->UserNotification as $record)
			{
				$record->deleteRecord();
			}
			
			foreach($this->UserReview as $record)
			{
				$record->deleteRecord();
			}
			
			//Delete Files
			if(File::exists(config("assetpath.user_profile_img") . $this->profile_img)) File::delete(config("assetpath.user_profile_img") . $this->profile_img);
			
			//Delete this record
			$this->revokeAccessTokens();
			$this->delete();
			
			return true;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			
			return false;
		}
	}
	
	//Relationships
	public function UserAddr()
	{
		return $this->hasOne("App\UserAddr");
	}
	
	public function UserNotification()
	{
		return $this->hasMany("App\UserNotification");
	}
	
	public function UserReview()
	{
		return $this->hasMany("App\UserReview");
	}
	
	public function UserReviewActive()
	{
		return $this->UserReview()->active();
	}
	
	//Scopes
	public function scopeId($query, $id)
	{
		return $query->where("id", $id);
	}
	
	public function scopeActive($query)
	{
		return $query->where("active", 1);
	}
	
	//Attributes
	public function getCreatedAtFormattedAttribute()
	{
		return date(env("GLOBAL_DATE_FORMAT"), strtotime($this->created_at));
	}
	
	public function getMemberSinceAttribute()
	{
		return Carbon::parse($this->created_at)->format("F Y");
	}
	
	public function getActiveTextAttribute()
	{
		switch($this->active)
		{
			case 0:
				return "Inactive";
				break;
			case 1:
				return "Active";
				break;
		}
	}
	
	public function getProfileImgAssetAttribute()
	{
		if(empty($this->profile_img)) return asset(config("assetpath.placeholder.profile_img"));
		return asset(config("assetpath.user_profile_img") . $this->profile_img);
	}
	
	public function getMobileProfileImgAssetAttribute()
	{
		if(empty($this->profile_img)) return asset(config("assetpath.placeholder.profile_img"));
		return asset(config("assetpath.user_profile_img") . "mobile/" . $this->profile_img);
	}
	
	public function getRatingAttribute()
	{
		$count = count($this->UserReviewActive);
		$rating = 0;
		
		if($count > 0)
		{
			foreach($this->UserReviewActive as $review)
			{
				$rating += $review->rating;
			}
			
			$rating = number_format($rating / $count, 2);
		}
		
		return $rating;
	}
	
	public function getAgeAttribute()
	{
		return Carbon::now()->diffInYears(Carbon::parse($this->dob));
	}
	
	//Function
	public function getAPIListData()
	{
		return
		[
			"id" => $this->id,
			"name" => $this->name,
			"email" => $this->email,
			"profile_img" => $this->mobile_profile_img_asset,
			"contact_num" => $this->contact_num,
			"rating" => $this->rating,
			"campaign_rating" => $this->campaign_rating,
			"overall_rating" => $this->overall_rating,
			"review_count" => $this->review_count,
			"campaign_review_count" => $this->campaign_review_count,
			"overall_review_count" => $this->overall_review_count,
		];
	}
	
	public function getAPIData()
	{
		return
		[
			"id" => $this->id,
			"name" => $this->name,
			"email" => $this->email,
			"profile_img" => $this->mobile_profile_img_asset,
			"ic_num" => $this->ic_num,
			"contact_num" => $this->contact_num,
			"dob" => $this->dob,
			"blood_type" => $this->blood_type,
			"gender" => $this->gender,
			"height" => $this->height,
			"weight" => $this->weight,
			"created_at_formatted" => $this->created_at_formatted,
			"rating" => $this->rating,
			"campaign_rating" => $this->campaign_rating,
			"overall_rating" => $this->overall_rating,
			"review_count" => $this->review_count,
			"campaign_review_count" => $this->campaign_review_count,
			"overall_review_count" => $this->overall_review_count,
		];
	}
	
	public function revokeAccessTokens()
	{
		foreach($this->tokens as $token)
		{
			$token->revoke();
		}
	}
	
	public function hasDetails()
	{
		if(!empty($this->ic_num)
			&& !empty($this->contact_num)
			&& !empty($this->dob)
			&& !empty($this->blood_type)
			&& !empty($this->gender)
			&& !empty($this->height)
			&& !empty($this->weight)
			&& !empty($this->UserAddr)) return true;
		return false;
	}
	
	public function validatePassword($pass)
	{
		//Alpha-numeric, lowercase, uppercase
		if(!(preg_match('/[a-z]/',  $pass) && preg_match('/[A-Z]/', $pass) && preg_match('/[0-9]/', $pass)))
		{
			return false;
		}
		
		//8 letters minimum
		if(strlen($pass) < 8)
		{
			return false;
		}
		
		return true;
	}
}
