<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use App\Helpers\PushNotification;
use Log, Mail;

class UserNotification extends Model
{
	protected $appends =
	[
		"created_at_formatted",
		"age_text",
	];
	
	protected static function boot()
	{
		parent::boot();
		
		static::addGlobalScope("orderBy", function(Builder $builder)
		{
			$builder->orderBy("user_notifications.created_at", "DESC");
		});
	}
	
	//CURD
	public function createRecord($data)
	{
		try
		{
			foreach($data as $key => $value)
			{
				$this->$key = $value;
			}
			$this->save();
			
			if(!PushNotification::pushToUser($this->id)) return false;
			if(!$this->sendEmail()) return false;
			
			return $this->id;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			return false;
		}
	}
	
	public function updateRecord($data)
	{
		try
		{
			foreach($data as $key => $value)
			{
				$this->$key = $value;
			}
			$this->save();
			
			return true;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			
			return false;
		}
	}
	
	public function deleteRecord()
	{
		try
		{
			//Delete this record
			$this->delete();
			
			return true;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			
			return false;
		}
	}
	
	//Helper
	private function sendEmail()
	{
		$notification = $this;
		$user = $this->User;
		
		Mail::send("Api.emails.user_notification", compact('user', 'notification'), function($msg) use($notification, $user)
		{
			$msg->subject($notification->title)
				->to($user->email, $user->name);
		});
		
		return true;
	}
	
	//Relationships
	public function User()
	{
		return $this->belongsTo("App\User");
	}
	
	public function UserActive()
	{
		return $this->User()->active();
	}
	
	//Scopes
	public function scopeId($query, $id)
	{
		return $query->where("id", $id);
	}
	
	public function scopeActive($query)
	{
		return $query->has("UserActive");
	}
	
	public function scopeIsRead($query, $val = 0)
	{
		return $query->where("is_read", $val);
	}
	
	public function scopeRead($query)
	{
		return $query->where("is_read", 1);
	}
	
	public function scopeUnread($query)
	{
		return $query->where("is_read", 0);
	}
	
	//Attributes
	public function getCreatedAtFormattedAttribute()
	{
		return date(env("GLOBAL_DATE_FORMAT"), strtotime($this->created_at));
	}
	
	public function getAgeTextAttribute()
	{
		return Carbon::parse($this->created_at)->diffForHumans();
	}
	
	//Functions
	public function getAPIListData()
	{
		return
		[
			"id" => $this->id,
			"title" => $this->title,
			"description" => $this->description,
			"is_read" => $this->is_read,
			"created_at" => $this->created_at->toDateTimeString(),
			"created_at_formatted" => $this->created_at_formatted,
		];
	}
}
