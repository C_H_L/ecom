<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use Log;

class UserAddr extends Model
{
	protected $appends =
	[
		"created_at_formatted",
	];
	
	protected static function boot()
	{
		parent::boot();
		
		static::addGlobalScope("orderBy", function(Builder $builder)
		{
			$builder->orderBy("user_addrs.created_at", "DESC");
		});
	}
	
	//CURD
	public function createRecord($data)
	{
		try
		{
			foreach($data as $key => $value)
			{
				$this->$key = $value;
			}
			$this->save();
			
			return $this->id;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			return false;
		}
	}
	
	public function updateRecord($data)
	{
		try
		{
			foreach($data as $key => $value)
			{
				$this->$key = $value;
			}
			$this->save();
			
			return true;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			
			return false;
		}
	}
	
	public function deleteRecord()
	{
		try
		{
			//Delete this record
			$this->delete();
			
			return true;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			
			return false;
		}
	}
	
	//Relationships
	public function User()
	{
		return $this->belongsTo("App\User");
	}
	
	public function UserActive()
	{
		return $this->User()->active();
	}
	
	//Scopes
	public function scopeId($query, $id)
	{
		return $query->where("id", $id);
	}
	
	public function scopeActive($query)
	{
		return $query->has("UserActive");
	}
	
	//Attributes
	public function getCreatedAtFormattedAttribute()
	{
		return date(env("GLOBAL_DATE_FORMAT"), strtotime($this->created_at));
	}
	
	//Functions
	public function getAPIData()
	{
		return
		[
			"id" => $this->id,
			"name" => $this->name,
			"addr_1" => $this->addr_1,
			"addr_2" => $this->addr_2,
			"zipcode" => $this->zipcode,
			"area" => $this->area,
			"state" => $this->state,
			"country" => $this->country,
			"addr_text" => $this->text("\n"),
			"created_at_formatted" => $this->created_at_formatted,
		];
	}
	
	public function text($breakChar = "<br />")
	{
		return $this->name . "," . $breakChar
			. $this->addr_1 . "," . $breakChar
			. $this->addr_2 . "," . $breakChar
			. $this->zipcode . " " . $this->area . "," . $breakChar
			. $this->state . " " . $this->country;
	}
}
