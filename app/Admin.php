<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Crypt;

class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	protected $appends =
	[
		"created_at_formatted",
		"active_text",
	];
	
	protected static function boot()
	{
		parent::boot();
		
		static::addGlobalScope("orderBy", function(Builder $builder)
		{
			$builder->orderBy("admins.created_at", "DESC");
		});
	}
	
	//CURD
	public function createRecord($data)
	{
		try
		{
			foreach($data as $key => $value)
			{
				$this->$key = $value;
			}
			$this->save();
			
			return $this->id;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			return false;
		}
	}
	
	public function updateRecord($data)
	{
		try
		{
			foreach($data as $key => $value)
			{
				$this->$key = $value;
			}
			$this->save();
			
			return true;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			
			return false;
		}
	}
	
	public function deleteRecord()
	{
		try
		{
			$this->delete();
			
			return true;
		}
		catch(\Exception $e)
		{
			Log::error($e);
			
			return false;
		}
	}
	
	//Scopes
	public function scopeActive($query)
	{
		return $query->where("active", 1);
	}
	
	public function scopeId($query, $id)
	{
		return $query->where("id", $id);
	}
	
	//Attributes
	public function getCreatedAtFormattedAttribute()
	{
		return date(env("Global_DATE_FORMAT"), strtotime($this->created_at));
	}
	
	public function getActiveTextAttribute()
	{
		switch($this->active)
		{
			case 0:
				return "Inactive";
				break;
			case 1:
				return "Active";
				break;
		}
	}
	
	
}
