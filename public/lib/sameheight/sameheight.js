(function($)
{
	$.fn.runUnhidden = function(func)
	{
		//Run on self first
		if($(this).css("display") == "none")
		{
			if($(this).attr("style") == null || $(this).attr("style").indexOf("display") == -1)
			{
				$(this).attr("data-unhidden-hideafter", "1");
			}
			else
			{
				$(this).attr("data-unhidden-hideafter", "2");
			}
			
			$(this).css("display", "block");
		}
		
		//Then run on all parents
		$(this).parents().each(function()
		{
			if($(this).css("display") == "none")
			{
				if($(this).attr("style") == null || $(this).attr("style").indexOf("display") == -1)
				{
					$(this).attr("data-unhidden-hideafter", "1");
				}
				else
				{
					$(this).attr("data-unhidden-hideafter", "2");
				}
				$(this).css("display", "block");
			}
		});
		
		func();
		
		$("[data-unhidden-hideafter]").each(function()
		{
			//$(this).removeAttr("data-unhidden-hideafter");
			if($(this).attr("style") == null || $(this).attr("style").indexOf("display") == -1)
			{
				$(this).css("display", "");
			}
			else
			{
				$(this).css("display", "none");
			}
			$(this).attr("data-unhidden-hideafter", "");
		});
	};
})(jQuery);

var lgScreenSize = 1200;
var mdScreenSize = 992;
var smScreenSize = 768;
var xsScreenSize = 768;
var observeDOMTree = false;

$(document).ready(function()
{
	recalcAllHeight();
	
	$(window).resize(function()
	{
		recalcAllHeight();
		
		$(window).trigger("sameheight_Resize");
	});

	$(window).on("load", function()
	{
		recalcAllHeight();
		observeDOMTree = true;
		
		$(window).trigger("sameheight_Load");
	});
	
	$(document).bind("DOMSubtreeModified", function()
	{
		if(observeDOMTree)
		{
			recalcAllHeight();
		}
	});
	
	$(".keep-height").each(function()
	{
		$(this).css("height", $(this).innerHeight());
	});
	
	$(".keep-width").each(function()
	{
		$(this).css("width", $(this).innerWidth());
	});
});

function recalcAllHeight()
{
	$(".same-height").each(function()
	{
		$(this).css("height", "");
	});
	
	$(".same-height").each(function()
	{
		if(/*($(this).hasClass("same-height-lg") && $(window).width() <= lgScreenSize)
			|| */($(this).hasClass("same-height-md") && $(window).width() < mdScreenSize)
			|| ($(this).hasClass("same-height-sm") && $(window).width() < smScreenSize)
			|| ($(this).hasClass("same-height-sm") && $(window).width() < xsScreenSize))
		{
			return;
		}
		
		if($(this).attr("data-sameheight") != null)
		{
			var highest = 0;
			var t = this;
			
			$(this).runUnhidden(function()
			{
				highest = $(t).outerHeight();
			});
			
			$(document).find(".same-height[data-sameheight='" + $(this).attr("data-sameheight") + "']").each(function()
			{
				t = this;
				
				$(this).runUnhidden(function()
				{
					if($(t).outerHeight() > highest) highest = $(t).outerHeight();
				});
			});
			
			$(this).css("height", highest);
		}
		else
		{
			var highest = 0;
			var t = this;
			
			$(this).runUnhidden(function()
			{
				highest = $(t).outerHeight();
			});
			
			$(this).siblings(".same-height").each(function()
			{
				t = this;
				
				$(this).runUnhidden(function()
				{
					if($(t).outerHeight() > highest) highest = $(t).outerHeight();
				});
			});
			
			$(this).css("height", highest);
		}
		
		$(this).trigger("sameheight_Recalculated");
	});
	
	$(document).trigger("sameheight_AllRecalculated");
}