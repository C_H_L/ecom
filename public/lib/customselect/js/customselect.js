$(document).ready(function()
{
	//Initialise select menus
	$(".cussel").each(function()
	{
		var html = "";
		var optionWidest = $(this).outerWidth();
		
		html += '<select name="' + $(this).attr("name") + '" ' + ($(this).attr("required") != undefined ? $(this).attr("required") : '') + '>';
		html += '<option value=""></option>';
		$(this).find(".cussel-option").each(function()
		{
			html += '<option value="' + $(this).attr("value") + '"' + ($(this).attr("selected") != undefined ? $(this).attr("selected") : '') + '>' + $(this).text() + '</option>';
			
			if($(this).outerWidth() > optionWidest) optionWidest = $(this).outerWidth();
		});
		html += '</select>';
		
		$(this).append(html);
		if($(this).css("width")) $(this).width(optionWidest);
		
		$(this).find(".cussel-display").html('<div class="text">' + $(this).find(".cussel-display").text() + '</div>');
		
		$(this).find(".cussel-dropdown").prepend('<div class="cussel-dropdown-title">' + $(this).find(".cussel-display").text() + '</div>');
	});
	
	$(window).resize(function()
	{
		$(".cussel").each(function()
		{
			$(this).resizeWidth();
		});
	});
	
	$(window).scroll(function()
	{
		$(".cussel-dropdown.use-fixed-pos").each(function()
		{
			$(this).css("top", ($(this).prev(".cussel-display").offset().top - $(window).scrollTop()) + $(this).prev(".cussel-display").outerHeight());
		});
	});
	
	$(".cussel .cussel-display").click(function()
	{
		if(!$(this).next(".cussel-dropdown").hasClass("show"))
		{
			$(this).closest(".cussel").openCussel();
		}
		else
		{
			$(this).closest(".cussel").closeCussel();
		}
	});
	
	$("*").click(function(e)
	{
		if(this === e.target)
		{
			var t = this;
			$(".cussel").each(function()
			{
				if(!$(t).closest(".cussel").is($(this)))
				{
					$(this).closeCussel();
				}
			});
		}
	});
	
	$(".cussel-dropdown .cussel-option").click(function()
	{
		$(this).closest(".cussel-dropdown").clearSelection();
		
		$(this).addClass("selected");
		$(this).closest(".cussel").find("select option[value='" + $(this).attr("value") + "']").attr("selected", "selected");
		$(this).closest(".cussel").find(".cussel-display .text").text($(this).text());
		
		$(this).closest(".cussel").closeCussel();
	});
});

(function($)
{
	$.fn.openCussel = function()
	{
		$(this).find(".cussel-display").finish().addClass("active");
		$(this).find(".cussel-dropdown").finish().addClass("show").hide().slideDown(350);
	};
	
	$.fn.closeCussel = function()
	{
		$(this).find(".cussel-display").finish().removeClass("active");
		$(this).find(".cussel-dropdown").finish().slideUp(350, function()
		{
			$(this).removeClass("show").show();
		});
	};
	
	$.fn.clearSelection = function()
	{
		$(this).find(".cussel-option").each(function()
		{
			$(this).removeClass("selected");
		});
		
		$(this).siblings("select").find("option").each(function()
		{
			$(this).removeAttr("selected");
		});
	};
	
	$.fn.resizeWidth = function()
	{
		$(this).css("width", "");
		var optionWidest = $(this).outerWidth();
		
		$(this).find(".cussel-option").each(function()
		{
			if($(this).outerWidth() > optionWidest) optionWidest = $(this).outerWidth();
		});
		
		if($(this).css("width")) $(this).width(optionWidest);
	};
})(jQuery);