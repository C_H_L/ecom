class ImageViewer
{
	static create(args)
	{
		if(args != undefined
		&& args.images != undefined && args.images.length > 0)
		{
			var viewerID = Math.random().toString(36).substring(2, 9);
			
			var html = '<div id="' + viewerID + '" class="imageviewer">'
				+ '<div class="imageviewer-close"></div>'
				
				+ '<div class="image-container">';
				
					for(var x = 0; x < args.images.length ; x++)
					{
						html += '<div class="image-src"><img src="' + libURL + 'imageviewer/image/image_loading.png" class="runtime-load" data-img-src="' + args.images[x] + '" /></div>'
					}
					
			html += '</div>'
				+ '<div class="controls left">'
					+ '<i class="fa fa-angle-left"></i>'
				+ '</div>'
				+ '<div class="controls right">'
					+ '<i class="fa fa-angle-right"></i>'
				+ '</div>'
			+ '</div>';
			
			$("body").append(html);
			
			$("#" + viewerID).imageviewerOpen();
		}
		else
		{
			console.error("Failed to create ImageViewer: Missing required arguments");
		}
	}
}

(function($)
{
	//Runtime Load
	$.fn.runtimeLoadImage = function()
	{
		var t = this;
		
		var loadImage = new Image();
		loadImage.src = $(t).attr("data-img-src");
		
		$(loadImage).on("load", function()
		{
			$(t).attr("src", $(t).attr("data-img-src"));
		});
	}
	
	//Image Viewer
	$.fn.imageviewerOpen = function(args)
	{
		$(this).addClass("active");
		if(args == undefined || args.resetActive || $(this).find(".image-container .image-src.active").length == 0)
		{
			$(this).find(".image-container .image-src.left").switchClass("left", "right", 0);
			$(this).find(".image-container .image-src.active").switchClass("active", "right", 0);
			$(this).find(".image-container .image-src:not(.right)").addClass("right");
			$(this).find(".image-container .image-src:first-child").switchClass("right", "active", 0);
		}
		
		$(this).fadeIn(function()
		{
			if(args == undefined || args.resetActive) $(this).find(".image-container .image-src.active .runtime-load").runtimeLoadImage();
			if(args != undefined && args.complete != undefined) args.complete();
		});
	}
	
	$.fn.imageviewerClose = function(args)
	{
		$(this).removeClass("active");
		$(this).fadeOut(function()
		{
			if(args != undefined && args.complete != undefined) args.complete();
		});
	}
	
	$.fn.imageviewerNavLeft = function(complete = function(){})
	{
		var active = $(this).find(".image-container .image-src.active");
		
		if(active.is(":not(:first-child)") && !active.is(":animated"))
		{
			active
				.stop(true, true)
				.switchClass("active", "right");
			
			active
				.prev(".image-src.left")
				.stop(true, true)
				.switchClass("left", "active",
				{
					complete: function()
					{
						$(this).find(".runtime-load")
						.runtimeLoadImage();
						
						complete();
					}
				});
		}
		else
		{
			complete();
		}
	}
	
	$.fn.imageviewerNavRight = function(complete = function(){})
	{
		var active = $(this).find(".image-container .image-src.active");
		
		if(active.is(":not(:last-child)") && !active.is(":animated"))
		{
			active
				.stop(true, true)
				.switchClass("active", "left");
			
			active
				.next(".image-src.right")
				.stop(true, true)
				.switchClass("right", "active",
				{
					complete: function()
					{
						$(this).find(".runtime-load")
						.runtimeLoadImage();
						
						complete();
					}
				});
		}
		else
		{
			complete();
		}
	}
})(jQuery);

var lgScreenSize = 1200;
var mdScreenSize = 992;
var smScreenSize = 768;
var xsScreenSize = 768;

$(document).ready(function()
{
	//Runtime Load
	$(".runtime-load").each(function()
	{
		$(this).attr("src", libURL + "imageviewer/image/image_loading.png");
	});
	
	//Show/Hide controls
	$(document).on("click", ".imageviewer .image-src", function(e)
	{
		if(this === e.target)
		{
			$(this).closest(".imageviewer").imageviewerClose();
		}
	});
	
	$(document).on("click", ".imageviewer .imageviewer-close", function()
	{
		$(this).closest(".imageviewer").imageviewerClose();
	});
	
	//Navigation Controls
	$(document).on("click", ".imageviewer .controls.left", function(e)
	{
		e.stopPropagation();
		
		$(this).closest(".imageviewer").imageviewerNavLeft();
	});
	
	
	$(document).on("click", ".imageviewer .controls.right", function(e)
	{
		e.stopPropagation();
		
		$(this).closest(".imageviewer").imageviewerNavRight();
	});
	
	//Swipe Gestures
	var ivSwipeX = 0;
	var ivSwipeY = 0;
	var swipeDir = [1, 1];
	var imgDragOffsetX = 0;
	var imgDragOffsetY = 0;
	var windowX = $(window).outerWidth();
	var windowY = $(window).outerHeight();
	
	$(document).on("mousedown touchstart", ".imageviewer .image-container", function(e)
	{
		e.preventDefault();
		
		var pageX = e.pageX != undefined ? e.pageX : e.touches[0].pageX;
		var pageY = e.pageY != undefined ? e.pageY : e.touches[0].pageY;
		
		ivSwipeX = pageX;
		ivSwipeY = pageY;
	})
	.on("mousemove touchmove", ".imageviewer .image-container", function(e)
	{
		if(ivSwipeX != 0 && ivSwipeY != 0 && !$(this).find(".image-src.active").is(":animated"))
		{
			var img = $(this).find(".image-src.active img");
			
			var pageX = e.pageX != undefined ? e.pageX : e.touches[0].pageX;
			var pageY = e.pageY != undefined ? e.pageY : e.touches[0].pageY;
			
			img.css("left", "");
			img.css("top", "");
			
			imgDragOffsetX = pageX - ivSwipeX;
			imgDragOffsetY = pageY - ivSwipeY;
			
			if(swipeDir[0] == 1 && swipeDir[1] == 1
			&& Math.abs(imgDragOffsetX) > 0.1 * windowX
			&& Math.abs(imgDragOffsetX) > Math.abs(imgDragOffsetY))
			{
				swipeDir[1] = 0;
			}
			else if(swipeDir[0] == 1 && swipeDir[1] == 1
			&& Math.abs(imgDragOffsetY) > 0.1 * windowY
			&& Math.abs(imgDragOffsetY) > Math.abs(imgDragOffsetX))
			{
				swipeDir[0] = 0;
			}
			
			img.css("left", parseFloat(img.css("left")) + imgDragOffsetX * swipeDir[0]);
			img.css("top", parseFloat(img.css("top")) + imgDragOffsetY * swipeDir[1]);
		}
	});
	
	$(document).on("mouseup touchend", function(e)
	{
		if(ivSwipeX != 0 && ivSwipeY != 0)
		{
			var active = $(".imageviewer.active");
			
			var pageX = e.pageX != undefined ? e.pageX : e.changedTouches[0].pageX;
			var pageY = e.pageY != undefined ? e.pageY : e.changedTouches[0].pageY;
			
			//console.log("X: " + pageX + " | " + ivSwipeX);
			//console.log("Y: " + pageY + " | " + ivSwipeY);
			
			if(swipeDir[0] == 1 && pageX - ivSwipeX > .2 * $(window).outerWidth())
			{
				var img = active.find(".image-src.active img");
				var offsetX = imgDragOffsetX;
				var offsetY = imgDragOffsetY;
				
				active.imageviewerNavLeft(function()
				{
					img.css("left", "");
					img.css("top", "");
				});
			}
			else if(swipeDir[0] == 1 && pageX - ivSwipeX < -.2 * $(window).outerWidth())
			{
				var img = active.find(".image-src.active img");
				var offsetX = imgDragOffsetX;
				var offsetY = imgDragOffsetY;
				
				active.imageviewerNavRight(function()
				{
					img.css("left", "");
					img.css("top", "");
				});
			}
			else if(swipeDir[1] == 1 && pageY - ivSwipeY > .25 * $(window).outerHeight())
			{
				var img = active.find(".image-src.active img");
				var offsetX = imgDragOffsetX;
				var offsetY = imgDragOffsetY;
				
				active.imageviewerClose(
				{
					complete:function()
					{
						img.css("left", "");
						img.css("top", "");
					}
				});
			}
			else
			{
				var img = active.find(".image-src.active img");
				img.css("left", "");
				img.css("top", "");
			}
			
			ivSwipeX = 0;
			ivSwipeY = 0;
			
			imgDragOffsetX = 0;
			imgDragOffsetY = 0;
			
			swipeDir = [1, 1];
		}
	});
	
	/*//Show/Hide controls
	$(".imageviewer .image-src").click(function(e)
	{
		if(this === e.target)
		{
			$(this).closest(".imageviewer").imageviewerClose();
		}
	});
	
	$(".imageviewer .imageviewer-close").click(function()
	{
		$(this).closest(".imageviewer").imageviewerClose();
	});
	
	//Navigation Controls
	$(".imageviewer .controls.left").click(function(e)
	{
		e.stopPropagation();
		
		$(this).closest(".imageviewer").imageviewerNavLeft();
	});
	
	
	$(".imageviewer .controls.right").click(function(e)
	{
		e.stopPropagation();
		
		$(this).closest(".imageviewer").imageviewerNavRight();
	});
	
	//Swipe Gestures
	var ivSwipeX = 0;
	var ivSwipeY = 0;
	var imgDragOffsetX = 0;
	var imgDragOffsetY = 0;
	
	$(".imageviewer .image-container").on("mousedown touchstart", function(e)
	{
		e.preventDefault();
		
		var pageX = e.pageX != undefined ? e.pageX : e.touches[0].pageX;
		var pageY = e.pageY != undefined ? e.pageY : e.touches[0].pageY;
		
		ivSwipeX = pageX;
		ivSwipeY = pageY;
	})
	.on("mousemove touchmove", function(e)
	{
		if(ivSwipeX != 0 && ivSwipeY != 0)
		{
			var img = $(this).find(".image-src.active img");
			
			var pageX = e.pageX != undefined ? e.pageX : e.touches[0].pageX;
			var pageY = e.pageY != undefined ? e.pageY : e.touches[0].pageY;
			
			img.css("left", "");
			img.css("top", "");
			
			imgDragOffsetX = pageX - ivSwipeX;
			imgDragOffsetY = pageY - ivSwipeY;
			
			img.css("left", parseFloat(img.css("left")) + imgDragOffsetX);
			img.css("top", parseFloat(img.css("top")) + imgDragOffsetY);
		}
	})
	.on("mouseup touchend", function(e)
	{
		var pageX = e.pageX != undefined ? e.pageX : e.changedTouches[0].pageX;
		var pageY = e.pageY != undefined ? e.pageY : e.changedTouches[0].pageY;
		
		//console.log("X: " + pageX + " | " + ivSwipeX);
		//console.log("Y: " + pageY + " | " + ivSwipeY);
		
		if(pageX - ivSwipeX > .2 * $(window).outerWidth())
		{
			var img = $(this).find(".image-src.active img");
			var offsetX = imgDragOffsetX;
			var offsetY = imgDragOffsetY;
			
			$(this).closest(".imageviewer").imageviewerNavLeft(function()
			{
				img.css("left", "");
				img.css("top", "");
			});
		}
		else if(pageX - ivSwipeX < -.2 * $(window).outerWidth())
		{
			var img = $(this).find(".image-src.active img");
			var offsetX = imgDragOffsetX;
			var offsetY = imgDragOffsetY;
			
			$(this).closest(".imageviewer").imageviewerNavRight(function()
			{
				img.css("left", "");
				img.css("top", "");
			});
		}
		else if(pageY - ivSwipeY > .25 * $(window).outerHeight())
		{
			var img = $(this).find(".image-src.active img");
			var offsetX = imgDragOffsetX;
			var offsetY = imgDragOffsetY;
			
			$(this).closest(".imageviewer").imageviewerClose(
			{
				complete:function()
				{
					img.css("left", "");
					img.css("top", "");
				}
			});
		}
		else
		{
			var img = $(this).find(".image-src.active img");
			img.css("left", "");
			img.css("top", "");
		}
		
		ivSwipeX = 0;
		ivSwipeY = 0;
		
		imgDragOffsetX = 0;
		imgDragOffsetY = 0;
	});*/
});