/*
	JS Custom Tooltip 
	By CHL, 13/07/2018
	
	Last rev: 13/07/2018
	
	- Popup position
	- Content type
	- Content
	- Trigger
	- Overlay(Absolute) / Expand Parent(Relative)
	
	- Change events to ondocument to accomodate dynamic elements
*/

$(document).ready(function()
{
	$.ctooltip();
	
	
});

(function($)
{
	$.ctooltip = function()
	{
		$(".ctooltip-trigger").each(function()
		{
			var _this = this;
			var trig = $(_this); //Reference ctooltip triggerer element
			
			var content = trig.attr("data-content") ? trig.attr("data-content") : "NULL";
			var trigger = trig.attr("data-trigger") ? trig.attr("data-trigger") : "hover";
			
			if(trigger == "hover")
			{
				$(document).on("mouseenter", "[data-content='" + content + "']", function()
				{
					trig.showCToolTip();
				});
			}
		});
		
		console.log("C");
	};
	
	$.fn.showCToolTip = function()
	{
		var _this = this;
		var trig = $(_this); //Reference ctooltip triggerer element
		
		//Trigger properties
		var position = trig.attr("data-position") ? trig.attr("data-position") : "bottom";
		var contentType = trig.attr("data-content-type") ? trig.attr("data-content-type") : "text";
		var content = trig.attr("data-content") ? trig.attr("data-content") : "NULL";
		var trigger = trig.attr("data-trigger") ? trig.attr("data-trigger") : "hover";
		var hierarchy = trig.attr("data-hierarchy") ? trig.attr("data-hierarchy") : "absolute";
		
		if(contentType == "exthtml")
		{
			var tt = $(content); //Reference ctooltip element
			
			//Add CSS position style
			tt.addClass(position);
			
			tt.setCTTPosition(position, trig, tt);
			//if(tt.find(".subctooltip-trigger")) tt.setCTTPosition(position, trig, tt);
			
			tt.css("display", "block");
			
			$(document).on("mouseleave", content, function()
			{
				trig.hideCToolTip();
			});
			$(document).on("mouseleave", "[data-content='" + content + "']", function()
			{
				setTimeout(function()
				{
					if(!tt.is(":hover") && !tt.find(":hover").length)
					{
						trig.hideCToolTip();
					}
				}, 100);
			});
			
			
			tt.find(".subctooltip-trigger").each(function()
			{
				var _this = this;
				var trig = $(_this); //Reference ctooltip triggerer element
				
				var content = trig.attr("data-content") ? trig.attr("data-content") : "NULL";
				var trigger = trig.attr("data-trigger") ? trig.attr("data-trigger") : "hover";
				
				if(trigger == "hover")
				{
					$(document).on("mouseenter", "[data-content='" + content + "']", function()
					{
						trig.showCToolTip();
					});
				}
			});
		}
	}
	
	$.fn.setCTTPosition = function(position, trig, tt)
	{
		var ttSizeOffsetX = 0;
		var ttSizeOffsetY = 0;
		
		$.runUnhidden(
		[
			trig[0],
			tt[0]
		],
		function()
		{
			switch(position)
			{
				case "top":
					ttSizeOffsetX = (trig.outerWidth()/2) - (tt.outerWidth()/2);
					ttSizeOffsetY = -tt.outerHeight(true);
					break;
				case "right":
					ttSizeOffsetX = trig.outerWidth() + (tt.outerWidth(true) - tt.outerWidth());
					ttSizeOffsetY = (trig.outerHeight()/2) - (tt.outerHeight()/2);
					break;
				case "bottom":
					ttSizeOffsetX = (trig.outerWidth()/2) - (tt.outerWidth()/2);
					ttSizeOffsetY = trig.outerHeight() + (tt.outerHeight(true) - tt.outerHeight());
					break;
				case "left":
					ttSizeOffsetX = -(tt.outerWidth() + (tt.outerWidth(true) - tt.outerWidth()));
					ttSizeOffsetY = (trig.outerHeight()/2) - (tt.outerHeight()/2);
					break;
			}
		});
		
		//Set TT position
		tt.offset({top: trig.offset().top + ttSizeOffsetY, left: trig.offset().left + ttSizeOffsetX});
	};
	
	
	$.runUnhidden = function(elements, func)
	{
		for(x = 0; x < elements.length; x++)
		{
			//Run on self first
			if($(elements[x]).css("display") == "none")
			{
				$(elements[x]).attr("data-unhidden-hideafter", "true");
				$(elements[x]).css("display", "block");
			}
			
			//Then run on all parents
			$(elements[x]).parents().each(function()
			{
				if($(elements[x]).css("display") == "none")
				{
					$(elements[x]).attr("data-unhidden-hideafter", "true");
					$(elements[x]).css("display", "block");
				}
			});
		}
			
		func();
			
		for(x = 0; x < elements.length; x++)
		{
			$("[data-unhidden-hideafter='true']").each(function()
			{
				$(elements[x]).removeAttr("data-unhidden-hideafter");
				$(elements[x]).css("display", "");
			});
		}
	};
})(jQuery);