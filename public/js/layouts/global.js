var lgScreenSize = 1200;
var mdScreenSize = 992;
var smScreenSize = 768;
var xsScreenSize = 768;

$(document).ready(function()
{
	wScroll();
	$(window).scroll(function()
	{
		wScroll();
	});
	
	wResize();
	inMD();
	$(window).resize(function()
	{
		wResize();
		inMD();
	});
	
});

function wScroll()
{
	if($(window).scrollTop() > $(".header-bottom.sticky").outerHeight())
	{
		$(".header-bottom.sticky").addClass("sticky-active");
	}
	else
	{
		$(".header-bottom.sticky").removeClass("sticky-active");
	}
}

function wResize()
{
	
}

function inMD()
{
	$(".in-md:not([aria-expanded])").each(function()
	{
		if($(window).outerWidth() <= mdScreenSize)
		{
			$(this).removeClass("in");
		}
		else
		{
			$(this).addClass("in");
		}
	});
}