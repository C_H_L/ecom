Dropzone.autoDiscover = false;

$(document).ready(function()
{
	if($(".dropzone").length > 0)
	{
		$(".dropzone").each(function()
		{
			var _this = this;
			
			var dz = new Dropzone("#" + $(this).attr("id"),
			{
				params:
				{
					_token: "{{ csrf_token() }}"
				},
				uploadMultiple: true,
				addRemoveLinks: false,
				parallelUploads: 1,
				autoProcessQueue: true,
				error: function(file, response)
				{
					console.log(response);
					
					if($.type(response) === "string")
						var message = response;
					else
						var message = response.message;

					$(_this).append(message);
					
					bootboxError(response)
				},
				paramName: "file",
			});

			dz.on("queuecomplete", function(progress)
			{
				window.location.reload();
			});
		});
	}
	
	$(".record-media .media").click(function()
	{
		$(".record-media-popup .content .media").remove();
		$(".record-media-popup .content").append($(this).clone());
		$(".record-media-popup").fadeIn(350);
	});
	
	$(".record-media-popup .content .btn-close").click(function()
	{
		$(".record-media-popup").fadeOut(350);
	});
	$(".record-media-popup").click(function(e)
	{
		if(this == e.target) $(this).fadeOut(350);
	});
	
	$(".record-list-table:not(.no-order)").DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
	});
	
	$(".record-list-table.no-order").DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"order": [],
	});
	
	$(this).on("click", "*[data-href]", function(e)
	{
		var tag = $(e.target).prop("tagName").toLowerCase();
		if(tag != "a" && tag != "button" && tag != "input")
		{
			$(".overlay-loading").show();
			window.location.href = $(this).attr("data-href");
		}
	});
	
	$(".img-upload input[type='file']").change(function(e)
	{
		$(".overlay-loading").show();
		if(this.files && this.files[0])
		{
			var _this = this;
			var img = this.files[0];
			
			var reader = new FileReader();
			reader.onload = function(e)
			{
				$(".overlay-loading").hide();
				$(_this).closest(".img-upload").find("img").attr("src", e.target.result);
			};
			
			reader.readAsDataURL(img);
		}
	});
	
	$(".modify-form").submit(function()
	{
		$(".overlay-loading").show();
	});
	
	$(".timepicker").timepicker(
	{
		"showInputs": false,
		"explicitMode": true,
		"snapToStep": true,
		"minuteStep": 10,
	});
	
	$(".datepicker").daterangepicker(
	{
		singleDatePicker: true,
		format: "YYYY-MM-DD",
	});
	
	$(".datetimepicker").each(function()
	{
		var drpFrom = $(this).daterangepicker(
		{
			singleDatePicker: true,
			timePicker: true,
			timePickerIncrement: 1,
			format: "YYYY-MM-DD hh:mm A",
		});
		
		/*if($(this).attr("data-datetimepicker-to"))
		{
			var to = $($(this).attr("data-datetimepicker-to"));
			
			var drpTo = to.daterangepicker(
			{
				singleDatePicker: true,
				timePicker: true,
				timePickerIncrement: 1,
				format: "YYYY-MM-DD hh:mm A",
			})
			.on("apply.daterangepicker", function(ev, picker)
			{
				console.log(drpFrom);
				drpFrom.datepicker("option", "minDate", picker.startDate.toDate());
			});
			
			drpFrom.on("apply.daterangepicker", function(ev, picker)
			{
				console.log(drpTo);
				drpTo.datepicker("option", "maxDate", picker.startDate.toDate());
			});
		}*/
	});
	
	/*$(".datepicker-from").each(function()
	{
		var datepickerFrom = $(this);
		var datepickerTo = $($(this).attr("data-datepicker-to"));
		var dateFormat = "yy-mm-dd";
		
		datepickerFrom.datepicker(
		{
			dateFormat: dateFormat,
		})
		.on("change", function()
		{
			datepickerTo.datepicker("option", "minDate", datepickerFrom.datepicker("getDate"));
		});
		
		datepickerTo.datepicker(
		{
			dateFormat: dateFormat,
		})
		.on("change", function()
		{
			datepickerFrom.datepicker("option", "maxDate", datepickerTo.datepicker("getDate"));
		});
	});*/
	
	$(".input-toggle").change(function(e)
	{
		var target = $($(this).attr("data-target"));
		target.prop("disabled", ($(this).is(":checked") ? false : true));
	});
	$(".input-toggle").each(function(e)
	{
		var target = $($(this).attr("data-target"));
		
		if(target.getVal())
		{
			$(this).setVal(true);
		}
	});
	
	$(".input-toggle-conditional").change(function(e)
	{
		var target = $(this).attr("data-target");
		var split = target.split(",");
		
		if(split.length > 1)
		{
			var element = $(split[1]);
			if($(this).getVal() == split[0])
			{
				element.show();
			}
			else
			{
				element.hide();
				element.find("input").addBack("input").setVal(null);
			}
		}
	});
	
	$(".input-toggle-withdata").change(function(e)
	{
		var target = $($(this).attr("data-target"));
		var url = $(this).find(":selected").attr("data-url");
		
		$(".overlay-loading").show();
		
		$.post(
		{
			url: url,
			data:
			{
				"_token": csrfToken,
			},
			success: function(response)
			{
				if(response[0] == "success")
				{
					var data = JSON.parse(response[1]);
					var html = "";
					$.each(data, function(key, value)
					{
						html += '<option value="' + key + '">' + value + '</option>';
					});
					
					target.find("select").html(html);
					target.find("select").setVal(target.find("select").attr("data-value"));
					target.show();
					
					$(".overlay-loading").hide();
				}
				else
				{
					target.hide();
					
					$(".overlay-loading").hide();
					
					bootboxError(response[1]);
				}
			},
		});
	});
	
	$(".record-delete").click(function()
	{
		var actionURL = $(this).attr("data-action-url");
		
		bootbox.confirm(
		{
			message: "Confirm delete record",
			backdrop: true,
			buttons:
			{
				confirm:
				{
					label: "Delete",
					className: "btn-danger",
				},
				cancel:
				{
					label: "Cancel",
				},
			},
			callback: function(result)
			{
				if(result)
				{
					$(".overlay-loading").show();
					window.location.href = actionURL;
				}
			},
		});
	});
	
	$(".record-toggle-active").click(function()
	{
		var actionURL = $(this).attr("data-action-url");
		
		bootbox.confirm(
		{
			message: "Confirm toggle record active status",
			backdrop: true,
			buttons:
			{
				confirm:
				{
					label: "Toggle",
					className: "btn-warning",
				},
				cancel:
				{
					label: "Cancel",
				},
			},
			callback: function(result)
			{
				if(result)
				{
					$(".overlay-loading").show();
					window.location.href = actionURL;
				}
			},
		});
	});
	
	$(".profile-img-form input[type='file']").change(function()
	{
		$(this).closest(".profile-img-form").submit();
	});
	
	//Bootstrap pills and tabs URL hash
	updatePillsAndTabs();
	
	$(window).on("hashchange", function()
	{
		updatePillsAndTabs();
	});
	
	function updatePillsAndTabs()
	{
		var hash = window.location.hash.split("#");
		
		if(hash.length != 0)
		{
			$.each(hash, function(key, val)
			{
				if($(".nav-link[href='#" + val + "']").length > 0)
				{
					$(".nav-link[href='#" + val + "']").tab("show");
					$(window).finish().animate(
					{
						scrollTop: $(".nav-link[href='#" + val + "']").offset().top,
					});
				}
			});
		}
	}
	
	$(".nav-pills .nav-link[data-toggle='tab']").click(function()
	{
		window.location.hash = $(this).attr("href");
	});
	
	$(".record-calendar").each(function()
	{
		var _this = this;
		
		$(this).fullCalendar(
		{
			aspectRatio: 1.5,
			eventSources:
			[
				{
					events: function(start, end, timezone, callback)
					{
						var _calendar = _this;
						var fetchURL = $(_calendar).attr("data-fetch-url");
						
						$(".overlay-loading").show();
						
						$.post(
						{
							url: fetchURL,
							data:
							{
								_token: csrfToken,
								start: start.format(),
								end: end.format(),
							},
							success: function(response)
							{
								callback(response);
							},
							error: function()
							{
								alert("Error retrieving events");
							},
							complete: function()
							{
								$(".overlay-loading").hide();
							},
						});
					},
					//className: "bg-success",
				},
			],
			eventRender:function(event, element)
			{
				$(element).tooltip(
				{
					title: event.tooltip,
				});
			},
			eventClick: function(event, jsEvent, view)
			{
				window.location.href = event.action_url;
			}
		});
	});
	
	//Set events dynamically
	/*$(".record-calendar").fullCalendar("renderEvent",
	{
		title: "Event 1",
		start: "2019-01-11",
	});*/
	
	//Rating input
	$(".input-rating").each(function()
	{
		$(this).updateRating($(this).find("input, textarea").getVal());
	});
	
	$(".input-rating .star").click(function()
	{
		$(this).closest(".input-rating").updateRating($(this).index(".star") + 1);
	});
});

function bootboxError(arg)
{
	var msg = '<b><i class="fa fa-exclamation-circle" style="color: red;"></i>&nbsp; Error</b></br></br>';
	if($.isArray(arg))
	{
		msg = '<b><i class="fa fa-exclamation-circle" style="color: red;"></i>&nbsp; Please resolve the following errors:</b></br></br>';
		
		$.each(arg, function(key, value)
		{
			msg += value + "</br>";
		});
	}
	else
	{
		msg += arg;
	}
	
	bootbox.alert(
	{
		message: msg,
		backdrop: true,
	});
}

function bootboxSuccess(str)
{
	var msg = '<b><i class="fa fa-check-circle" style="color: #28a745;"></i>&nbsp; Success</b></br></br>' + str;
	
	bootbox.alert(
	{
		message: msg,
		backdrop: true,
	});
}

(function($)
{
	$.fn.getVal = function()
	{
		if($(this).prop("tagName").toLowerCase() == "select")
		{
			return $(this).val();
		}
		else
		{
			if($(this).attr("type") && $(this).attr("type").toLowerCase() == "checkbox")
			{
				return $(this).val();
			}
			else
			{
				return $(this).val();
			}
		}
		
		return null;
	};
	
	$.fn.setVal = function(val)
	{
		if($(this).prop("tagName").toLowerCase() == "select")
		{
			if(val) $(this).find("option[value='" + val + "']").prop("selected", true).change();
		}
		else
		{
			if($(this).attr("type").toLowerCase() == "checkbox")
			{
				if(val == true)
				{
					$(this).prop("checked", true).change();
				}
				else
				{
					$(this).val(val).change();
				}
			}
			else
			{
				$(this).val(val).change();
			}
		}
	};
	
	$.fn.postRedirect = function(url, data)
	{
		var _this = this;
		
		var form = '<form method="POST" action="' + url + '">'
			+ csrfField;
		
			$.each(data, function(key, val)
			{
				form += '<input type="hidden" name="' + key + '" value="' + val + '" />';
			});
				
		form += '</form>';
		
		$(form).appendTo(_this).submit();
	};
	
	$.fn.updateRating = function(val)
	{
		$(this).find("input, textarea").setVal(0);
		
		if(val && parseInt(val))
		{
			var stars = $(this).find(".star");
			
			for(var x = 0; x < stars.length; x++)
			{
				stars.eq(x).removeClass("active");
				if(x < val) stars.eq(x).addClass("active");
			}
			
			$(this).find("input, textarea").setVal(val);
			console.log($(this).find("input, textarea").getVal());
		}
	};
	
})(jQuery)