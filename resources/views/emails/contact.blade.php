<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link href="{{ asset('css/email.css?' . env('APP_VERSION')) }}" rel="stylesheet" type="text/css" />
    </head>
    <body>
		<img src="{{ asset('images/logo.png') }}" class="main-logo" />
		<br />
		
		Hi {{ env("APP_NAME") }} Admin, you have received a new enquiry, here are the details<br />
		<br />
		
		<b>Name:</b> {{ $request->name }}<br />
		<b>E-mail:</b> <a href="mailto:{{ $request->email }}">{{ $request->email }}</a><br />
		<b>Contact Number:</b> <a href="tel:{{ $request->contact_num }}">{{ $request->contact_num }}</a><br />
		<b>Message:</b><br />
		{{ $request->message }}
		
		<br />
		<br />
		Best Regards,<br />
		<b>{{ env("APP_NAME") }},</b><br />
		<br />
		<br />
		
		<div class="footer">
			Copyright {{ date("Y") }} &copy; {{ env("APP_NAME") }}<br />
			<div class="separator"></div>
			<div class="disclaimer">This is an automatically generated email, please do not reply to it</div>
		</div>
    </body>
</html>