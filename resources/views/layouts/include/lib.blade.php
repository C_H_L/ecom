<script>
	var libURL = "{{ asset('/lib/') . '/' }}";
	
	var bladeAsset = "{{ asset('/') }}";
	var bladeURL = "{{ url('/') . '/' }}";
	
	var csrfToken = '{{ csrf_token() }}';
	var csrfField = '{{ csrf_field() }}';
</script>
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Acme|Roboto|Oswald|PT+Sans|Anton|Baloo+Tammudu|Lobster|Pacifico|Uncial+Antiqua|Libre+Franklin|Abel|Nunito+Sans|Shadows+Into+Light|Spicy+Rice" rel="stylesheet">

<!-- jQuery -->
<script src="{{ asset('lib/jquery/jquery.min.js?' . env('WEB_VERSION')) }}"></script>
<script src="{{ asset('lib/jquery-ui/jquery-ui.js?' . env('WEB_VERSION')) }}"></script>
<link rel="stylesheet" href="{{ asset('lib/jquery-ui/jquery-ui.css?' . env('WEB_VERSION')) }}"/>

<!-- Bootstrap -->
<link rel="stylesheet" href="{{ asset('lib/bootstrap/css/bootstrap.min.css?' . env('WEB_VERSION')) }}"/>
<script src="{{ asset('lib/bootstrap/js/bootstrap.min.js?' . env('WEB_VERSION')) }}"></script>

<!-- Font Awesome -->
<link href="{{ asset('lib/font-awesome/css/all.css?' . env('WEB_VERSION')) }}" rel='stylesheet' type='text/css'/>

<!-- Bootbox -->
<script src="{{ asset('lib/bootbox/bootbox.min.js?' . env('WEB_VERSION')) }}"></script>

<!-- NoUI Slider -->
<link rel="stylesheet" href="{{ asset('lib/nouislider/nouislider.min.css?' . env('WEB_VERSION')) }}"/>
<script src="{{ asset('lib/nouislider/nouislider.min.js?' . env('WEB_VERSION')) }}"></script>

<!-- MatchhHeight Slider -->
<script src="{{ asset('lib/matchHeight/matchHeight-min.js?' . env('WEB_VERSION')) }}"></script>

<!-- Slick Slider -->
<link rel="stylesheet" href="{{ asset('lib/slick/slick/slick.css?' . env('WEB_VERSION')) }}"/>
<script src="{{ asset('lib/slick/slick/slick.min.js?' . env('WEB_VERSION')) }}"></script>

<!-- FancyBox -->
<link rel="stylesheet" href="{{ asset('lib/fancybox/dist/jquery.fancybox.min.css?' . env('WEB_VERSION')) }}"/>
<script src="{{ asset('lib/fancybox/dist/jquery.fancybox.min.js?' . env('WEB_VERSION')) }}"></script>

<!-- Custom Select -->
<script src="{{ asset('lib/customselect/js/customselect.js?' . env('WEB_VERSION')) }}"></script>
<link href="{{ asset('lib/customselect/css/customselect.css?' . env('WEB_VERSION')) }}" rel="stylesheet"/>

<!-- Same Height -->
<script src="{{ asset('lib/sameheight/sameheight.js?' . env('WEB_VERSION')) }}"></script>

<!-- Image Viewer -->
<script src="{{ asset('lib/imageviewer/js/imageviewer.js?' . env('WEB_VERSION')) }}"></script>
<link href="{{ asset('lib/imageviewer/css/imageviewer.css?' . env('WEB_VERSION')) }}" rel="stylesheet"/>

<!-- Image Viewer -->
<script src="{{ asset('js/layouts/global.js?' . env('WEB_VERSION')) }}"></script>
<link href="{{ asset('css/layouts/global.css?' . env('WEB_VERSION')) }}" rel="stylesheet"/>