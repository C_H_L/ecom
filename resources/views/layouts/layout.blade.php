@php
	$include_header = (!isset($include_header) ? true : $include_header);
	$include_footer = (!isset($include_footer) ? true : $include_header);
@endphp
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link href="{{ asset('images/logo.png?' . env('WEB_VERSION')) }}" rel="shortcut icon" />

		<title>{{ $title }} - {{ config('app.name', 'Laravel') }}</title>

		@include('layouts/include/lib')
		
		@yield('head')
    </head>
    <body>
		<script>
			$(document).ready(function()
			{
				@if(!empty($error) && $errors->formerror->any())
					bootbox.alert({
						message: "<span style='font-size: .85rem;'><b>Please resolve the following errors:</b></br>"
							@foreach($errors->formerror->toArray() as $error)
								@if(is_array($error[0]))
									@foreach($error[0] as $sub)
										+ '</br> {!! $sub !!}'
									@endforeach
								@else
									+ '</br> {!! $error[0] !!}'
								@endif
							@endforeach
							+ "</span>"
					});
				@endif
				
				@if(Session::has("formsuccess"))
					bootbox.alert({
						message: '<b>{!! Session::get("formsuccess") !!}</b>'
					});
				@endif
				
				@if(Session::has("bootboxMSG"))
					bootbox.alert({
						message: '<b>{!! Session::get("bootboxMSG") !!}</b>'
					});
				@endif
				
				@if(Session::has("bootboxError"))
					bootbox.alert({
						title: '<b><i class="fa fa-exclamation-circle" style="color: red;"></i> Error</b>',
						message: '{!! Session::get("bootboxError") !!}'
					});
				@endif
			});
		</script>
		
		@if($include_header)
			<div class="header">
				<div class="container">
					<div class="col-logo">
						<div class="logo">
							<a href="#"><img src="{{ asset('images/logo.png') }}" /></a>
						</div>
					</div>
					<div class="col-get-app">
					</div>
				</div>
			</div>
		@endif
		
		<div class="content-body">
			@yield('content')
		</div>
		
		@if($include_footer)
			<div class="footer">
				<div class="container">
					<b>Copyright &copy; {{ date("Y") }}</b>
				</div>
			</div>
		@endif
    </body>
</html>