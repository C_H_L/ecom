@extends(env('ADMIN_FOLDER') . 'layouts.master')

@section('bundle') active @endsection

@section('content')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">Bundles</h1>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content-header -->

		<!-- Main content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-primary card-outline">
							<div class="card-body">
								<a href="{{ route('admin.bundle.add') }}" class="btn btn-sm btn-success">Add Bundle</a>
								<hr />

								<table class="table table-striped table-bordered record-list-table">
									<thead>
										<tr>
											<th style="width: 3%;">
												ID
											</th>
											<th style="width: 10%;">
												Name
											</th>
											<th style="width: 3%;">
												Sort
											</th>
											<th style="width: 5%;">
												Price
											</th>
											<th style="width: 5%;">
												Active
											</th>
											<th style="width: 10%;">
												Created Time
											</th>
										</tr>
									</thead>
									<tbody>
										@foreach($bundles as $bundle)
											<tr data-href="{{ route('admin.bundle.single', ['id' => $bundle->id]) }}">
												<td>
													{{ $bundle->id }}
												</td>
												<td>
													{{ $bundle->name }}
												</td>
												<td>
													{{ $bundle->sort }}
												</td>
												<td>
													{{ $bundle->price }}
												</td>
												<td>
													{{ $bundle->active_text }}
												</td>
												<td>
													{{ $bundle->created_at_text }}
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div><!-- /.card -->
					</div>
					<!-- /.col-md-6 -->

				</div>
				<!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content -->

	</div>
	<!-- /.content-wrapper -->
@endsection