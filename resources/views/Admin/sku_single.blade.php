@extends(env('ADMIN_FOLDER') . 'layouts.master')
@section('product') active @endsection

@section('content')
	<script>
		$(document).ready(function()
		{
			@if(!empty($sku))
				$(".modify-form .record-delete").click(function()
				{
					bootbox.confirm(
					{
						message: "Confirm delete record",
						backdrop: true,
						buttons:
						{
							confirm:
							{
								label: "Delete",
								className: "btn-danger",
							},
							cancel:
							{
								label: "Cancel",
							},
						},
						callback: function(result)
						{
							if(result)
							{
								$(".overlay-loading").show();
								window.location.href = "{{ route('admin.product.sku.delete', ['product_id' => $product_id, 'id' => $sku->id]) }}";
							}
						},
					});
				});
				
				$(".modify-form .record-toggleactive").click(function()
				{
					bootbox.dialog(
					{
						message: 'Confirm set sku as @if($sku->active) inactive @else active @endif?',
						backdrop: true,
						buttons:
						{
							cancel:
							{
								label: 'Cancel',
							},
							confirm:
							{
								label: 'Confirm',
								className: "btn-warning",
								callback: function()
								{
									window.location.href = "{{ route('admin.product.sku.toggleactive', ['product_id' => $product_id, 'id' => $sku->id]) }}";
								},
							},
						},
					});
				});
				
				$(".modify-form .record-toggledirectpurchase").click(function()
				{
					bootbox.dialog(
					{
						message: 'Confirm toggle direct purchase?',
						backdrop: true,
						buttons:
						{
							cancel:
							{
								label: 'Cancel',
							},
							confirm:
							{
								label: 'Confirm',
								className: "btn-warning",
								callback: function()
								{
									window.location.href = "{{ route('admin.product.sku.toggledirectpurchase', ['product_id' => $product_id, 'id' => $sku->id]) }}";
								},
							},
						},
					});
				});
				
				$(".record-media .record-modify").click(function()
				{
					var actionURL = $(this).attr("data-action-url");
					var modalElement = $("#record-media-modify");
					var modalForm = modalElement.find("form");
					
					$(".overlay-loading").show();
					
					$.post(
					{
						url: actionURL,
						data:
						{
							"_token": "{{ csrf_token() }}",
						},
						success: function(response)
						{
							$.each(response, function(key, value)
							{
								var formElement = modalElement.find("[name='" + key + "']");
								
								if(formElement.attr("type") == "checkbox")
								{
									formElement.prop("checked", (value == 1 ? true : false));
								}
								else
								{
									formElement.val(value);
								}
							});
							
							modalElement.modal('show');
							$(".overlay-loading").hide();
							
							modalForm.submit(function(e)
							{
								e.preventDefault();
								
								$(".overlay-loading").show();
								$.post(
								{
									url: actionURL + "/submit",
									data: modalForm .serialize(),
									complete: function()
									{
										$(".overlay-loading").hide();
									},
									success: function(response)
									{
										if(response == "success")
										{
											window.location.reload();
										}
										else
										{
											bootboxError(response);
										}
									},
									error: function(response)
									{
										bootboxError(response);
									},
								});
							});
						},
						error: function(response)
						{
							bootboxError(response);
						},
					});
				});
				
				$(".record-media .record-delete").click(function()
				{
					var actionURL = $(this).attr("data-action-url");
					
					bootbox.confirm(
					{
						message: "Confirm delete sku image?",
						backdrop: true,
						buttons:
						{
							cancel:
							{
								label: "Cancel",
								className: "btn-default"
							},
							confirm:
							{
								label: "Delete",
								className: "btn-danger",
							}
						},
						callback: function(result)
						{
							if(result)
							{
								$(".overlay-loading").show();
								
								$.get(actionURL, function(response)
								{
									if(response == "success")
									{
										$(".overlay-loading").hide();
										window.location.reload();
									}
								});
							}
						}
					});
				});
			@endif
			
			ClassicEditor
				.create(document.querySelector("#ckeditor_title"))
				.catch(error =>
				{
					console.error(error);
				});
			
			ClassicEditor
				.create(document.querySelector("#ckeditor_description"))
				.catch(error =>
				{
					console.error(error);
				});
			
			ClassicEditor
				.create(document.querySelector("#ckeditor_freebie"))
				.catch(error =>
				{
					console.error(error);
				});
			
		});
	</script>
	
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2 mt-1">
					<div class="col-sm-6">
						<a href="{{ route('admin.product.single', ['id' => $product_id]) }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i>&nbsp; Back</a>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content-header -->

		<!-- Main content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<form class="modify-form" method="POST" action="{{ (!empty($sku) ?  route('admin.product.sku.single.submit', ['product_id' => $product_id, 'id' => $sku->id])  : route('admin.product.sku.add.submit', ['product_id' => $product_id])) }}" enctype="multipart/form-data">
							{{ csrf_field() }}
							
							<div class="card @if(!empty($sku)) card-primary @else card-success @endif">
								<div class="card-header">
									<h3 class="card-title">@if(!empty($sku)) {{ $sku->name }} @else Add SKU @endif</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="card-body">
									@if(!empty($sku))
										<button type="button" class="btn btn-sm btn-warning record-toggledirectpurchase">Toggle Direct Purchase</button>
									
										<span class="clearfix">
											<button type="button" class="btn btn-sm btn-danger btn-float-right record-delete"><i class="fa fa-trash"></i></button>
											<button type="button" class="btn btn-sm btn-warning btn-float-right record-toggleactive">Set @if($sku->active) inactive @else active @endif</button>
										</span>
										<hr />
										
										<table class="record-single-table">
											<tr>
												<td>ID</td>
												<td>{{ $sku->id }}</td>
												<td>Created Time</td>
												<td>{{ $sku->created_at_text }}</td>
											</tr>
											<tr>
												<td>Active</td>
												<td>{{ $sku->active_text }}</td>
												<td>Direct Purchase</td>
												<td>{{ $sku->direct_purchase_text }}</td>
											</tr>
										</table>
									@endif

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label>Name</label>
												<input type="text" class="form-control" name="name" value="{{ old('name', !empty($sku) ? $sku->name : '') }}" placeholder="Name" />
											</div>
											
											<div class="form-group">
												<label>Sorting Index</label>
												<input type="number" class="form-control" name="sort" value="{{ old('sort', !empty($sku) ? $sku->sort : $sort) }}" placeholder="Index" />
											</div>
											
											<div class="form-group">
												<label>Main Image</label>
												<div class="img-upload">
													<input type="file" class="form-control" name="img_url" />
													<img src="{{ (!empty($sku) ? $sku->image_asset : '') }}" />
												</div>
											</div>
											
											<div class="form-group">
												<label>Dimension Image</label>
												<div class="img-upload">
													<input type="file" class="form-control" name="dimension_url" />
													<img src="{{ (!empty($sku) ? $sku->dimension_asset : '') }}" />
												</div>
											</div>
											
											<div class="form-group">
												<label>Ribbon Image</label>
												<div class="img-upload">
													<input type="file" class="form-control" name="ribbon_url" />
													<img src="{{ (!empty($sku) ? $sku->ribbon_asset : '') }}" />
												</div>
											</div>
											
											@if(empty($sku))
												<div class="form-check">
													<label class="form-check-label"><input type="checkbox" class="form-check-input" name="direct_purchase" @if(empty($sku) || $sku->direct_purchase) checked @endif /> Direct Purchase</label>
												</div>
												
												<div class="form-check">
													<label class="form-check-label"><input type="checkbox" class="form-check-input" name="active" @if(empty($sku) || $sku->active) checked @endif /> Active</label>
												</div>
											@endif
											
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label>Size</label>
												<input type="text" class="form-control" name="size" value="{{ old('size', !empty($sku) ? $sku->size : '') }}" placeholder="Size" />
											</div>
											
											<div class="form-group">
												<label>Price</label>
												<input type="number" class="form-control" name="price" step="any" value="{{ old('price', !empty($sku) ? $sku->price : '') }}" placeholder="Price" />
											</div>
											
											<div class="form-group">
												<label>Title</label>
												<textarea id="ckeditor_title" class="form-control" name="title" placeholder="Title">{{ old('title', !empty($sku) ? $sku->title : '') }}</textarea>
											</div>
											
											<div class="form-group">
												<label>Description</label>
												<textarea id="ckeditor_description" class="form-control" name="description" placeholder="Description">{{ old('description', !empty($sku) ? $sku->description : '') }}</textarea>
											</div>
											
											<div class="form-group">
												<label>Freebie</label>
												<textarea id="ckeditor_freebie" class="form-control" name="freebie" placeholder="Freebie">{{ old('freebie', !empty($sku) ? $sku->freebie : '') }}</textarea>
											</div>
											
										</div>
									</div>
								</div>
								<div class="card-footer">
									@if(!empty($sku))
										<button type="submit" class="btn btn-primary">Change</button>
									@else
										<button type="submit" class="btn btn-success">Add</button>
									@endif
								</div>
							</div><!-- /.card -->
						</form>
					</div>
					<!-- /.col-lg-12 -->

				</div>
				<!-- /.row -->
				
				@if(!empty($sku))
					<div class="row">
						<div class="col-lg-12">
							<div id="sku_image" class="card card-info">
								<div class="card-header">
									<h3 class="card-title">SKU Images</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="card-body">
									<div class="row">
										<div class="col-md-12">
											<form method="POST" action="{{ route('admin.product.sku.image.upload', ['product_id' => $product_id, 'sku_id' => $sku->id]) }}" id="dropzone-media" class="dropzone dropzone-media" enctype="multipart/form-data">
												{{ csrf_field() }}
											</form>
											</br>
										</div>
										@foreach($sku_images as $image)
											<div class="col-md-3 same-height">
												<div class="record-media">
													<img src="{{ $image->image_asset }}" class="media" />
													
													<div class="overlay">
														<div class="control record-modify" data-action-url="{{ route('admin.product.sku.image.single', ['product_id' => $product_id, 'sku_id' => $image->sku_id, 'id' => $image->id]) }}"><i class="fa fa-edit"></i></div>
														<div class="control record-delete" data-action-url="{{ route('admin.product.sku.image.delete', ['product_id' => $product_id, 'sku_id' => $image->sku_id, 'id' => $image->id]) }}"><i class="fa fa-trash"></i></div>
														
														@if($image->active == 0)
															<div class="not-active"><i class="fa fa-eye-slash"></i></div>
														@endif
													</div>
												</div>
											</div>
										@endforeach
									</div>
								</div>
							</div><!-- /.card -->
						</div>
					</div>
				@endif
				
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content -->

	</div>
	<!-- /.content-wrapper -->
@endsection