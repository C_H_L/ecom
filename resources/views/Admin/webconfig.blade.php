@extends(env('ADMIN_FOLDER') . 'layouts.master')
@section('webconfig') active @endsection

@section('content')
<script>
	$(document).ready(function()
	{
		
	});
</script>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
	  <div class="container-fluid">
		<div class="row mb-2">
		  <div class="col-sm-6">
			<h1 class="m-0 text-dark">Website Config</h1>
		  </div><!-- /.col -->
		</div><!-- /.row -->
	  </div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<div class="content">
	  <div class="container-fluid">
		<div class="row">
		  <div class="col-lg-12">
			<form id="modify-form" method="POST" action="{{ route('admin.webconfig.submit') }}">
				{{ csrf_field() }}
					
				<div class="card card-primary card-outline">
				  <div class="card-body">
					<div class="row">
						<div class="col-sm-10">
							@foreach($webConfigs as $webConfig)
								<label>{{ $webConfig->name }}</label>
								<div class="row">
									<div class="col-md-5">
										@if(!empty($webConfig->getValueNames()) && count($webConfig->getValueNames()) > 0)
											<script>
												$(document).ready(function()
												{
													$("select[name='{{ $webConfig->name }}'] option[value='{{ old($webConfig->name, $webConfig->value) }}']").prop("selected", true);
												});
											</script>
											
											<div class="form-group">
												<select name="{{ $webConfig->name }}" class="form-control" />
													@foreach($webConfig->getValueNames() as $key=>$value)
														<option value="{{ $key }}">{{ $value }}</option>
													@endforeach
												</select>
											</div>
										@else
											<div class="form-group">
												<input type="text" name="{{ $webConfig->name }}" class="form-control" value="{{ old($webConfig->name, $webConfig->value) }}" placeholder="Value" />
											</div>
										@endif
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<input type="text" name="{{ $webConfig->name }}_unit" class="form-control" value="{{ old($webConfig->name . '_unit', $webConfig->unit) }}" placeholder="Unit" />
										</div>
									</div>
									<div class="col-md-5">
										<div class="note">{{ $webConfig->description }}</div>
									</div>
								</div>
							@endforeach
							
						</div>
						<div class="col-sm-6">
							
						</div>
					</div>
				  </div>
				  <div class="card-footer">
						<div class="note">* The units are used for display purposes only, changing it wont alter its calculations</div>
						<button type="submit" class="btn btn-primary">Change</button>
				  </div>
				</div><!-- /.card -->
			</form>
		  </div>
		  <!-- /.col-md-6 -->
		  
		</div>
		<!-- /.row -->
	  </div><!-- /.container-fluid -->
	</div>
	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection