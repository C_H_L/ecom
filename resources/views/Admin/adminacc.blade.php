@extends(env("ADMIN_FOLDER") . 'layouts.master')

@section('adminacc') active @endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
	  <div class="container-fluid">
		<div class="row mb-2">
		  <div class="col-sm-6">
			<h1 class="m-0 text-dark">Admin Account</h1>
		  </div><!-- /.col -->
		</div><!-- /.row -->
	  </div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<div class="content">
	  <div class="container-fluid">
		<div class="row">
		  <div class="col-lg-12">
			<form id="modify-form" method="POST" action="{{ route('admin.adminacc.submit') }}">
				{{ csrf_field() }}
				
				<div class="card card-primary card-outline">
				  <div class="card-body">
					<div class="form-group">
						<label>Current Pasword</label>
						<input type="password" name="cpassword" class="form-control" placeholder="Current password" value="{{ old('cpass') }}" />
					</div>
					
					<div class="form-group">
						<label>New Pasword</label>
						<input type="password" name="password" class="form-control" placeholder="New password" value="{{ old('npass') }}" />
					</div>
					
					<div class="form-group">
						<label>Confirm New Pasword</label>
						<input type="password" name="password_confirmation" class="form-control" placeholder="Confirm password" value="{{ old('cnpass') }}" />
					</div>
					
				  </div>
				  <div class="card-footer">
					<button type="submit" class="btn btn-success">Change</submit>
				  </div>
				</div><!-- /.card -->
			</form>
		  </div>
		  <!-- /.col-md-6 -->
		  
		</div>
		<!-- /.row -->
	  </div><!-- /.container-fluid -->
	</div>
	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection