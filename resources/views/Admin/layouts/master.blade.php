@php
	if(empty($unauth)) $unauth = false;
@endphp

<!doctype html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link href="{{ asset('images/logo.png') }}" rel="shortcut icon" />

		<title>{{ config('app.name', 'Laravel') }} | Admin</title>

		<!-- Font Awesome Icons -->
		<link rel="stylesheet" href="{{ asset(env('ADMIN_FOLDER') . 'plugins/font-awesome/css/font-awesome.min.css') }}">
		<!-- Theme style -->
		<link rel="stylesheet" href="{{ asset(env('ADMIN_FOLDER') . 'css/adminlte.min.css') }}">
		<!-- Google Font: Source Sans Pro -->
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
		
		<!-- jQuery UI -->
		<link rel="stylesheet" href="{{ asset(env('ADMIN_FOLDER') . 'plugins/jqueryUI/jquery-ui.min.css') }}">
		
		<link rel="stylesheet" href="{{ asset(env('ADMIN_FOLDER') . 'plugins/timepicker/bootstrap-timepicker.css') }}">
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

		<!-- CSS And JavaScript -->
		<!--<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->

		<!-- jQuery -->
		<script src="{{ asset(env('ADMIN_FOLDER') . 'plugins/jquery/jquery.min.js') }}"></script>
		<!-- jQuery UI -->
		<script src="{{ asset(env('ADMIN_FOLDER') . 'plugins/jqueryUI/jquery-ui.min.js') }}"></script>
		<!-- Bootstrap 4 -->
		<script src="{{ asset(env('ADMIN_FOLDER') . 'plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
		<!-- AdminLTE App -->
		<script src="{{ asset(env('ADMIN_FOLDER') . 'js/adminlte.min.js') }}"></script>
		<!-- fullCalendar -->
		<link rel="stylesheet" href="{{ asset(env('ADMIN_FOLDER') . 'plugins/fullcalendar-3.10.0/fullcalendar.css') }}">
		<script src="{{ asset(env('ADMIN_FOLDER') . 'plugins/fullcalendar-3.10.0/lib/moment.min.js') }}"></script>
		<script src="{{ asset(env('ADMIN_FOLDER') . 'plugins/fullcalendar-3.10.0/fullcalendar.js') }}"></script>

		<script src="{{ asset(env('ADMIN_FOLDER') . 'plugins/timepicker/bootstrap-timepicker.js') }}"></script>
		
		<!-- REQUIRED SCRIPTS -->
		<script src="{{ asset(env('ADMIN_FOLDER') . 'js/bootbox.min.js') }}"></script>

		<link rel="stylesheet" href="{{ asset(env('ADMIN_FOLDER') . 'js/dropzone/dropzone.css') }}">
		<script src="{{ asset(env('ADMIN_FOLDER') . 'js/dropzone/dropzone.js') }}"></script>

		<script src="{{ asset(env('ADMIN_FOLDER') . 'plugins/datatables/jquery.dataTables.js') }}"></script>
		<link rel="stylesheet" href="{{ asset(env('ADMIN_FOLDER') . 'plugins/datatables/dataTables.bootstrap4.css') }}">
		<script src="{{ asset(env('ADMIN_FOLDER') . 'plugins/datatables/dataTables.bootstrap4.js') }}"></script>

		<script src="{{ asset(env('ADMIN_FOLDER') . 'js/ckeditor/ckeditor.js') }}"></script>

		<script src="{{ asset(env('ADMIN_FOLDER') . 'plugins/daterangepicker/moment.js') }}"></script>
		<script src="{{ asset(env('ADMIN_FOLDER') . 'plugins/daterangepicker/daterangepicker.js') }}"></script>
		<link rel="stylesheet" href="{{ asset(env('ADMIN_FOLDER') . 'plugins/daterangepicker/daterangepicker-bs3.css') }}">
		
		<script src="{{ asset(env('ADMIN_FOLDER') . 'js/sameheight/sameheight.js') }}"></script>

		<link rel="stylesheet" href="{{ asset(env('ADMIN_FOLDER') . 'css/layouts/global.css') }}">
		<link rel="stylesheet" href="{{ asset(env('ADMIN_FOLDER') . 'css/layouts/media.css') }}">
		<link rel="stylesheet" href="{{ asset(env('ADMIN_FOLDER') . 'css/layouts/listing.css') }}">
		<link rel="stylesheet" href="{{ asset(env('ADMIN_FOLDER') . 'css/layouts/single.css') }}">
		<link rel="stylesheet" href="{{ asset(env('ADMIN_FOLDER') . 'css/layouts/category.css') }}">
		<link rel="stylesheet" href="{{ asset(env('ADMIN_FOLDER') . 'css/layouts/profile.css') }}">
		<link rel="stylesheet" href="{{ asset(env('ADMIN_FOLDER') . 'css/layouts/unauth.css') }}">
		<script src="{{ asset(env('ADMIN_FOLDER') . 'js/layouts/global.js') }}"></script>
		
		<script>
			function initMap()
			{
				$(".input-map").each(function()
				{
					var _this = this;
					var lat = parseFloat($(_this).closest(".modify-form").find("input[name='" + $(_this).attr("data-lat-name") + "']").getVal());
					var lng = parseFloat($(_this).closest(".modify-form").find("input[name='" + $(_this).attr("data-long-name") + "']").getVal());
					
					var inputMap = new google.maps.Map(_this,
					{
						zoom: (lat && lng ? 15 : 6 ),
						center:
						{
							lat: (lat ? lat : 3.1385036),
							lng: (lng ? lng : 101.6169484),
						},
					});
					
					var marker = new google.maps.Marker(
					{
						position:
						{
							lat: lat,
							lng: lng,
						},
						animation: google.maps.Animation.DROP,
						map: inputMap,
					});
					
					var inputField = $('<input type="text" class="search-box" placeholder="Search..." />').appendTo(_this);
					var searchBox = new google.maps.places.SearchBox(inputField[0]);
					inputMap.controls[google.maps.ControlPosition.TOP_CENTER].push(inputField[0]);
					
					searchBox.addListener("places_changed", function(e)
					{
						var places = searchBox.getPlaces();
						
						if(places.length > 0)
						{
							places.forEach(function(place)
							{
								$(_this).closest(".modify-form").find("input[name='" + $(_this).attr("data-lat-name") + "']").setVal(place.geometry.location.lat());
								$(_this).closest(".modify-form").find("input[name='" + $(_this).attr("data-long-name") + "']").setVal(place.geometry.location.lng());
								
								marker.setPosition(
								{
									lat: place.geometry.location.lat(),
									lng: place.geometry.location.lng(),
								});
								/*inputMap.panTo(
								{
									lat: place.geometry.location.lat(),
									lng: place.geometry.location.lng(),
								});*/
							});
						}
					});
					
					inputMap.addListener("click", function(e)
					{
						$(_this).closest(".modify-form").find("input[name='" + $(_this).attr("data-lat-name") + "']").setVal(e.latLng.lat());
						$(_this).closest(".modify-form").find("input[name='" + $(_this).attr("data-long-name") + "']").setVal(e.latLng.lng());
						
						marker.setPosition(
						{
							lat: e.latLng.lat(),
							lng: e.latLng.lng(),
						});
						/*inputMap.panTo(
						{
							lat: e.latLng.lat(),
							lng: e.latLng.lng(),
						});*/
						
						/*bootbox.confirm(
						{
							message: "Update address too?",
							backdrop: true,
							buttons:
							{
								confirm:
								{
									label: "Yes",
									className: "btn-success",
								},
								cancel:
								{
									label: "No",
								},
							},
							callback: function(result)
							{
								if(result)
								{
									var geoCoder = new google.maps.Geocoder();
									
									geoCoder.geocode(
									{
										location: e.latLng,
									},
									function(result, status)
									{
										if(status == google.maps.GeocoderStatus.OK)
										{
											console.log(result);
										}
										else
										{
											bootboxError("Google Maps failed to resolve address");
										}
									});
								}
							},
						});*/
					});
				});
			}
		</script>
		<!-- Google Maps API -->
		<!-- Callback function must exist before loading library -->
		<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ env('MAPS_GOOGLE_API_KEY') }}&libraries=places&callback=initMap" type="text/javascript"></script>

		<script>
			var libURL = "{{ asset(env('ADMIN_FOLDER') . '/lib/') . '/' }}";

			var bladeAsset = "{{ asset(env('ADMIN_FOLDER') . '/') }}";
			var bladeURL = "{{ url('/') . '/' }}";

			var csrfToken = '{{ csrf_token() }}';
			var csrfField = '{{ csrf_field() }}';

			$(document).ready(function()
			{
				@if(count($errors->formerror->all()) > 0)
					bootbox.alert(
					{
						message: '<b><i class="fa fa-exclamation-circle" style="color: red;"></i>&nbsp; Please resolve the following errors:</b></br>'
						@foreach($errors->formerror->all() as $error)
							+ '</br> {{ $error }}'
						@endforeach
						,
						backdrop: true,
					});
				@endif

				@if(Session::has("formsuccess"))
					bootbox.alert(
					{
						message: '<b>{{ Session::get("formsuccess") }}</b>',
						backdrop: true,
					});
				@endif
				
				@if(Session::has("bootboxMSG"))
					bootbox.alert({
						message: '<b>{!! Session::get("bootboxMSG") !!}</b>',
						backdrop: true,
					});
				@endif
				
				@if(Session::has("bootboxError"))
					bootboxError('{!! Session::get("bootboxError") !!}');
				@endif
				
				@if(Session::has("bootboxSuccess"))
					bootboxSuccess('{!! Session::get("bootboxSuccess") !!}');
				@endif
				
				@if(Session::has("showBootstrapModal"))
					$("{{ Session::get('showBootstrapModal') }}").modal("show");
				@endif
			});
		</script>
		@yield('head')
	</head>
	<body class="hold-transition sidebar-mini">
		<div class="wrapper">
			@if(!$unauth)
				<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
					<ul class="nav navbar-nav">
						<li class="nav-item">
							<a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
						</li>
					</ul>
					
					<ul class="nav navbar-nav navbar-right" style="position: relative;right: 0;">
						@auth
							<li class="nav-item">
								<a href="{{ route('doctor.profile') }}" class="nav-link">
									<i class="fa fa-user"></i>&nbsp; {{ Auth::user()->name }}
								</a>
							</li>
							<li class="nav-item">
								<a href="{{ route('admin.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="nav-link" style="margin-right: .5rem;">
									<i class="fa fa-sign-out"></i> Logout
								</a>

								<form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>
							</li>
						@endauth
					</ul>
				</nav>
				
				<aside class="main-sidebar sidebar-dark-primary elevation-4">
					<a href="{{ route('admin.index') }}" class="brand-link">
						<img src="{{ asset(env('ADMIN_FOLDER') . 'img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
						style="opacity: .8">
						<span class="brand-text font-weight-light">Admin Panel</span>
					</a>

					<div class="sidebar">
						<nav class="mt-2">
							<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
								<li class="nav-item">
									<a href="{{ route('doctor.index') }}" class="nav-link @yield('index')">
										<i class="nav-icon fa fa-dashboard"></i>
										<p>
											Dashboard
										</p>
									</a>
								</li>
								<li class="nav-item has-treeview">
									<a href="{{ route('doctor.profile') }}" class="nav-link @yield('profile')">
										<i class="nav-icon fa fa-user"></i>
										<p>
											Profile
											<i class="right fa fa-angle-left"></i>
										</p>
									</a>
									
									<ul class="nav nav-treeview">
										<li class="nav-item">
											<a href="{{ route('doctor.profile') . '#profile' }}" class="nav-link @yield('profile.details')">
												<i class="nav-icon fa fa-list"></i>
												<p>
													Details
												</p>
											</a>
										</li>
										<li class="nav-item">
											<a href="{{ route('doctor.profile') . '#address' }}" class="nav-link @yield('profile.address')">
												<i class="nav-icon fa fa-address-book-o"></i>
												<p>
													Address
												</p>
											</a>
										</li>
										<li class="nav-item">
											<a href="{{ route('doctor.profile') . '#treatment' }}" class="nav-link @yield('profile.treatment')">
												<i class="nav-icon fa fa-stethoscope"></i>
												<p>
													Treatment
												</p>
											</a>
										</li>
										<li class="nav-item">
											<a href="{{ route('doctor.profile') . '#password' }}" class="nav-link @yield('profile.password')">
												<i class="nav-icon fa fa-key"></i>
												<p>
													Password
												</p>
											</a>
										</li>
									</ul>
								</li>
								<li class="nav-item">
									<a href="{{ route('doctor.session') }}" class="nav-link @yield('session')">
										<i class="nav-icon fa fa-calendar"></i>
										<p>
											Session
										</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ route('doctor.appointment') }}" class="nav-link @yield('appointment')">
										<i class="nav-icon fa fa-briefcase"></i>
										<p>
											Appointment
										</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ route('doctor.notification') }}" class="nav-link @yield('notification')">
										<i class="nav-icon fa fa-bell"></i>
										<p>
											Notification
										</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ route('doctor.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="nav-link @yield('logout')">
										<i class="nav-icon fa fa-sign-out"></i>
										<p>
											Logout
										</p>
									</a>
								</li>
							</ul>
						</nav>
					</div>
				</aside>
			@endif

			<div class="overlay-loading"></div>
			
			@yield('content')

			@if(!$unauth)
				<aside class="control-sidebar control-sidebar-dark">
					<div class="p-3">
						<h5>Title</h5>
						<p>Sidebar content</p>
					</div>
				</aside>

				<footer class="main-footer">
					<div class="float-right d-none d-sm-inline"></div>
					<strong>Copyright &copy; {{ date("Y") }} {{ config('app.name', 'Laravel') }}</strong> All rights reserved.
				</footer>
			@endif
		</div>
	</body>
</html>
