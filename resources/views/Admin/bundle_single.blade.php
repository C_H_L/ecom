@extends(env('ADMIN_FOLDER') . 'layouts.master')
@section('bundle') active @endsection

@section('content')
	<script>
		$(document).ready(function()
		{
			@if(!empty($bundle))
				$(".modify-form .record-delete").click(function()
				{
					bootbox.confirm(
					{
						message: "Confirm delete record",
						backdrop: true,
						buttons:
						{
							confirm:
							{
								label: "Delete",
								className: "btn-danger",
							},
							cancel:
							{
								label: "Cancel",
							},
						},
						callback: function(result)
						{
							if(result)
							{
								$(".overlay-loading").show();
								window.location.href = "{{ route('admin.bundle.delete', ['id' => $bundle->id]) }}";
							}
						},
					});
				});
				
				$(".modify-form .record-toggleactive").click(function()
				{
					bootbox.dialog(
					{
						message: 'Confirm set bundle as @if($bundle->active) inactive @else active @endif?',
						backdrop: true,
						buttons:
						{
							cancel:
							{
								label: 'Cancel',
							},
							confirm:
							{
								label: 'Confirm',
								className: "btn-warning",
								callback: function()
								{
									window.location.href = "{{ route('admin.bundle.toggleactive', ['id' => $bundle->id]) }}";
								},
							},
						},
					});
				});
				
				$(".record-media .record-modify").click(function()
				{
					var actionURL = $(this).attr("data-action-url");
					var modalElement = $("#record-media-modify");
					var modalForm = modalElement.find("form");
					
					$(".overlay-loading").show();
					
					$.post(
					{
						url: actionURL,
						data:
						{
							"_token": "{{ csrf_token() }}",
						},
						success: function(response)
						{
							$.each(response, function(key, value)
							{
								var formElement = modalElement.find("[name='" + key + "']");
								
								if(formElement.attr("type") == "checkbox")
								{
									formElement.prop("checked", (value == 1 ? true : false));
								}
								else
								{
									formElement.val(value);
								}
							});
							
							modalElement.modal('show');
							$(".overlay-loading").hide();
							
							modalForm.submit(function(e)
							{
								e.preventDefault();
								
								$(".overlay-loading").show();
								$.post(
								{
									url: actionURL + "/submit",
									data: modalForm .serialize(),
									complete: function()
									{
										$(".overlay-loading").hide();
									},
									success: function(response)
									{
										if(response == "success")
										{
											window.location.reload();
										}
										else
										{
											bootboxError(response);
										}
									},
									error: function(response)
									{
										bootboxError(response);
									},
								});
							});
						},
						error: function(response)
						{
							bootboxError(response);
						},
					});
				});
				
				$(".record-media .record-delete").click(function()
				{
					var actionURL = $(this).attr("data-action-url");
					
					bootbox.confirm(
					{
						message: "Confirm delete bundle image?",
						backdrop: true,
						buttons:
						{
							cancel:
							{
								label: "Cancel",
								className: "btn-default"
							},
							confirm:
							{
								label: "Delete",
								className: "btn-danger",
							}
						},
						callback: function(result)
						{
							if(result)
							{
								$(".overlay-loading").show();
								
								$.get(actionURL, function(response)
								{
									if(response == "success")
									{
										$(".overlay-loading").hide();
										window.location.reload();
									}
								});
							}
						}
					});
				});
			@endif
			
			ClassicEditor
				.create(document.querySelector("#ckeditor_title"))
				.catch(error =>
				{
					console.error(error);
				});
			
			ClassicEditor
				.create(document.querySelector("#ckeditor_description"))
				.catch(error =>
				{
					console.error(error);
				});
			
		});
	</script>
	
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2 mt-1">
					<div class="col-sm-6">
						<a href="{{ route('admin.bundle') }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i>&nbsp; Back</a>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content-header -->

		<!-- Main content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<form class="modify-form" method="POST" action="{{ (!empty($bundle) ?  route('admin.bundle.single.submit', ['id' => $bundle->id])  : route('admin.bundle.add.submit')) }}">
							{{ csrf_field() }}
							
							<div class="card @if(!empty($bundle)) card-primary @else card-success @endif">
								<div class="card-header">
									<h3 class="card-title">@if(!empty($bundle)) {{ $bundle->name }} @else Add Bundle @endif</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="card-body">
									@if(!empty($bundle))
										<div class="clearfix">
											<button type="button" class="btn btn-sm btn-danger btn-float-right record-delete"><i class="fa fa-trash"></i></button>
											<button type="button" class="btn btn-sm btn-warning btn-float-right record-toggleactive">Set @if($bundle->active) inactive @else active @endif</button>
										</div>
										<hr />
										
										<table class="record-single-table">
											<tr>
												<td>ID</td>
												<td>{{ $bundle->id }}</td>
												<td>Created Time</td>
												<td>{{ $bundle->created_at_text }}</td>
											</tr>
											<tr>
												<td>Active</td>
												<td>{{ $bundle->active_text }}</td>
											</tr>
										</table>
									@endif

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label>Name</label>
												<input type="text" class="form-control" name="name" value="{{ old('name', !empty($bundle) ? $bundle->name : '') }}" placeholder="Name" />
											</div>
											
											<div class="form-group">
												<label>Sorting Index</label>
												<input type="number" class="form-control" name="sort" value="{{ old('sort', !empty($bundle) ? $bundle->sort : $sort) }}" placeholder="Index" />
											</div>
											
											<div class="form-group">
												<label>Price</label>
												<input type="number" class="form-control" name="price" step="any" value="{{ old('price', !empty($bundle) ? $bundle->price : '') }}" placeholder="Price" />
											</div>
											
											@if(empty($bundle))
												<div class="form-check">
													<label class="form-check-label"><input type="checkbox" class="form-check-input" name="active" @if(empty($bundle) || $bundle->active) checked @endif /> Active</label>
												</div>
											@endif
											
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label>Title</label>
												<textarea id="ckeditor_title" class="form-control" name="title" placeholder="Title">{{ old('title', !empty($bundle) ? $bundle->title : '') }}</textarea>
											</div>
											
											<div class="form-group">
												<label>Description</label>
												<textarea id="ckeditor_description" class="form-control" name="description" placeholder="Description">{{ old('description', !empty($bundle) ? $bundle->description : '') }}</textarea>
											</div>
											
										</div>
									</div>
								</div>
								<div class="card-footer">
									@if(!empty($bundle))
										<button type="submit" class="btn btn-primary">Change</button>
									@else
										<button type="submit" class="btn btn-success">Add</button>
									@endif
								</div>
							</div><!-- /.card -->
						</form>
					</div>
					<!-- /.col-lg-12 -->

				</div>
				<!-- /.row -->
				
				@if(!empty($bundle))
					<div class="row">
						<div class="col-lg-12">
							<div id="bundle_item" class="card card-info">
								<div class="card-header">
									<h3 class="card-title">Bundle Items</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="card-body">
									<a href="{{ route('admin.bundle.item.add', ['bundle_id' => $bundle->id]) }}" class="btn btn-sm btn-success">Add Bundle Item</a>
									<hr />

									<table class="table table-striped table-bordered record-list-table">
										<thead>
											<tr>
												<th style="width: 3%;">
													ID
												</th>
												<th style="width: 5%;">
													Product
												</th>
												<th style="width: 5%;">
													SKU
												</th>
												<th style="width: 5%;">
													Quantity
												</th>
												<th style="width: 5%;">
													Active
												</th>
												<th style="width: 10%;">
													Created Time
												</th>
											</tr>
										</thead>
										<tbody>
											@foreach($bundle_items as $item)
												<tr data-href="{{ route('admin.bundle.item.single', ['bundle_id' => $bundle->id, 'id' => $item->id]) }}">
													<td>
														{{ $item->id }}
													</td>
													<td>
														<a href="{{ route('admin.product.single', ['id' => $item->product_id]) }}">{{ $item->Product->name }}</a>
													</td>
													<td>
														@if(!empty($item->Sku) && count($item->Sku) > 0)
															<a href="{{ route('admin.product.sku.single', ['product_id' => $item->product_id, 'id' => $item->sku_id]) }}">{{ $item->Sku->name }}</a>
														@else
															N/A
														@endif
													</td>
													<td>
														{{ $item->quantity }}
													</td>
													<td>
														{{ $item->active_text }}
													</td>
													<td>
														{{ $item->created_at_text }}
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div><!-- /.card -->
						</div>
					</div>
					
					<div class="row">
						<div class="col-lg-12">
							<div id="bundle_image" class="card card-info">
								<div class="card-header">
									<h3 class="card-title">Bundle Images</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="card-body">
									<div class="row">
										<div class="col-md-12">
											<form method="POST" action="{{ route('admin.bundle.image.upload', ['bundle_id' => $bundle->id]) }}" id="dropzone-media" class="dropzone dropzone-media" enctype="multipart/form-data">
												{{ csrf_field() }}
											</form>
											</br>
										</div>
										@foreach($bundle_images as $image)
											<div class="col-md-3 same-height">
												<div class="record-media">
													<img src="{{ $image->image_asset }}" class="media" />
													
													<div class="overlay">
														<div class="control record-modify" data-action-url="{{ route('admin.bundle.image.single', ['bundle_id' => $image->bundle_id, 'id' => $image->id]) }}"><i class="fa fa-edit"></i></div>
														<div class="control record-delete" data-action-url="{{ route('admin.bundle.image.delete', ['bundle_id' => $image->bundle_id,'id' => $image->id]) }}"><i class="fa fa-trash"></i></div>
														
														@if($image->active == 0)
															<div class="not-active"><i class="fa fa-eye-slash"></i></div>
														@endif
													</div>
												</div>
											</div>
										@endforeach
									</div>
								</div>
							</div><!-- /.card -->
						</div>
					</div>
				@endif
				
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content -->

	</div>
	<!-- /.content-wrapper -->
@endsection