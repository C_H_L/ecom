@extends(env('ADMIN_FOLDER') . 'layouts.master', ['unauth' => true])

@section('content')
	<div class="unauth-wrapper">
		<div class="unauth-container">
			<div class="row">
				<div class="col-md-12 text-center">
					<img src="{{ asset('images/logo.png') }}" class="logo" />
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<form class="form-horizontal" method="POST" action="{{ route('admin.login') }}">
						{{ csrf_field() }}

						<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
							<div class="row">
								<label for="username" class="col-md-4 control-label">Username</label>

								<div class="col-md-12">
									<input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

									@if ($errors->has('username'))
										<span class="help-block text-danger">
											<strong>{{ $errors->first('username') }}</strong>
										</span>
									@endif
								</div>
							</div>
						</div>

						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<div class="row">
								<label for="password" class="col-md-4 control-label">Password</label>

								<div class="col-md-12">
									<input id="password" type="password" class="form-control" name="password" required>

									@if ($errors->has('password'))
										<span class="help-block text-danger">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
									@endif
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
										</label>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary btn-full-width">Login</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
