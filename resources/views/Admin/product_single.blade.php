@extends(env('ADMIN_FOLDER') . 'layouts.master')
@section('product') active @endsection

@section('content')
	<script>
		$(document).ready(function()
		{
			@if(!empty($product))
				$(".modify-form .record-delete").click(function()
				{
					bootbox.confirm(
					{
						message: "Confirm delete record",
						backdrop: true,
						buttons:
						{
							confirm:
							{
								label: "Delete",
								className: "btn-danger",
							},
							cancel:
							{
								label: "Cancel",
							},
						},
						callback: function(result)
						{
							if(result)
							{
								$(".overlay-loading").show();
								window.location.href = "{{ route('admin.product.delete', ['id' => $product->id]) }}";
							}
						},
					});
				});
				
				$(".modify-form .record-toggleactive").click(function()
				{
					bootbox.dialog(
					{
						message: 'Confirm set product as @if($product->active) inactive @else active @endif?',
						backdrop: true,
						buttons:
						{
							cancel:
							{
								label: 'Cancel',
							},
							confirm:
							{
								label: 'Confirm',
								className: "btn-warning",
								callback: function()
								{
									window.location.href = "{{ route('admin.product.toggleactive', ['id' => $product->id]) }}";
								},
							},
						},
					});
				});
				
				$(".record-media .record-modify").click(function()
				{
					var actionURL = $(this).attr("data-action-url");
					var modalElement = $("#record-media-modify");
					var modalForm = modalElement.find("form");
					
					$(".overlay-loading").show();
					
					$.post(
					{
						url: actionURL,
						data:
						{
							"_token": "{{ csrf_token() }}",
						},
						success: function(response)
						{
							$.each(response, function(key, value)
							{
								var formElement = modalElement.find("[name='" + key + "']");
								
								if(formElement.attr("type") == "checkbox")
								{
									formElement.prop("checked", (value == 1 ? true : false));
								}
								else
								{
									formElement.val(value);
								}
							});
							
							modalElement.modal('show');
							$(".overlay-loading").hide();
							
							modalForm.submit(function(e)
							{
								e.preventDefault();
								
								$(".overlay-loading").show();
								$.post(
								{
									url: actionURL + "/submit",
									data: modalForm .serialize(),
									complete: function()
									{
										$(".overlay-loading").hide();
									},
									success: function(response)
									{
										if(response == "success")
										{
											window.location.reload();
										}
										else
										{
											bootboxError(response);
										}
									},
									error: function(response)
									{
										bootboxError(response);
									},
								});
							});
						},
						error: function(response)
						{
							bootboxError(response);
						},
					});
				});
				
				$(".record-media .record-delete").click(function()
				{
					var actionURL = $(this).attr("data-action-url");
					
					bootbox.confirm(
					{
						message: "Confirm delete product image?",
						backdrop: true,
						buttons:
						{
							cancel:
							{
								label: "Cancel",
								className: "btn-default"
							},
							confirm:
							{
								label: "Delete",
								className: "btn-danger",
							}
						},
						callback: function(result)
						{
							if(result)
							{
								$(".overlay-loading").show();
								
								$.get(actionURL, function(response)
								{
									if(response == "success")
									{
										$(".overlay-loading").hide();
										window.location.reload();
									}
								});
							}
						}
					});
				});
			@endif
			
		});
	</script>
	
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2 mt-1">
					<div class="col-sm-6">
						<a href="{{ route('admin.product') }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i>&nbsp; Back</a>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content-header -->

		<!-- Main content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<form class="modify-form" method="POST" action="{{ (!empty($product) ?  route('admin.product.single.submit', ['id' => $product->id])  : route('admin.product.add.submit')) }}">
							{{ csrf_field() }}
							
							<div class="card @if(!empty($product)) card-primary @else card-success @endif">
								<div class="card-header">
									<h3 class="card-title">@if(!empty($product)) {{ $product->name }} @else Add Product @endif</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="card-body">
									@if(!empty($product))
										<div class="clearfix">
											<button type="button" class="btn btn-sm btn-danger btn-float-right record-delete"><i class="fa fa-trash"></i></button>
											<button type="button" class="btn btn-sm btn-warning btn-float-right record-toggleactive">Set @if($product->active) inactive @else active @endif</button>
										</div>
										<hr />
										
										<table class="record-single-table">
											<tr>
												<td>ID</td>
												<td>{{ $product->id }}</td>
												<td>Created Time</td>
												<td>{{ $product->created_at_text }}</td>
											</tr>
											<tr>
												<td>Active</td>
												<td>{{ $product->active_text }}</td>
											</tr>
										</table>
									@endif

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label>Name</label>
												<input type="text" class="form-control" name="name" value="{{ old('name', !empty($product) ? $product->name : '') }}" placeholder="Name" />
											</div>
											
											@if(empty($product))
												<div class="form-check">
													<label class="form-check-label"><input type="checkbox" class="form-check-input" name="active" @if(empty($product) || $product->active) checked @endif /> Active</label>
												</div>
											@endif
											
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label>Sorting Index</label>
												<input type="number" class="form-control" name="sort" value="{{ old('sort', !empty($product) ? $product->sort : $sort) }}" placeholder="Index" />
											</div>
											
										</div>
									</div>
								</div>
								<div class="card-footer">
									@if(!empty($product))
										<button type="submit" class="btn btn-primary">Change</button>
									@else
										<button type="submit" class="btn btn-success">Add</button>
									@endif
								</div>
							</div><!-- /.card -->
						</form>
					</div>
					<!-- /.col-lg-12 -->

				</div>
				<!-- /.row -->
				
				@if(!empty($product))
					<div class="row">
						<div class="col-lg-12">
							<div id="sku" class="card card-info">
								<div class="card-header">
									<h3 class="card-title">Skus</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="card-body">
									<a href="{{ route('admin.product.sku.add', ['product_id' => $product->id]) }}" class="btn btn-sm btn-success">Add SKU</a>
									<hr />

									<table class="table table-striped table-bordered record-list-table">
										<thead>
											<tr>
												<th style="width: 3%;">
													ID
												</th>
												<th style="width: 5%;">
													Sort
												</th>
												<th style="width: 10%;">
													Name
												</th>
												<th style="width: 5%;">
													Price
												</th>
												<th style="width: 5%;">
													Direct Purchase
												</th>
												<th style="width: 5%;">
													Active
												</th>
												<th style="width: 10%;">
													Created Time
												</th>
											</tr>
										</thead>
										<tbody>
											@foreach($skus as $sku)
												<tr data-href="{{ route('admin.product.sku.single', ['product_id' => $product->id, 'id' => $sku->id]) }}">
													<td>
														{{ $sku->id }}
													</td>
													<td>
														{{ $sku->sort }}
													</td>
													<td>
														{{ $sku->name }}
													</td>
													<td>
														{{ $sku->price }}
													</td>
													<td>
														{{ $sku->direct_purchase_text }}
													</td>
													<td>
														{{ $sku->active_text }}
													</td>
													<td>
														{{ $sku->created_at_text }}
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div><!-- /.card -->
						</div>
					</div>
				@endif
				
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content -->

	</div>
	<!-- /.content-wrapper -->
@endsection