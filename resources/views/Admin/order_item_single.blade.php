@extends(env('ADMIN_FOLDER') . 'layouts.master')
@section('order') active @endsection

@section('content')
	<script>
		$(document).ready(function()
		{
			
		});
	</script>
	
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2 mt-1">
					<div class="col-sm-6">
						<a href="{{ route('admin.order.single', ['id' => $order_id]) }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i>&nbsp; Back</a>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content-header -->

		<!-- Main content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="card @if(!empty($order_item)) card-primary @else card-success @endif">
							<div class="card-header">
								<h3 class="card-title">@if(!empty($order_item)) Order #{{ $order_id }} Item #{{ $order_item->id }} @else Add Order Item @endif</h3>
								<div class="card-tools">
									<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
								</div>
							</div>
							<div class="card-body">
								@if(!empty($order_item))
									<table class="record-single-table">
										<tr>
											<td>ID</td>
											<td>{{ $order_item->id }}</td>
											<td>Name</td>
											<td>
												@if($order_item->type == 0)
													<a href="{{ route('admin.product.sku.single', ['product_id' => $order_item->Item->product_id, 'id' => $order_item->item_id]) }}">{{ $order_item->Item->name }}</a>
												@elseif($order_item->type == 1)
													<a href="{{ route('admin.bundle.single', ['id' => $order_item->item_id]) }}">{{ $order_item->Item->name }}</a>
												@endif
											</td>
										</tr>
										<tr>
											<td>Quantity</td>
											<td>{{ $order_item->quantity }}</td>
											<td>Unit Price</td>
											<td>RM{{ $order_item->unit_price_text }}</td>
										</tr>
										<tr>
											<td>Total</td>
											<td colspan="3">RM{{ $order_item->total_text }}</td>
										</tr>
									</table>
								@endif
							</div>
						</div><!-- /.card -->
					</div>
					<!-- /.col-lg-12 -->

				</div>
				<!-- /.row -->
				
				@if(!empty($order_item))
					<div class="row">
						<div class="col-lg-12">
							<div id="order_item_sku" class="card card-info">
								<div class="card-header">
									<h3 class="card-title">Order Item SKUs</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="card-body">
									<table class="table table-striped table-bordered record-list-table">
										<thead>
											<tr>
												<th style="width: 5%;">
													ID
												</th>
												<th style="width: 15%;">
													SKU
												</th>
											</tr>
										</thead>
										<tbody>
											@foreach($order_item->OrderItemSku as $sku)
												<tr>
													<td>
														{{ $sku->id }}
													</td>
													<td>
														<a href="{{ route('admin.product.sku.single', ['product_id' => $sku->Sku->product_id, 'id' => $sku->sku_id]) }}">{{ $sku->Sku->name }}</a>
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div><!-- /.card -->
						</div>
					</div>
				@endif
				
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content -->

	</div>
	<!-- /.content-wrapper -->
@endsection