@extends(env('ADMIN_FOLDER') . 'layouts.master')

@section('coupon') active @endsection

@section('content')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">Coupons</h1>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content-header -->

		<!-- Main content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-primary card-outline">
							<div class="card-body">
								<a href="{{ route('admin.coupon.add') }}" class="btn btn-sm btn-success">Add Coupon</a>
								<hr />

								<table class="table table-striped table-bordered record-list-table">
									<thead>
										<tr>
											<th style="width: 3%;">
												ID
											</th>
											<th style="width: 5%;">
												Name
											</th>
											<th style="width: 5%;">
												Amount
											</th>
											<th style="width: 10%;">
												Validity Start
											</th>
											<th style="width: 10%;">
												Validity End
											</th>
											<th style="width: 5%;">
												Active
											</th>
											<th style="width: 10%;">
												Created Time
											</th>
										</tr>
									</thead>
									<tbody>
										@foreach($coupons as $coupon)
											<tr data-href="{{ route('admin.coupon.single', ['id' => $coupon->id]) }}">
												<td>
													{{ $coupon->id }}
												</td>
												<td>
													{{ $coupon->name }}
												</td>
												<td>
													{{ $coupon->amount_text }}
												</td>
												<td>
													{{ $coupon->start_time_text }}
												</td>
												<td>
													{{ $coupon->end_time_text }}
												</td>
												<td>
													{{ $coupon->active_text }}
												</td>
												<td>
													{{ $coupon->created_at_text }}
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div><!-- /.card -->
					</div>
					<!-- /.col-md-6 -->

				</div>
				<!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content -->

	</div>
	<!-- /.content-wrapper -->
@endsection