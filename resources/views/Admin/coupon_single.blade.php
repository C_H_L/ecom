@extends(env('ADMIN_FOLDER') . 'layouts.master')
@section('coupon') active @endsection

@section('content')
	<script>
		$(document).ready(function()
		{
			@if(!empty($coupon))
				$(".modify-form .record-delete").click(function()
				{
					bootbox.confirm(
					{
						message: "Confirm delete record",
						backdrop: true,
						buttons:
						{
							confirm:
							{
								label: "Delete",
								className: "btn-danger",
							},
							cancel:
							{
								label: "Cancel",
							},
						},
						callback: function(result)
						{
							if(result)
							{
								$(".overlay-loading").show();
								window.location.href = "{{ route('admin.coupon.delete', ['id' => $coupon->id]) }}";
							}
						},
					});
				});
				
				$(".modify-form .record-toggleactive").click(function()
				{
					bootbox.dialog(
					{
						message: 'Confirm set coupon as @if($coupon->active) inactive @else active @endif?',
						backdrop: true,
						buttons:
						{
							cancel:
							{
								label: 'Cancel',
							},
							confirm:
							{
								label: 'Confirm',
								className: "btn-warning",
								callback: function()
								{
									window.location.href = "{{ route('admin.coupon.toggleactive', ['id' => $coupon->id]) }}";
								},
							},
						},
					});
				});
			@endif
		});
	</script>
	
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2 mt-1">
					<div class="col-sm-6">
						<!--<h1 class="m-0 text-dark">@if(!empty($coupon)) {{ $coupon->name}} @else Add Coupon @endif</h1>-->
						<a href="{{ route('admin.coupon') }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i>&nbsp; Back</a>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content-header -->

		<!-- Main content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<form class="modify-form" method="POST" action="{{ (!empty($coupon) ?  route('admin.coupon.single.submit', ['id' => $coupon->id])  : route('admin.coupon.add.submit')) }}">
							{{ csrf_field() }}
							
							<div class="card @if(!empty($coupon)) card-primary @else card-success @endif">
								<div class="card-header">
									<h3 class="card-title">@if(!empty($coupon)) {{ $coupon->name}} @else Add Coupon @endif</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="card-body">
									@if(!empty($coupon))
										<div class="clearfix">
											<button type="button" class="btn btn-sm btn-danger btn-float-right record-delete"><i class="fa fa-trash"></i></button>
											<button type="button" class="btn btn-sm btn-warning btn-float-right record-toggleactive">Set @if($coupon->active) inactive @else active @endif</button>
										</div>
										<hr />
										
										<table class="record-single-table">
											<tr>
												<td>ID</td>
												<td>{{ $coupon->id }}</td>
												<td>Created Time</td>
												<td>{{ $coupon->created_at_text }}</td>
											</tr>
											<tr>
												<td>Active</td>
												<td>{{ $coupon->active_text }}</td>
												<td>Usage Count</td>
												<td>{{ $coupon->usage_count }}</td>
											</tr>
										</table>
									@endif

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label>Name</label>
												<input type="text" class="form-control" name="name" value="{{ old('name', !empty($coupon) ? $coupon->name : '') }}" placeholder="Name" />
											</div>
											
											<script>
												$(document).ready(function()
												{
													$("select[name='formula']").change(function()
													{
														if($(this).getVal() == 0)
														{
															$(this).closest(".input-group").find(".input-group-append").show();
														}
														else
														{
															$(this).closest(".input-group").find(".input-group-append").hide();
														}
													});
													
													$("select[name='formula']").setVal("{{ old('formula', !empty($coupon) ? $coupon->formula : '') }}");
												});
											</script>
											<div class="form-group">
												<label>Amount</label>
												<div class="input-group">
													<div class="input-group-prepend">
														<select class="form-control" name="formula" />
															@foreach($formula_names as $key=>$value)
																<option value="{{ $key }}">{{ $value }}</option>
															@endforeach
														</select>
													</div>
													<input type="number" class="form-control" name="amount" step="any" value="{{ old('amount', !empty($coupon) ? $coupon->amount_formatted : '') }}" placeholder="Amount" />
													<div class="input-group-append display-none">
														<div class="input-group-text">
															%
														</div>
													</div>
												</div>
											</div>
											
											<div class="form-group">
												<label>Validity Start</label>
												<div class="input-group date">
													<div class="input-group-prepend">
														<span class="input-group-text">
															<input type="checkbox" class="input-toggle" data-target=".modify-form input[name='start_time']">
														</span>
													</div>
													<input type="text" class="form-control datetimepicker" data-datetimepicker-to=".modify-form input[name='end_time']" name="start_time" value="{{ old('start_time', !empty($coupon) ? $coupon->start_time_formatted : '') }}" placeholder="Start Time" disabled />
												</div>
											</div>
											
											@if(empty($coupon))
												<div class="form-check">
													<label class="form-check-label"><input type="checkbox" class="form-check-input" name="active" @if(empty($coupon) || $coupon->active) checked @endif /> Active</label>
												</div>
											@endif
											
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label>Code</label>
												<input type="text" class="form-control" name="code" value="{{ old('code', !empty($coupon) ? $coupon->code : '') }}" placeholder="Code" />
											</div>
											
											<script>
												$(document).ready(function()
												{
													$("select[name='usage_type']").setVal("{{ old('usage_type', !empty($coupon) ? $coupon->usage_type : '') }}");
												});
											</script>
											<div class="form-group">
												<label>Usage Type</label>
												<div class="input-group">
													<select class="form-control input-toggle-conditional" name="usage_type" data-target="1, #usage_target_group" />
														@foreach($usage_types as $key=>$value)
															<option value="{{ $key }}">{{ $value }}</option>
														@endforeach
													</select>
													<div id="usage_target_group" class="input-group-append display-none">
														<input type="number" class="form-control" name="usage_target" value="{{ old('usage_target', !empty($coupon) ? $coupon->usage_target : '') }}" placeholder="Usage Target" />
													</div>
												</div>
											</div>
											
											<div class="form-group">
												<label>Validity End</label>
												<div class="input-group">
													<div class="input-group-prepend">
														<span class="input-group-text">
															<input type="checkbox" class="input-toggle" data-target=".modify-form input[name='end_time']">
														</span>
													</div>
													<input type="text" class="form-control datetimepicker" name="end_time" value="{{ old('end_time', !empty($coupon) ? $coupon->end_time_formatted : '') }}" placeholder="End Time" disabled />
												</div>
											</div>
											
										</div>
									</div>
								</div>
								<div class="card-footer">
									@if(!empty($coupon))
										<button type="submit" class="btn btn-primary">Change</button>
									@else
										<button type="submit" class="btn btn-success">Add</button>
									@endif
								</div>
							</div><!-- /.card -->
						</form>
					</div>
					<!-- /.col-md-6 -->

				</div>
				<!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content -->

	</div>
	<!-- /.content-wrapper -->
@endsection