@extends(env('ADMIN_FOLDER') . 'layouts.master')
@section('bundle') active @endsection

@section('content')
	<script>
		$(document).ready(function()
		{
			@if(!empty($bundle_item))
				$(".modify-form .record-delete").click(function()
				{
					bootbox.confirm(
					{
						message: "Confirm delete record",
						backdrop: true,
						buttons:
						{
							confirm:
							{
								label: "Delete",
								className: "btn-danger",
							},
							cancel:
							{
								label: "Cancel",
							},
						},
						callback: function(result)
						{
							if(result)
							{
								$(".overlay-loading").show();
								window.location.href = "{{ route('admin.bundle.item.delete', ['bundle_id' => $bundle_id, 'id' => $bundle_item->id]) }}";
							}
						},
					});
				});
				
				$(".modify-form .record-toggleactive").click(function()
				{
					bootbox.dialog(
					{
						message: 'Confirm set bundle item as @if($bundle_item->active) inactive @else active @endif?',
						backdrop: true,
						buttons:
						{
							cancel:
							{
								label: 'Cancel',
							},
							confirm:
							{
								label: 'Confirm',
								className: "btn-warning",
								callback: function()
								{
									window.location.href = "{{ route('admin.bundle.item.toggleactive', ['bundle_id' => $bundle_id, 'id' => $bundle_item->id]) }}";
								},
							},
						},
					});
				});
			@endif
			
		});
	</script>
	
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2 mt-1">
					<div class="col-sm-6">
						<a href="{{ route('admin.bundle.single', ['id' => $bundle_id, '#bundle_item']) }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i>&nbsp; Back</a>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content-header -->

		<!-- Main content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<form class="modify-form" method="POST" action="{{ (!empty($bundle_item) ?  route('admin.bundle.item.single.submit', ['bundle_id' => $bundle_id, 'id' => $bundle_item->id])  : route('admin.bundle.item.add.submit', ['bundle_id' => $bundle_id])) }}">
							{{ csrf_field() }}
							
							<div class="card @if(!empty($bundle_item)) card-primary @else card-success @endif">
								<div class="card-header">
									<h3 class="card-title">@if(!empty($bundle_item)) Bundle Item #{{ $bundle_item->id }} @else Add Bundle Item @endif</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="card-body">
									@if(!empty($bundle_item))
										<div class="clearfix">
											<button type="button" class="btn btn-sm btn-danger btn-float-right record-delete"><i class="fa fa-trash"></i></button>
											<button type="button" class="btn btn-sm btn-warning btn-float-right record-toggleactive">Set @if($bundle_item->active) inactive @else active @endif</button>
										</div>
										<hr />
										
										<table class="record-single-table">
											<tr>
												<td>ID</td>
												<td>{{ $bundle_item->id }}</td>
												<td>Created Time</td>
												<td>{{ $bundle_item->created_at_text }}</td>
											</tr>
											<tr>
												<td>Active</td>
												<td>{{ $bundle_item->active_text }}</td>
											</tr>
										</table>
									@endif

									<div class="row">
										<div class="col-sm-6">
											<script>
												$(document).ready(function()
												{
													$("select[name='product_id']").setVal("{{ old('product_id', !empty($bundle_item) ? $bundle_item->product_id : '') }}");
												});
											</script>
											<div class="form-group">
												<label>Product</label>
												<div class="input-group">
													<select name="product_id" class="form-control input-toggle-withdata" data-target="#sku_id" />
														@foreach($products as $product)
															<option value="{{ $product->id }}" data-url="{{ route('admin.product.getskus', ['id' => $product->id]) }}">{{ $product->name }}</option>
														@endforeach
													</select>
												</div>
											</div>
											
											<div class="form-group">
												<label>Quantity</label>
												<input type="number" class="form-control" name="quantity" value="{{ old('quantity', !empty($bundle_item) ? $bundle_item->quantity : '') }}" placeholder="Quantity" />
											</div>
											
											@if(empty($bundle_item))
												<div class="form-check">
													<label class="form-check-label"><input type="checkbox" class="form-check-input" name="active" @if(empty($bundle_item) || $bundle_item->active) checked @endif /> Active</label>
												</div>
											@endif
											
										</div>
										<div class="col-sm-6">
											<script>
												$(document).ready(function()
												{
													$("select[name='sku_id']").setVal("{{ old('sku_id', !empty($bundle_item) ? $bundle_item->sku_id : '') }}");
												});
											</script>
											<div id="sku_id" class="form-group">
												<label>Sku</label>
												<div class="input-group">
													<select name="sku_id" class="form-control" data-value="{{ old('sku_id', !empty($bundle_item) ? $bundle_item->sku_id : '') }}"/>
														@foreach($skus as $sku)
															<option value="{{ $sku->id }}">{{ $sku->name }}</option>
														@endforeach
													</select>
												</div>
											</div>
											
										</div>
									</div>
								</div>
								<div class="card-footer">
									@if(!empty($bundle_item))
										<button type="submit" class="btn btn-primary">Change</button>
									@else
										<button type="submit" class="btn btn-success">Add</button>
									@endif
								</div>
							</div><!-- /.card -->
						</form>
					</div>
					<!-- /.col-lg-12 -->

				</div>
				<!-- /.row -->
				
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content -->

	</div>
	<!-- /.content-wrapper -->
@endsection