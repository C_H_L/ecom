@extends(env('ADMIN_FOLDER') . 'layouts.master')
@section('order') active @endsection

@section('content')
	<script>
		$(document).ready(function()
		{
			@if(!empty($order))
				$(".record-delete").click(function()
				{
					bootbox.confirm(
					{
						message: "Confirm delete record",
						backdrop: true,
						buttons:
						{
							confirm:
							{
								label: "Delete",
								className: "btn-danger",
							},
							cancel:
							{
								label: "Cancel",
							},
						},
						callback: function(result)
						{
							if(result)
							{
								$(".overlay-loading").show();
								window.location.href = "{{ route('admin.order.delete', ['id' => $order->id]) }}";
							}
						},
					});
				});
				
				$(".record-toggleactive").click(function()
				{
					bootbox.dialog(
					{
						message: 'Confirm set order as @if($order->active) inactive @else active @endif?',
						backdrop: true,
						buttons:
						{
							cancel:
							{
								label: 'Cancel',
							},
							confirm:
							{
								label: 'Confirm',
								className: "btn-warning",
								callback: function()
								{
									window.location.href = "{{ route('admin.order.toggleactive', ['id' => $order->id]) }}";
								},
							},
						},
					});
				});
				
				@if($order->status > 0)
					$(".btn-update-status-prev").click(function()
					{
						bootbox.dialog(
						{
							message: 'Confirm update status to {{ $order->getStatus($order->status - 1) }}?',
							backdrop: true,
							buttons:
							{
								cancel:
								{
									label: 'Cancel',
								},
								confirm:
								{
									label: 'Confirm',
									className: "btn-warning",
									callback: function()
									{
										window.location.href = "{{ route('admin.order.updatestatus', ['id' => $order->id, 'status' => '0']) }}";
									},
								},
							},
						});
					});
				@endif
				
				@if($order->status < (count(App\Order::$statuses) - 1))
					$(".btn-update-status-next").click(function()
					{
						bootbox.dialog(
						{
							message: 'Confirm update status to {{ $order->getStatus($order->status + 1) }}?',
							backdrop: true,
							buttons:
							{
								cancel:
								{
									label: 'Cancel',
								},
								confirm:
								{
									label: 'Confirm',
									className: "btn-success",
									callback: function()
									{
										window.location.href = "{{ route('admin.order.updatestatus', ['id' => $order->id, 'status' => '1']) }}";
									},
								},
							},
						});
					});
				@endif
			@endif
			
		});
	</script>
	
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2 mt-1">
					<div class="col-sm-6">
						<a href="{{ route('admin.order') }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i>&nbsp; Back</a>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content-header -->

		<!-- Main content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="card @if(!empty($order)) card-primary @else card-success @endif">
							<div class="card-header">
								<h3 class="card-title">@if(!empty($order)) Order #{{ $order->id }} @else Add Order @endif</h3>
								<div class="card-tools">
									<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
								</div>
							</div>
							<div class="card-body">
								@if(!empty($order))
									@if($order->status > 0)
										<button type="button" class="btn btn-sm btn-warning btn-update-status-prev">Set status to {{ $order->getStatus($order->status - 1) }}</button>
									@endif
									
									
									@if($order->status < (count(App\Order::$statuses) - 1))
										<button type="button" class="btn btn-sm btn-success btn-update-status-next">Set status to {{ $order->getStatus($order->status + 1) }}</button>
									@endif
								
									<span class="clearfix">
										<button type="button" class="btn btn-sm btn-danger btn-float-right record-delete"><i class="fa fa-trash"></i></button>
										<button type="button" class="btn btn-sm btn-warning btn-float-right record-toggleactive">Set @if($order->active) inactive @else active @endif</button>
									</span>
									<hr />
									
									<table class="record-single-table">
										<tr>
											<td>ID</td>
											<td>{{ $order->id }}</td>
											<td>Created Time</td>
											<td>{{ $order->created_at_text }}</td>
										</tr>
										<tr>
											<td>Item total</td>
											<td>RM{{ $order->item_total_text }}</td>
											<td>Sub total</td>
											<td>{{ $order->Payment->sub_total_text }}</td>
										</tr>
										<tr>
											<td>Coupon Value</td>
											<td>RM{{ $order->coupon_value_text }}</td>
											<td>Payment Gateway</td>
											<td>{{ $order->Payment->payment_gateway_text }} ({{ $order->Payment->payment_method }})</td>
										</tr>
										<tr>
											<td>Shipping Fee</td>
											<td>RM{{ $order->shipping_fee_text }}</td>
											<td>Payment Reference</td>
											<td>{{ $order->Payment->payment_ref }}</td>
										</tr>
										<tr>
											<td>Tax</td>
											<td>RM{{ $order->tax_text }}</td>
											<td>Grand Total</td>
											<td>RM{{ $order->grand_total_text }}</td>
										</tr>
										<tr>
											<td>Status</td>
											<td>{{ $order->status_name }}</td>
											<td>Active</td>
											<td>{{ $order->active_text }}</td>
										</tr>
									</table>
								@endif
							</div>
						</div><!-- /.card -->
					</div>
					<!-- /.col-lg-12 -->

				</div>
				<!-- /.row -->
				
				@if(!empty($order))
					@if(count($order->OrderShippingAddr) > 0 && count($order->OrderBillingAddr) > 0)
						<div class="row">
							<div class="col-lg-12">
								<div id="order_addr" class="card card-info">
									<div class="card-header">
										<h3 class="card-title">Order Addresses</h3>
										<div class="card-tools">
											<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
										</div>
									</div>
									<div class="card-body">
										<div class="record-single-title">Shipping Details</div>
										<table class="record-single-table">
											<tr>
												<td>First Name</td>
												<td>{{ $order->OrderShippingAddr[0]->first_name }}</td>
												<td>Last Name</td>
												<td>{{ $order->OrderShippingAddr[0]->last_name }}</td>
											</tr>
											<tr>
												<td>DOB</td>
												<td>{{ $order->OrderShippingAddr[0]->dob_text }}</td>
												<td>Gender</td>
												<td>{{ $order->OrderShippingAddr[0]->full_name }}</td>
											</tr>
											<tr>
												<td>Email</td>
												<td><a href="mailto:{{ $order->OrderShippingAddr[0]->email }}">{{ $order->OrderShippingAddr[0]->email }}</a></td>
												<td>Contact Number</td>
												<td><a href="tel:{{ $order->OrderShippingAddr[0]->contact_num }}">{{ $order->OrderShippingAddr[0]->contact_num }}</a></td>
											</tr>
											<tr>
												<td>Address</td>
												<td colspan="3">
													{{ $order->OrderShippingAddr[0]->addr_1 }},</br>
													{{ $order->OrderShippingAddr[0]->addr_2 }}
												</td>
											</tr>
											<tr>
												<td>City</td>
												<td>{{ $order->OrderShippingAddr[0]->city }}</td>
												<td>Zipcode</td>
												<td>{{ $order->OrderShippingAddr[0]->zipcode }}</td>
											</tr>
											<tr>
												<td>Country</td>
												<td colspan="3">{{ $order->OrderShippingAddr[0]->country }}</td>
											</tr>
										</table>
										
										<div class="record-single-title">Billing Details</div>
										<table class="record-single-table">
											<tr>
												<td>First Name</td>
												<td>{{ $order->OrderBillingAddr[0]->first_name }}</td>
												<td>Last Name</td>
												<td>{{ $order->OrderBillingAddr[0]->last_name }}</td>
											</tr>
											<tr>
												<td>DOB</td>
												<td>{{ $order->OrderBillingAddr[0]->dob_text }}</td>
												<td>Gender</td>
												<td>{{ $order->OrderBillingAddr[0]->full_name }}</td>
											</tr>
											<tr>
												<td>Email</td>
												<td><a href="mailto:{{ $order->OrderBillingAddr[0]->email }}">{{ $order->OrderBillingAddr[0]->email }}</a></td>
												<td>Contact Number</td>
												<td><a href="tel:{{ $order->OrderBillingAddr[0]->contact_num }}">{{ $order->OrderBillingAddr[0]->contact_num }}</a></td>
											</tr>
											<tr>
												<td>Address</td>
												<td colspan="3">
													{{ $order->OrderBillingAddr[0]->addr_1 }},</br>
													{{ $order->OrderBillingAddr[0]->addr_2 }}
												</td>
											</tr>
											<tr>
												<td>City</td>
												<td>{{ $order->OrderBillingAddr[0]->city }}</td>
												<td>Zipcode</td>
												<td>{{ $order->OrderBillingAddr[0]->zipcode }}</td>
											</tr>
											<tr>
												<td>Country</td>
												<td colspan="3">{{ $order->OrderBillingAddr[0]->country }}</td>
											</tr>
										</table>
									</div>
								</div><!-- /.card -->
							</div>
						</div>
					@endif
					
					<div class="row">
						<div class="col-lg-12">
							<div id="order_coupon" class="card card-info">
								<div class="card-header">
									<h3 class="card-title">Order Coupons</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="card-body">
									<table class="table table-striped table-bordered record-list-table">
										<thead>
											<tr>
												<th style="width: 1%;">
													ID
												</th>
												<th style="width: 15%;">
													Coupon Name
												</th>
												<th style="width: 5%;">
													Amount
												</th>
											</tr>
										</thead>
										<tbody>
											@foreach($order->OrderCoupon as $coupon)
												<tr>
													<td>
														{{ $coupon->id }}
													</td>
													<td>
														<a href="{{ route('admin.coupon.single', ['id' => $coupon->Coupon->id]) }}">{{ $coupon->Coupon->name }}</a>
													</td>
													<td>
														{{ $coupon->amount_text }}
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div><!-- /.card -->
						</div>
					</div>
					
					<div class="row">
						<div class="col-lg-12">
							<div id="order_item" class="card card-info">
								<div class="card-header">
									<h3 class="card-title">Order Items</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="card-body">
									<table class="table table-striped table-bordered record-list-table">
										<thead>
											<tr>
												<th style="width: 1%;">
													ID
												</th>
												<th style="width: 15%;">
													Item
												</th>
												<th style="width: 5%;">
													Quantity
												</th>
												<th style="width: 5%;">
													Unit Price
												</th>
												<th style="width: 5%;">
													Total
												</th>
											</tr>
										</thead>
										<tbody>
											@foreach($order->OrderItem as $item)
												<tr data-href="{{ route('admin.order.item.single', ['order_id' => $order->id, 'id' => $item->id]) }}">
													<td>
														{{ $item->id }}
													</td>
													<td>
														@if($item->type == 0)
															<a href="{{ route('admin.product.sku.single', ['product_id' => $item->Item->product_id, 'id' => $item->item_id]) }}">{{ $item->Item->name }}</a>
														@elseif($item->type == 1)
															<a href="{{ route('admin.bundle.single', ['id' => $item->item_id]) }}">{{ $item->Item->name }}</a>
														@endif
													</td>
													<td>
														{{ $item->quantity }}
													</td>
													<td>
														RM{{ $item->unit_price_text }}
													</td>
													<td>
														RM{{ $item->total_text }}
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div><!-- /.card -->
						</div>
					</div>
				@endif
				
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content -->

	</div>
	<!-- /.content-wrapper -->
@endsection