@extends(env('ADMIN_FOLDER'). 'layouts.master')
@section('banner') active @endsection

@section('content')
<script>
	$(document).ready(function()
	{
		$(".record-media .record-modify").click(function()
		{
			var actionURL = $(this).attr("data-action-url");
			var modalElement = $("#record-media-modify");
			var modalForm = modalElement.find("form");
			
			$(".overlay-loading").show();
			
			$.post(
			{
				url: actionURL,
				data:
				{
					"_token": "{{ csrf_token() }}",
				},
				success: function(response)
				{
					$.each(response, function(key, value)
					{
						var formElement = modalElement.find("[name='" + key + "']");
						
						if(formElement.attr("type") == "checkbox")
						{
							formElement.prop("checked", (value == 1 ? true : false));
						}
						else
						{
							formElement.val(value);
						}
					});
					
					modalElement.modal('show');
					$(".overlay-loading").hide();
					
					modalForm.submit(function(e)
					{
						e.preventDefault();
						
						$(".overlay-loading").show();
						$.post(
						{
							url: actionURL + "/submit",
							data: modalForm .serialize(),
							complete: function()
							{
								$(".overlay-loading").hide();
							},
							success: function(response)
							{
								if(response == "success")
								{
									window.location.reload();
								}
								else
								{
									bootboxError(response);
								}
							},
						});
					});
				}
			});
		});
		
		$(".record-media .record-delete").click(function()
		{
			var actionURL = $(this).attr("data-action-url");
			
			var modal = bootbox.dialog(
			{
				message: "Confirm delete banner?",
				title: "Delete Banner",
				buttons:
				{
					close:
					{
						label: "Cancel",
						className: "btn-default"
					},
					save:
					{
						label: "Delete",
						className: "btn-danger",
						callback: function()
						{
							$(".overlay-loading").show();
							$.get(actionURL, function(response)
							{
								if(response == "success")
								{
									$(".overlay-loading").hide();
									window.location.reload();
								}
							});
						}
					}
				},
				backdrop: true,
				closeButton: false,
				onEscape: function()
				{
					modal.modal("hide");
				}
			});
		});
	});
</script>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
	  <div class="container-fluid">
		<div class="row mb-2">
		  <div class="col-sm-6">
			<h1 class="m-0 text-dark">Banner</h1>
		  </div><!-- /.col -->
		</div><!-- /.row -->
	  </div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<div class="content">
	  <div class="container-fluid">
		<div class="row">
		  <div class="col-lg-12">
			<div class="card card-primary card-outline">
			  <div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="{{ route('admin.banner.upload') }}" id="dropzone-media" class="dropzone dropzone-media" enctype="multipart/form-data">
							{{ csrf_field() }}
						</form>
						</br>
					</div>
					@foreach($banners as $banner)
						<div class="col-md-3 same-height">
							<div class="record-media">
								@if($banner->type == 0)
									<img src="{{ $banner->image_asset }}" class="media" />
								@elseif($banner->type == 1)
									<video class="media" playsinline autoplay="autoplay" muted="muted" loop>
										<source src="{{ $banner->image_asset }}" type="video/mp4" />
									</video>
								@endif
								
								<div class="overlay">
									<div class="control record-modify" data-action-url="{{ route('admin.banner.single', ['id' => $banner->id]) }}"><i class="fa fa-edit"></i></div>
									<div class="control record-delete" data-action-url="{{ route('admin.banner.delete', ['id' => $banner->id]) }}"><i class="fa fa-trash"></i></div>
									
									@if($banner->active == 0)
										<div class="not-active"><i class="fa fa-eye-slash"></i></div>
									@endif
								</div>
							</div>
						</div>
					@endforeach
				</div>
			  </div>
			</div><!-- /.card -->
		  </div>
		  <!-- /.col-md-6 -->
		  
		</div>
		<!-- /.row -->
	  </div><!-- /.container-fluid -->
	</div>
	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection