@extends('admin.layouts.master')

@section('head')
	<link rel="stylesheet" href="{{ asset('js/dropzone/dropzone.css') }}">
	<link rel="stylesheet" href="{{ asset('css/banner.css') }}">

	<script src="{{ asset('js/dropzone/dropzone.js') }}"></script>
	<script src="{{ asset('js/bootbox.min.js') }}"></script>
@endsection

@section('content')

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-12 text-center">
						<h1 class="m-0 text-dark">Select a page from the left</h1>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content-header -->
	</div>
	<!-- /.content-wrapper -->
@endsection