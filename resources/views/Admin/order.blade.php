@extends(env('ADMIN_FOLDER') . 'layouts.master')

@section('order') active @endsection

@section('content')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">Orders</h1>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content-header -->

		<!-- Main content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-primary card-outline">
							<div class="card-header p-2">
								<ul class="nav nav-pills">
									@foreach($order_groups as $key=>$group)
										<li class="nav-item">
											<a href="#{{ $key }}" class="nav-link @if($loop->first) active @endif" data-toggle="tab">
												{{ $group->name }}&nbsp;
												<span class="badge bg-primary">{{ $group->count }}</span>
											</a>
										</li>
									@endforeach
								</ul>
							</div>
							<div class="card-body">
								<div class="tab-content">
									@foreach($order_groups as $key=>$group)
										<div id="{{ $key }}" class="tab-pane @if($loop->first) active @endif">
											<table class="table table-striped table-bordered record-list-table">
												<thead>
													<tr>
														<th style="width: 2%;">
															ID
														</th>
														<th style="width: 11.5%;">
															Name
														</th>
														<th style="width: 11.5%;">
															Email
														</th>
														<th style="width: 11.5%;">
															Shipping Fee
														</th>
														<th style="width: 5%;">
															Coupon
														</th>
														<th style="width: 5%;">
															Tax
														</th>
														<th style="width: 10%;">
															Grand Total
														</th>
														<th style="width: 13%;">
															Created Time
														</th>
													</tr>
												</thead>
												<tbody>
													@foreach($group->orders as $order)
														<tr data-href="{{ route('admin.order.single', ['id' => $order->id]) }}">
															<td>
																{{ $order->id }}
															</td>
															<td>
																{{ (!empty($order->OrderShippingAddr) && count($order->OrderShippingAddr) > 0 ? $order->OrderShippingAddr[0]->full_name : "") }}
															</td>
															<td>
																@if(!empty($order->OrderShippingAddr) && count($order->OrderShippingAddr) > 0)
																	<a href="mailto:{{ $order->OrderShippingAddr[0]->email }}">{{ $order->OrderShippingAddr[0]->email }}</a>
																@endif
															</td>
															<td>
																RM{{ $order->shipping_fee_text }}
															</td>
															<td>
																RM{{ $order->coupon_value_text }}
															</td>
															<td>
																RM{{ $order->tax_text }}
															</td>
															<td>
																RM{{ $order->grand_total_text }}
															</td>
															<td>
																{{ $order->created_at_text }}
															</td>
														</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									@endforeach
								</div>
							</div>
						</div><!-- /.card -->
					</div>
					<!-- /.col-md-6 -->

				</div>
				<!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content -->

	</div>
	<!-- /.content-wrapper -->
@endsection