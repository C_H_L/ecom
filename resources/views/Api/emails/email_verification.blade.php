<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link href="{{ asset('css/email.css?' . env('APP_VERSION')) }}" rel="stylesheet" type="text/css" />
    </head>
    <body>
		<img src="{{ asset('images/logo.png') }}" class="main-logo" />
		<br />
		
		Hi {{ $user->name }},<br />
		<br />
		
		Thank you for registering at {{ env("APP_NAME") }}.<br />
		To verify your account email please click the link below<br />
		<br />
		
		<br />
		<div class="main-link">
			<a href="{{ route('user.verify.email', ['key' => $key]) }}" class="btn">Verify Your Email</a><br />
			<br />
			Or copy this link into your browser address bar<br />
			<a href="{{ route('user.verify.email', ['key' => $key]) }}" target="blank">{{ route('user.verify.email', ['key' => $key]) }}</a>
		</div>
		
		<br />
		<br />
		Best Regards,<br />
		<b>{{ env("APP_NAME") }},</b><br />
		<br />
		<br />
		
		<div class="footer">
			Copyright {{ date("Y") }} &copy; {{ env("APP_NAME") }}<br />
			<div class="separator"></div>
			<div class="disclaimer">This is an automatically generated email, please do not reply to it</div>
		</div>
    </body>
</html>