@extends('layouts/layout', ["title" => "Contact Us", "include_header" => false, "include_footer" => false])

@section('head')
	
@endsection

@section('content')
	<div class="container text-center">
		<div class="row">
			<div class="col-md-offset-3 col-md-6">
				<div class="section">
					<div class="title center">
						<div class="icon">
							<i class="fas fa-envelope"></i>
						</div>
						
						<div class="text">Contact Us</div>
					</div>
					<form method="POST" class="text-left" action="{{ route('app.contact.submit') }}">
						{{ csrf_field() }}
						
						<div class="row">
							<div class="col-md-12">
								<div class="form-group {{ ($errors->has('name') ? 'has-error' : '') }}">
									<label>Name</label>
									<input type="text" name="name" class="no-style form-control" value="{{ old('name') }}" placeholder="Your name" />
									<small class="error">{{ ($errors->has('name') ? $errors->first('name') : '') }}</small>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group {{ ($errors->has('email') ? 'has-error' : '') }}">
									<label>E-mail</label>
									<input type="email" name="email" class="no-style form-control" value="{{ old('email') }}" placeholder="Your e-mail" />
									<small class="error">{{ ($errors->has('email') ? $errors->first('email') : '') }}</small>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group {{ ($errors->has('contact_num') ? 'has-error' : '') }}">
									<label>Contact Number</label>
									<input type="text" name="contact_num" class="no-style form-control" value="{{ old('contact_num') }}" placeholder="Your contact number" />
									<small class="error">{{ ($errors->has('contact_num') ? $errors->first('contact_num') : '') }}</small>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								<div class="form-group {{ ($errors->has('message') ? 'has-error' : '') }}">
									<label>Message</label>
									<textarea name="message" rows="6" class="no-style form-control" placeholder="Write your message here">{{ old('message') }}</textarea>
									<small class="error">{{ ($errors->has('message') ? $errors->first('message') : '') }}</small>
								</div>
							</div>
						</div>
						
						<div class="text-right">
							<button class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection