@extends('layouts/layout', ["title" => "Doctor Profile"])

@section('head')
	
@endsection

@section('content')
	<div class="container text-center">
		<div class="notification-box">
			<img src="{{ asset('images/logo.png') }}" class="logo" />
			Please view this page on our mobile app</br>
			</br>
			
			<a href="{{ env('APP_STORE_ANDROID') }}" class="get-app-link">
				<img src="{{ asset('images/global/play_store.png') }}" />
			</a>
			
			<a href="{{ env('APP_STORE_IOS') }}" class="get-app-link">
				<img src="{{ asset('images/global/ios_store.png') }}" />
			</a>
		</div>
	</div>
@endsection