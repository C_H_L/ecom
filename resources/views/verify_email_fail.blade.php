@extends('layouts/layout', ["title" => "Failed"])

@section('head')
	
@endsection

@section('content')
	<div class="container text-center">
		<div class="vp-v-center notification-box">
			<div class="icon text-danger">
				<i class="fas fa-exclamation-circle"></i>
			</div>
			
			<span class="font-1-5r">Failed to verify email</span>
		</div>
	</div>
@endsection