@extends('layouts/layout', ["title" => "Success"])

@section('head')
	
@endsection

@section('content')
	<div class="container text-center">
		<div class="vp-v-center notification-box">
			<div class="icon text-success">
				<i class="far fa-check-circle"></i>
			</div>
			
			<span class="font-1-5r">Successfully verified email</span>
		</div>
	</div>
@endsection