<?php
	return
	[
		"record" =>
		[
			"not_found" => "Record not found",
			"already_exist" => "Record already exist",
			"create_success" => "Create successful",
			"update_success" => "Update successful",
			"read_success" => "Read successful",
			"delete_success" => "Delete successful",
			"create_fail" => "Error creating record",
			"update_fail" => "Error updating record",
			"read_fail" => "Error retrieving record",
			"delete_fail" => "Error deleting record",
		],
		"access" =>
		[
			"version_not_found" => "The requested version is not found",
			"route_not_found" => "The requested API route is not found",
			"invalid_app_version" => "Your app version is invalid",
			"older_app_version" => "A newer version of this app is available, please update your app",
			"appointment_needs_review" => "You have a completed appointment that needs review",
			"encryption" =>
			[
				"decryption_fail" => "Failed to decrypt data",
				"encryption_fail" => "Failed to encrypt data",
			]
		],
		"auth" =>
		[
			"unauthenticated" => "Please login to continue",
		],
		"register" =>
		[
			"taken" => "The username or email have been taken",
			"success" => "A verification email have been sent to your email",
			"resend_email_verification" =>
			[
				"not_found" => "No user with that email was found",
				"already_verified" => "The account email have already been verified",
				"not_required" => "The account email doesn't require verification",
				"success" => "Successfully resend email verification",
			],
			"password_validation_failed" => "Pasword must be alpha-numeric, minimum 8 letters, contains lowercase and uppercase",
			"thirdparty" =>
			[
				"already_exist" => "The user account already exist",
			],
		],
		"login" =>
		[
			"inactive" => "This account is inactive",
			"not_match" => "Login credentials do not match our records",
			"verify_email" => "Please verify your email to continue",
			"incorrect_login_type" => "Wrong login method",
			"forgot_password" =>
			[
				"not_found" => "No user with that email was found",
				"not_available" => "This feature is not available for your registration type",
				"success" => "Plese check your email for the password reset instructions",
			],
			"thirdparty" =>
			[
				"not_found" => "The requested user is not found",
			],
		],
		"logout" =>
		[
			"success" => "Sucessfully logged out",
		],
		"user" =>
		[
			"incorrect_password" => "Incorrect current password",
			"not_available" => "This feature is not available for your registration type",
			"email_exist" => "That email is already being use by another account",
		],
		"user_emergency_contact" =>
		[
			"max_reached" => "Maximum allowed number of contacts is " . env("MAX_USER_EMERGENCY_CONTACT", 3),
		],
		"appointment" =>
		[
			"session_not_available" => "The session is unavailable for booking",
			"doctor_not_verified" => "This doctor account is not verified",
			"not_available" => "This feature is not available for the status of this appointment",
			"guest_appointment" => "This feature is not available for guest appointments",
			"already_set" => "Nothing to change",
			"no_user_details" => "You must first update your account details if you choose to share your information with the doctor",
		],
		"doctor" =>
		[
			"already_verified" => "This doctor account have already been verified",
			"already_requested" => "You have already requested for this doctor to verify their acccount",
		],
		"doctor_review" =>
		[
			"not_available" => "Review is not yet available for this appointment",
			"already_reviewed" => "You have already left a review for this appointment",
			"success" => "Review submitted",
		],
		"campaign" =>
		[
			"already_verified" => "This campaign account have already been verified",
			"already_requested" => "You have already requested for this campaign to verify their acccount",
		],
		"campaign_review" =>
		[
			"not_available" => "Review is not yet available for this appointment",
			"already_reviewed" => "You have already left a review for this appointment",
			"success" => "Review submitted",
		],
	];